package ecs160.project.locationtask;

public class Message extends Communication
{
	public Message() {}
	
	public Message(String text, Location loc)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = false;
	}
	
	public Message(String text, Location loc, boolean read)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = read;
	}
	
	public boolean getMessageRead()
	{
		return messageRead;
	}
	
	public void setMessageRead(boolean val)
	{
		messageRead = val;
	}
	
	private boolean messageRead;
}