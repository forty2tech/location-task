package ecs160.project.locationtask;

public class Communication
{
	public Communication() 
	{
		location = new Location(0.0, 0.0);
		sender = "tempSender";
	}
	
	public void setTextData(String msg)
	{
		textData = msg;
	}
	
	public void setLocation(Location loc)
	{
		location = loc;
	}
	
	public String getTextData()
	{
		return textData;
	}	
	
	public Location getLocation()
	{
		return location;
	}
	
	public String getSender()
	{
		return sender;
	}	
	
	public void setSender(String s)
	{
		sender = s;
	}
	
	private String textData;
	private Location location;
	private String sender;
}

