Files:

	1.	MySQLiteHelper.java
 	2.	DataSource.java (implementation)

Notes:

	-	All of the Communication classes (Communication, Message, Task, Query)
		as well as Time and Location have been modified.
	
	-	We might change how we implement the Time class because there may be an
		easier way to record the date and time.

Steps:
	
	Setting up the database
	
	1.	Put all the files inside this folder into the src folder.
	
	2.	In your main activity, outside of any method, declare the database as:
	
			static DataSource [database name];
			 
		This will create a DataSource reference.
		
	3.	Inside the onCreate method for that main activity, instantiate the 
		database by doing:
		
			[database name] = new DataSource(this);
		 
	4.	Do Shift+CTRL+O to import the SQLite packages that are needed.


	Using the database
	
	1.	Every time you access the database, remember to open it by doing:
	
			[database name].open();
			
		After you're done using it, close it by doing:
		
			[database name].close();
			
		Note: You can open and close the database as many times as you want.
		
	2.	The database includes several methods that you can use with different 
		Communication types including:

		Note:	In the following examples, "user: refers to the person currently
				using the app.
		Note2:	Again, remember to open the database before using it and close 
				it after you're done with it. This was not shown in the 
				following examples to keep them short.

		----- Message -----
		
			void deleteMessage(String user, Message msg) 
			
				Ex: 
					//deletes Viet's message
					[database name].deleteMessage("Viet", new Message("Hello",
							new Location(1.0, 2.0))); 
				
			int getTotalNumberMessages(String user)
			
				Ex: 
					 //gets all msgs for Viet
					int numberMsgs;
					numberMsgs = [database name].getTotalNumberMessage("Viet");
				
			int getNumberMessagesAtLocation(String user, Location loc)
			
				Ex: 
					//gets number of messages at the specified location
					int numberMsgsLoc = 
						[database name].getNumberMessagesAtLocation("Viet",
								new Location(1.0, 2.0)); 
					
			
			ArrayList<Message> getAllMessagesAtLocation(String user, Location l)
			
				Ex: 
					//gets all messages at the specified location
					ArrayList<Message> msgs;
					msgs = [database name].getAllMessagesAtLocation("Viet", 
							new Location(1.0, 2.0)); 
			
			void storeMessage(String user, Message m)
			
				Ex: 
					//stores the message for the given user
					[database name].storeMessage("Viet", new Message("Hello", 
							new Location(1.0, 2.0))); 
				
			void updateMessage(String id, Message m)
			
				Ex: 
					//updates the hasRead variable for Viet's message
					[database name].updateMessage("Viet", new Message("Hello",
							new Location(1.0, 2.0), true)); 
			
		----- Task -----
		
			void deleteTask(String user, Task tsk)
			
				Ex: 
					[database name].deleteTask("Viet", new Task());
				
			int getTotalNumberTasks(String user)
			
				Ex: 
					int numberTasks
					numberTasks = [database name].getTotalNumberTasks("Viet");
				
			int getNumberTasksAtLocation(String user, Location loc)
			
				Ex: 
					int numberTasksLoc;
					numberTasksLoc = 
						[database name].getNumberTasksAtLocation("Viet",
								new Location());
				
			ArrayList<Task> getAllTasksAtLocation(String user, Location loc)
			
				Ex: 
					ArrayList<Task> tasks;
					tasks = [database name].getAllTasksAtLocation("Viet", 
							new Location());
			
			void storeTask(String user, Task t)
			
				Ex: 
					[database name].storeTask("Viet", new Task());
					
				
		----- Query -----
			
			void storeQuery(String user, Query q)
			
				Ex: 	
					[database name].storeQuery("Raymond", new Query("Message",
							new Location(0.0,0.0) ));
					
					or	
					
					Time begin = new Time();
					Time end = new Time();
					
					[database name].storeQuery("Raymond", new Query("Message",
							new Location(0.0,0.0), begin, end));
						
			
			void deleteQuery(String user, Query q)
			
				Ex:		
					[database name].storeQuery("Viet", 
							new Query("How good is the food here?", 
									new Location(21.2, 45.7) ));
		
		
			ArrayList<Query> getAllQueriesAtLocation(String user, Location loc)
			
				Ex:
					ArrayList<Query> Qs = new ArrayList<Query>();
					
					Qs = [database name].getAllQueriesAtLocation("Viet", 
							new Location(1.0, 2.0) );
					
					
			int getTotalNumberQueries(String user)
			
				Ex:
					Well, you get the point.
					
			int getNumberQueriesAtLocation(String user, Location loc)
			

- Robert & Luis