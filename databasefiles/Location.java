package ecs160.project.locationtask;

public class Location
{
	public Location() {}

	public Location(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public double getLatitude()
	{
		return latitude;
	}

	private double longitude;
	private double latitude;
}
