package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProcessOutGeoMessage extends Activity {
	Button del_out_geomsg,cancel_out_geomsg;
	String out_geomsg,sender,out_imgURL;
	Double out_geomsg_lat=0.0;
	Double out_geomsg_long=0.0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.process_outgeomsg);
		
		//insert code here...
		Bundle out_geomsg_bundle = getIntent().getExtras();
		out_geomsg = out_geomsg_bundle.getString("out_geomsg");
		sender = out_geomsg_bundle.getString("sender");
		out_imgURL = out_geomsg_bundle.getString("out_imgURL");
		out_geomsg_lat = out_geomsg_bundle.getDouble("out_geolatitude");
		out_geomsg_long= out_geomsg_bundle.getDouble("out_geolongitude");
		
		TextView out_geomsg_frview = (TextView)findViewById(R.id.geooutmsg_from_textView);
		String s = "To " + sender;
		out_geomsg_frview.setText(s);
		
		TextView out_geomsg_dataview = (TextView)findViewById(R.id.geomoutmsg_msg_textView);
		out_geomsg_dataview.setText(out_geomsg);
		
		TextView out_geomsg_locview = (TextView)findViewById(R.id.geooutmsg_loc_textView);
		s = "Latitude = "+ Double.toString(out_geomsg_lat) + " , Longitude = "+ Double.toString(out_geomsg_long);
		out_geomsg_locview.setText(s);
		
		Drawable image = ImageOperations(getBaseContext(), out_imgURL,"image.jpg");
		ImageView out_imgView = (ImageView)findViewById(R.id.geooutmsg_imageView);
		out_imgView.setImageDrawable(image);
		
		del_out_geomsg = (Button)findViewById( R.id.geooutmsg_del_button);
		del_out_geomsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				AlertPopUp("Delete GeoMessage","Are you sure you want to delete this geo_message ?");
				Log.w("C2DM","del GeoMsg =" + out_geomsg);
			}
		});
		
		cancel_out_geomsg = (Button)findViewById( R.id.geooutmsg_cancel_button);
		cancel_out_geomsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				Intent i = new Intent(ProcessOutGeoMessage.this,outGeoMsgPage.class);
				ProcessOutGeoMessage.this.startActivity(i);
			}
		});
		
	}
	
	public void AlertPopUp(String title, String warningMsg){
		new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(warningMsg)
			.setIcon(R.drawable.attentionicon)
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					LocationTaskActivity.myDATABASE.open();
					LocationTaskActivity.myDATABASE.deleteGeoMessage(LocationTaskActivity.username, new GeoMessage(out_imgURL, sender, out_geomsg,new Location(out_geomsg_lat,out_geomsg_long)));
					LocationTaskActivity.myDATABASE.close();
					
					Toast.makeText(getBaseContext(),"GeoMessage is deleted", Toast.LENGTH_LONG).show();
					
					Intent i = new Intent(ProcessOutGeoMessage.this,outGeoMsgPage.class);
					ProcessOutGeoMessage.this.startActivity(i);
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(),"GeoMessage is not deleted", Toast.LENGTH_LONG).show();
				}
			})
			.show();
	}
	
	  private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
	
		 try {
		   InputStream is = (InputStream) this.fetch(url);
		   Drawable d = Drawable.createFromStream(is, "src");		
		   return d;	
		 } catch (MalformedURLException e) {		
		   e.printStackTrace();		
		   return null;		
		 } catch (IOException e) {		
		   e.printStackTrace();		
		   return null;		
		 }	
	}
	
	public Object fetch(String address) throws MalformedURLException,IOException {
	
		 URL url = new URL(address);		
		 Object content = url.getContent();		
		 return content;		
	}
}
