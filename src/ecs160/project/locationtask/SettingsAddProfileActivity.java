package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SettingsAddProfileActivity extends Activity
{
	EditText profileName;
	
	ToggleButton ringer, vibrate;
	
	//expiration
	TextView expirationDisplay;
	Button expiration;
	int expYear;
	int expMonth;
	int expDay;
	static final int DATE_EXPIRE_ID = 0;
	
	CheckBox monday, tuesday, wednesday, thursday, friday, saturday, sunday;
	
	//monday block
	TextView mon_text, beginMon_label, endMon_label;
	TextView mon_start_display, mon_end_display; Button mon_start, mon_end;
	static final int START_TIME_MONDAY = 1;
	static final int END_TIME_MONDAY = 2;
	int monStartHr, monEndHr;
	int monStartMin, monEndMin;
	int monStartAMPM, monEndAMPM;
	int monsCounter; 
	int moneCounter;
	
	//tuesday block
	TextView tue_text, beginTue_label, endTue_label;
	TextView tue_start_display, tue_end_display; Button tue_start, tue_end;
	static final int START_TIME_TUESDAY = 3;
	static final int END_TIME_TUESDAY = 4;
	int tueStartHr, tueEndHr;
	int tueStartMin, tueEndMin;
	int tueStartAMPM, tueEndAMPM;
	int tuesCounter;
	int tueeCounter;
	
	//wednesday block
	TextView wed_text, beginWed_label, endWed_label;
	TextView wed_start_display, wed_end_display; Button wed_start, wed_end;
	static final int START_TIME_WEDNESDAY = 5;
	static final int END_TIME_WEDNESDAY = 6;
	int wedStartHr, wedEndHr;
	int wedStartMin, wedEndMin;
	int wedStartAMPM, wedEndAMPM;
	int wedsCounter;
	int wedeCounter;
	
	//thursday block
	TextView thu_text, beginThu_label, endThu_label;
	TextView thu_start_display, thu_end_display; Button thu_start, thu_end;
	static final int START_TIME_THURSDAY = 7;
	static final int END_TIME_THURSDAY = 8;
	int thuStartHr, thuEndHr;
	int thuStartMin, thuEndMin;
	int thuStartAMPM, thuEndAMPM;
	int thusCounter;
	int thueCounter;
	
	//friday block
	TextView fri_text, beginFri_label, endFri_label;
	TextView fri_start_display, fri_end_display; Button fri_start, fri_end;
	static final int START_TIME_FRIDAY = 9;
	static final int END_TIME_FRIDAY = 10;
	int friStartHr, friEndHr;
	int friStartMin, friEndMin;
	int friStartAMPM, friEndAMPM;
	int frisCounter;
	int frieCounter;
	
	//saturday block
	TextView sat_text, beginSat_label, endSat_label;
	TextView sat_start_display, sat_end_display; Button sat_start, sat_end;
	static final int START_TIME_SATURDAY = 11;
	static final int END_TIME_SATURDAY = 12;
	int satStartHr, satEndHr;
	int satStartMin, satEndMin;
	int satStartAMPM, satEndAMPM;
	int satsCounter;
	int sateCounter;
	
	//sunday block
	TextView sun_text, beginSun_label, endSun_label;
	TextView sun_start_display, sun_end_display; Button sun_start, sun_end;
	static final int START_TIME_SUNDAY = 13;
	static final int END_TIME_SUNDAY = 14;
	int sunStartHr, sunEndHr;
	int sunStartMin, sunEndMin;
	int sunStartAMPM, sunEndAMPM;
	int sunsCounter;
	int suneCounter;
	
	//contacts
	ArrayList<String> allContacts;
	EditText contactException; 
	Button addContactException;
	ListView contacts;
	
	//locations
	ArrayList<String> allLocations;
	EditText locationException;
	Button addLocationException;
	ListView locations;
	
	Button createProfile;
	
	public void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_profile);
		
		initializeVariables();
		
		//---expiration button
        expiration.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) 
            {
                showDialog(DATE_EXPIRE_ID);
            }
        });
        //---expiration button
        
		//---check boxes
		monday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				if(monday.isChecked()) enableDisableDays("Monday", true);
				else enableDisableDays("Monday", false);
			}
		});		
		tuesday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{			
				if(tuesday.isChecked()) enableDisableDays("Tuesday", true);
				else enableDisableDays("Tuesday", false);			
			}
		});		
		wednesday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{			
				if(wednesday.isChecked()) enableDisableDays("Wednesday", true);
				else enableDisableDays("Wednesday", false);
			}
		});	
		thursday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				if(thursday.isChecked()) enableDisableDays("Thursday", true);
				else enableDisableDays("Thursday", false);
			}
		});		
		friday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{			
				if(friday.isChecked()) enableDisableDays("Friday", true);
				else enableDisableDays("Friday", false);
			}
		});	
		saturday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				if(saturday.isChecked()) enableDisableDays("Saturday", true);
				else enableDisableDays("Saturday", false);
			}
		});
		sunday.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				if(sunday.isChecked()) enableDisableDays("Sunday", true);
				else enableDisableDays("Sunday", false);
			}
		});
		//----check boxes
		
        //---time buttons
		mon_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_MONDAY);
			}
		});
		mon_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_MONDAY);
			}
		});
		tue_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_TUESDAY);
			}
		});
		tue_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_TUESDAY);
			}
		});
		wed_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_WEDNESDAY);
			}
		});
		wed_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_WEDNESDAY);
			}
		});
		thu_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_THURSDAY);
			}
		});
		thu_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_THURSDAY);
			}
		});
		fri_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_FRIDAY);
			}
		});
		fri_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_FRIDAY);
			}
		});
		sat_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_SATURDAY);
			}
		});
		sat_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_SATURDAY);
			}
		});
		sun_start.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(START_TIME_SUNDAY);
			}
		});
		sun_end.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog(END_TIME_SUNDAY);
			}
		});
		//---time buttons
		
		//---contact exceptions 
		addContactException.setOnClickListener(new View.OnClickListener() //add contact button
		{	
			public void onClick(View v) 
			{
				String contact = contactException.getText().toString();
				contactException.setText("");
				allContacts.add(contact);
				refreshContactList();
			}
		});
		contacts.setOnItemClickListener(new OnItemClickListener() //remove item from list when clicked
		{
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) 
			{
				allContacts.remove(position);
				refreshContactList();
			}
		});
		//---contact exceptions
		
		//---location exceptions
		addLocationException.setOnClickListener(new View.OnClickListener() 
		{	
			public void onClick(View v) 
			{
				String location = locationException.getText().toString();
				locationException.setText("");
				allLocations.add(location);
				refreshLocationList();
			}
		});
		locations.setOnItemClickListener(new OnItemClickListener() //for items in the list
		{
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) 
			{
				allLocations.remove(position);
				refreshLocationList();
			}
		});
		//---location exceptions
		
		//---create profile button
		createProfile.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) 
            {
            	Profile profile = new Profile();
            	
            	LocationTaskActivity.profileDATABASE.open();
            	if(profileName.getText().length() == 0) //profile must have a name
            	{
            		Toast.makeText(SettingsAddProfileActivity.this, "You must enter a profile name", Toast.LENGTH_SHORT).show();
            	}
            	else if(LocationTaskActivity.profileDATABASE.isDuplicate(LocationTaskActivity.username, profileName.getText().toString()))
            		Toast.makeText(SettingsAddProfileActivity.this, "A profile with the name " + "\"" + profileName.getText().toString() + "\"" + " already exists. Select a different profile name.", Toast.LENGTH_SHORT).show();
            	//if a start time is selected then an end time MUST be selected and vice versa
            	else if((monday.isChecked() && monsCounter == 0 && monEndHr != 0) || (tuesday.isChecked() && tuesCounter == 0 && tueEndHr != 0) || (wednesday.isChecked() && wedsCounter == 0 && wedEndHr != 0) || (thursday.isChecked() && thusCounter == 0 && thuEndHr != 0) || (friday.isChecked() && frisCounter == 0 && friEndHr != 0) || (saturday.isChecked() && satsCounter == 0 && satEndHr != 0) || (sunday.isChecked() && sunsCounter == 0 && sunEndHr != 0) ||
            			(monday.isChecked() && moneCounter == 0 && monStartHr != 0) || (tuesday.isChecked() && tueeCounter == 0 && tueStartHr != 0) || (wednesday.isChecked() && wedeCounter == 0 && wedStartHr != 0) || (thursday.isChecked() && thueCounter == 0 && thuStartHr != 0) || (friday.isChecked() && frieCounter == 0 && friStartHr != 0) || (saturday.isChecked() && satsCounter == 0 && satStartHr != 0) || (sunday.isChecked() && suneCounter == 0 && sunStartHr != 0))
            	{
            		if(monday.isChecked() &&  moneCounter == 0 && monStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Monday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(monday.isChecked() && monsCounter == 0 && monEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Monday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(tuesday.isChecked() && tueeCounter == 0 && tueStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Tuesday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(tuesday.isChecked() && tuesCounter == 0 && tueEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Tuesday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(wednesday.isChecked() && wedeCounter == 0 && wedStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Wednesday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(wednesday.isChecked() && wedsCounter == 0 && wedEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Wednesday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(thursday.isChecked() && thueCounter == 0 && thuStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Thursday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(thursday.isChecked() && thusCounter == 0 && thuEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Thursday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(friday.isChecked() && frieCounter == 0 && friStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Friday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(friday.isChecked() && frisCounter == 0 && friEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Friday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(saturday.isChecked() && sateCounter == 0 && satStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Saturday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(saturday.isChecked() && satsCounter == 0 && satEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Saturday is missing a start time", Toast.LENGTH_SHORT).show();
            		if(sunday.isChecked() && suneCounter == 0 && sunStartHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Sunday is missing an end time", Toast.LENGTH_SHORT).show();
            		else if(sunday.isChecked() && sunsCounter == 0 && sunEndHr != 0)
            			Toast.makeText(SettingsAddProfileActivity.this, "Sunday is missing a start time", Toast.LENGTH_SHORT).show();            	}
            	else
            	{
            		//profile name and user
	            	profile.setProfileName(profileName.getText().toString());
	            	profile.setProfileUser(LocationTaskActivity.username);
	            	
	            	//enable
	            	profile.setEnabled(true);
	            	
	            	//ringer
	            	if(ringer.getText().toString().compareTo("OFF") == 0)
	            		profile.setRinger(false);
	            	else
	            		profile.setRinger(true);
	            	
	            	//vibrate
	            	if(vibrate.getText().toString().compareTo("OFF") == 0)
	            		profile.setVibrate(false);
	            	else
	            		profile.setVibrate(true);
	            	
	            	//expiration
	            	Time expiration = new Time();
	            	expiration.setMonth(expMonth);
	            	expiration.setDay(expDay);
	            	expiration.setYear(expYear);
	            	profile.setExpiration(expiration);       	

	            	//days
	            	if(monday.isChecked())
	            		profile.addDayException(new Day("Monday", new Time(monStartHr, monStartMin, monStartAMPM), new Time(monEndHr, monEndMin, monEndAMPM)));
	            	if(tuesday.isChecked())
	            		profile.addDayException(new Day("Tuesday", new Time(tueStartHr, tueStartMin, tueStartAMPM), new Time(tueEndHr, tueEndMin, tueEndAMPM)));
	            	if(wednesday.isChecked())
	            		profile.addDayException(new Day("Wednesday", new Time(wedStartHr, wedStartMin, wedStartAMPM), new Time(wedEndHr, wedEndMin, wedEndAMPM)));
	            	if(thursday.isChecked())
	            		profile.addDayException(new Day("Thursday", new Time(thuStartHr, thuStartMin, thuStartAMPM), new Time(thuEndHr, thuEndMin, thuEndAMPM)));
	            	if(friday.isChecked())
	            		profile.addDayException(new Day("Friday", new Time(friStartHr, friStartMin, friStartAMPM), new Time(friEndHr, friEndMin, friEndAMPM)));
	            	if(saturday.isChecked())
	            		profile.addDayException(new Day("Saturday", new Time(satStartHr, satStartMin, satStartAMPM), new Time(satEndHr, satEndMin, satEndAMPM)));
	            	if(sunday.isChecked())
	            		profile.addDayException(new Day("Sunday", new Time(sunStartHr, sunStartMin, sunStartAMPM), new Time(sunEndHr, sunEndMin, sunEndAMPM)));

	            	for(int i = 0; i < allContacts.size(); i++)
	            		profile.addContactException(allContacts.get(i));
	            	
	            	for(int i = 0; i < allLocations.size(); i++)
	            	{
	            		StringTokenizer tokens = new StringTokenizer(allLocations.get(i), " ");
	            		while (tokens.hasMoreTokens()) 
	            			profile.addLocationException(new Location(Double.parseDouble(tokens.nextToken()), Double.parseDouble(tokens.nextToken())));
	            	}
	            	
	            	LocationTaskActivity.profileDATABASE.open();
					LocationTaskActivity.currentProfile.setEnabled(false); //disable the current profile
					LocationTaskActivity.profileDATABASE.updateProfileEnable(LocationTaskActivity.currentProfile); //store that change in the db
					LocationTaskActivity.currentProfile = profile; //get profile from db that was selected from spinner
					LocationTaskActivity.profileDATABASE.storeProfile(profile);
					LocationTaskActivity.profileDATABASE.close();
					
					Intent resultIntent = new Intent();
					setResult(Activity.RESULT_OK, resultIntent);
					finish();
            	}
            	LocationTaskActivity.profileDATABASE.close();
            }
        });
        //---create profile button
	}  
	
	public void initializeVariables()
	{
		profileName = (EditText) findViewById(R.id.profile_name);
		ringer = (ToggleButton) findViewById(R.id.toggleRinger);
		vibrate = (ToggleButton) findViewById(R.id.toggleVibrate);
		 
		expirationDisplay = (TextView) findViewById(R.id.exp_time);
        expiration = (Button) findViewById(R.id.exp_button);
         
		monday = (CheckBox) findViewById(R.id.checkBox_mon);
		tuesday = (CheckBox) findViewById(R.id.checkBox_tue);
		wednesday = (CheckBox) findViewById(R.id.checkBox_wed);
		thursday = (CheckBox) findViewById(R.id.checkBox_thu);
		friday = (CheckBox) findViewById(R.id.checkBox_fri);
		saturday = (CheckBox) findViewById(R.id.checkBox_sat);
		sunday = (CheckBox) findViewById(R.id.checkBox_sun);
		
		//monday block	
		mon_text = (TextView) findViewById(R.id.beginTimeMon_label);
		beginMon_label = (TextView) findViewById(R.id.beginMon_label);
		endMon_label = (TextView) findViewById(R.id.endMon_label);
		mon_start_display = (TextView) findViewById(R.id.beginMon_time);
		mon_end_display = (TextView) findViewById(R.id.endMon_time);
		mon_start = (Button) findViewById(R.id.beginMon_button);
		mon_end = (Button) findViewById(R.id.endMon_button); 
//		if(monStartHr == 0 && monEndHr == 0)
//		{
//			monsCounter++;
//			moneCounter++;
//		}
			
//		monsCounter = 0;
//		moneCounter = 0;
		
		//tuesday block	
		tue_text = (TextView) findViewById(R.id.beginTimeTue_label);
		beginTue_label = (TextView) findViewById(R.id.beginTue_label);
		endTue_label = (TextView) findViewById(R.id.endTue_label);
		tue_start_display = (TextView) findViewById(R.id.beginTue_time);
		tue_end_display = (TextView) findViewById(R.id.endTue_time);
		tue_start = (Button) findViewById(R.id.beginTue_button);
		tue_end = (Button) findViewById(R.id.endTue_button);
//		if(tueStartHr == 0 && tueEndHr == 0)
//		{
//			tuesCounter++;
//			tueeCounter++;
//		}
//		tueCounter = 0;
		
		//wednesday block	
		wed_text = (TextView) findViewById(R.id.beginTimeWed_label);
		beginWed_label = (TextView) findViewById(R.id.beginWed_label);
		endWed_label = (TextView) findViewById(R.id.endWed_label);
		wed_start_display = (TextView) findViewById(R.id.beginWed_time);
		wed_end_display = (TextView) findViewById(R.id.endWed_time);
		wed_start = (Button) findViewById(R.id.beginWed_button);
		wed_end = (Button) findViewById(R.id.endWed_button);
//		if(wedStartHr == 0 && wedEndHr == 0)
//		{
//			wedsCounter++;
//			wedeCounter++;
//		}
//		wedCounter = 0;
		
		//thursday block	
		thu_text = (TextView) findViewById(R.id.beginTimeThu_label);
		beginThu_label = (TextView) findViewById(R.id.beginThu_label);
		endThu_label = (TextView) findViewById(R.id.endThu_label);
		thu_start_display = (TextView) findViewById(R.id.beginThu_time);
		thu_end_display = (TextView) findViewById(R.id.endThu_time);
		thu_start = (Button) findViewById(R.id.beginThu_button);
		thu_end = (Button) findViewById(R.id.endThu_button);
//		if(thuStartHr == 0 && thuEndHr == 0)
//		{
//			thusCounter++;
//			thueCounter++;
//		}
//		thuCounter = 0;
		
		//friday block	
		fri_text = (TextView) findViewById(R.id.beginTimeFri_label);
		beginFri_label = (TextView) findViewById(R.id.beginFri_label);
		endFri_label = (TextView) findViewById(R.id.endFri_label);
		fri_start_display = (TextView) findViewById(R.id.beginFri_time);
		fri_end_display = (TextView) findViewById(R.id.endFri_time);
		fri_start = (Button) findViewById(R.id.beginFri_button);
		fri_end = (Button) findViewById(R.id.endFri_button);
//		if(friStartHr == 0 && friEndHr == 0)
//		{
//			frisCounter++;
//			frieCounter++;
//		}
//		friCounter = 0;
		
		//saturday block	
		sat_text = (TextView) findViewById(R.id.beginTimeSat_label);
		beginSat_label = (TextView) findViewById(R.id.beginSat_label);
		endSat_label = (TextView) findViewById(R.id.endSat_label);
		sat_start_display = (TextView) findViewById(R.id.beginSat_time);
		sat_end_display = (TextView) findViewById(R.id.endSat_time);
		sat_start = (Button) findViewById(R.id.beginSat_button);
		sat_end = (Button) findViewById(R.id.endSat_button);
//		if(satStartHr == 0 && satEndHr == 0)
//		{
//			satsCounter++;
//			sateCounter++;
//		}
//		satCounter = 0;
		
		//sunday block	
		sun_text = (TextView) findViewById(R.id.beginTimeSun_label);
		beginSun_label = (TextView) findViewById(R.id.beginSun_label);
		endSun_label = (TextView) findViewById(R.id.endSun_label);
		sun_start_display = (TextView) findViewById(R.id.beginSun_time);
		sun_end_display = (TextView) findViewById(R.id.endSun_time);
		sun_start = (Button) findViewById(R.id.beginSun_button);
		sun_end = (Button) findViewById(R.id.endSun_button);
//		if(sunStartHr == 0 && sunEndHr == 0)
//		{
//			sunsCounter++;
//			suneCounter++;
//		}
//		sunCounter = 0;
		
		//disable all day blocks by default (must be down here or else the variable above won't have been initialized and app will crash
		enableDisableDays("Monday", false);
		enableDisableDays("Tuesday", false);
		enableDisableDays("Wednesday", false);
		enableDisableDays("Thursday", false);
		enableDisableDays("Friday", false);
		enableDisableDays("Saturday", false);
		enableDisableDays("Sunday", false);
		
		allContacts = new ArrayList<String>();
		contactException = (EditText) findViewById(R.id.editText_contactExs);
		addContactException = (Button) findViewById(R.id.contactExs_button);
		contacts = (ListView) findViewById(R.id.contactExs_List);	
		
		allLocations = new ArrayList<String>();
		locationException = (EditText) findViewById(R.id.editText_locExs);
		addLocationException = (Button) findViewById(R.id.contactLocs_button);
		locations = (ListView) findViewById(R.id.contactLocs_List);
		
		 createProfile = (Button) findViewById(R.id.createProfile_button);	
	}
	
	private void refreshContactList()
	{
		contacts.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, allContacts));
	}
	private void refreshLocationList()
	{
		locations.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, allLocations));
	}
    
	protected Dialog onCreateDialog(int id) 
	{
		switch (id) 
		{
			case DATE_EXPIRE_ID:
			{
//				if(expYear == 0)
					return new DatePickerDialog(this, expirationDatePicker, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
//				else
//					return new DatePickerDialog(this, expirationDatePicker, expYear, expMonth, expDay);
			}
			case START_TIME_MONDAY:
				return new TimePickerDialog(this, monStartTimePicker, monStartHr, monStartMin, false);
			case END_TIME_MONDAY:
				return new TimePickerDialog(this, monEndTimePicker, monEndHr, monEndMin, false);
			case START_TIME_TUESDAY:
				return new TimePickerDialog(this, tueStartTimePicker, tueStartHr, tueStartMin, false);
			case END_TIME_TUESDAY:
				return new TimePickerDialog(this, tueEndTimePicker, tueEndHr, tueEndMin, false);
			case START_TIME_WEDNESDAY:
				return new TimePickerDialog(this, wedStartTimePicker, wedStartHr, wedStartMin, false);
			case END_TIME_WEDNESDAY:
				return new TimePickerDialog(this, wedEndTimePicker, wedEndHr, wedEndMin, false);
			case START_TIME_THURSDAY:
				return new TimePickerDialog(this, thuStartTimePicker, thuStartHr, thuStartMin, false);
			case END_TIME_THURSDAY:
				return new TimePickerDialog(this, thuEndTimePicker, thuEndHr, thuEndMin, false);
			case START_TIME_FRIDAY:
				return new TimePickerDialog(this, friStartTimePicker, friStartHr, friStartMin, false);
			case END_TIME_FRIDAY:
				return new TimePickerDialog(this, friEndTimePicker, friEndHr, friEndMin, false);		
			case START_TIME_SATURDAY:
				return new TimePickerDialog(this, satStartTimePicker, satStartHr, satStartMin, false);
			case END_TIME_SATURDAY:
				return new TimePickerDialog(this, satEndTimePicker, satEndHr, satEndMin, false);
			case START_TIME_SUNDAY:
				return new TimePickerDialog(this, sunStartTimePicker, sunStartHr, sunStartMin, false);
			case END_TIME_SUNDAY:
				return new TimePickerDialog(this, sunEndTimePicker, sunEndHr, sunEndMin, false);
		}
		return null;

	}

	private DatePickerDialog.OnDateSetListener expirationDatePicker = new DatePickerDialog.OnDateSetListener() 
    {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) 
        {
            expYear = year;
            expMonth = monthOfYear + 1;
            expDay = dayOfMonth;
        	expirationDisplay.setText((expMonth) + "-" + expDay + "-" + expYear);
        } 
    };
    private TimePickerDialog.OnTimeSetListener monStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			monStartHr = hour;
			if(hour >= 12) { monStartAMPM = 1; monStartHr -= 12; } else monStartAMPM = 0;
			if(hour == 0 || hour == 12) monStartHr = 12;  
			monStartMin = min;
			monsCounter++; //keeps track to make sure that start and end times were both set
			
			//display
			text += monStartHr + ":";
			if(monStartMin < 10) text += "0" + monStartMin; else text += monStartMin;
			if(monStartAMPM == 0) text += "a.m."; else text += "p.m.";
			mon_start_display.setText(text);
//			
//			if(monCounter != 0 && mon_end_display.getText().toString().compareTo("Not set") != 0)
//				monCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener monEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			monEndHr = hour;
			if(hour >= 12) { monEndAMPM = 1; monEndHr -= 12; } else monEndAMPM = 0;
			if(hour == 0 || hour == 12) monEndHr = 12; 
			monEndMin = min;
			moneCounter++;
			
			//display
			text += monEndHr + ":";
			if(monEndMin < 10) text += "0" + monEndMin; else text += monEndMin;
			if(monEndAMPM == 0) text += "a.m."; else text += "p.m.";
			mon_end_display.setText(text);
			
//			if(monCounter != 0 && mon_start_display.getText().toString().compareTo("Not set") != 0)
//				monCounter++;
		}
	};	
	private TimePickerDialog.OnTimeSetListener tueStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			tueStartHr = hour;
			if(hour >= 12) { tueStartAMPM = 1; tueStartHr -= 12; } else tueStartAMPM = 0;
			if(hour == 0 || hour == 12) tueStartHr = 12;  
			tueStartMin = min;
			tuesCounter++;

			text += tueStartHr + ":";
			if(tueStartMin < 10) text += "0" + tueStartMin; else text += tueStartMin;
			if(tueStartAMPM == 0) text += "a.m."; else text += "p.m.";
			tue_start_display.setText(text);
			
//			if(tueCounter != 0 && tue_end_display.getText().toString().compareTo("Not set") != 0)
//				tueCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener tueEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			tueEndHr = hour;
			if(hour >= 12) { tueEndAMPM = 1; tueEndHr -= 12; } else tueEndAMPM = 0;
			if(hour == 0 || hour == 12) tueEndHr = 12; 
			tueEndMin = min;
			tueeCounter++;
			
			text += tueEndHr + ":";
			if(tueEndMin < 10) text += "0" + tueEndMin; else text += tueEndMin;
			if(tueEndAMPM == 0) text += "a.m."; else text += "p.m.";
			tue_end_display.setText(text);
			
//			if(tueCounter != 0 && tue_start_display.getText().toString().compareTo("Not set") != 0)
//				tueCounter++;
		}
	};
	private TimePickerDialog.OnTimeSetListener wedStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			wedStartHr = hour;
			if(hour >= 12) { wedStartAMPM = 1; wedStartHr -= 12; } else wedStartAMPM = 0;
			if(hour == 0 || hour == 12) wedStartHr = 12;  
			wedStartMin = min;
			wedsCounter++;
			
			text += wedStartHr + ":";
			if(wedStartMin < 10) text += "0" + wedStartMin; else text += wedStartMin;
			if(wedStartAMPM == 0) text += "a.m."; else text += "p.m.";
			wed_start_display.setText(text);
			
//			if(wedCounter != 0  && wed_end_display.getText().toString().compareTo("Not set") != 0)
//				wedCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener wedEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			wedEndHr = hour;
			if(hour >= 12) { wedEndAMPM = 1; wedEndHr -= 12; } else wedEndAMPM = 0;
			if(hour == 0 || hour == 12) wedEndHr = 12;  
			wedEndMin = min;
			wedeCounter++;
			
			text += wedEndHr + ":";
			if(wedEndMin < 10) text += "0" + wedEndMin; else text += wedEndMin;
			if(wedEndAMPM == 0) text += "a.m."; else text += "p.m.";
			wed_end_display.setText(text);
			
//			if(wedCounter != 0 && wed_start_display.getText().toString().compareTo("Not set") != 0)
//				wedCounter++;
		}
	};
	private TimePickerDialog.OnTimeSetListener thuStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			thuStartHr = hour;
			if(hour >= 12) { thuStartAMPM = 1; thuStartHr -= 12; } else thuStartAMPM = 0;
			if(hour == 0 || hour == 12) thuStartHr = 12;  
			thuStartMin = min;
			thusCounter++;
			
			text += thuStartHr + ":";
			if(thuStartMin < 10) text += "0" + thuStartMin; else text += thuStartMin;
			if(thuStartAMPM == 0) text += "a.m."; else text += "p.m.";
			thu_start_display.setText(text);
			
//			if(thuCounter != 0 && thu_end_display.getText().toString().compareTo("Not set") != 0)
//				thuCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener thuEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			thuEndHr = hour;
			if(hour >= 12) { thuEndAMPM = 1; thuEndHr -= 12; } else thuEndAMPM = 0;
			if(hour == 0 || hour == 12) thuEndHr = 12;  
			thuEndMin = min;
			thueCounter++;
			
			text += thuEndHr + ":";
			if(thuEndMin < 10) text += "0" + thuEndMin; else text += thuEndMin;
			if(thuEndAMPM == 0) text += "a.m."; else text += "p.m.";
			thu_end_display.setText(text); 
//			
//			if(thuCounter != 0 && thu_start_display.getText().toString().compareTo("Not set") != 0)
//				thuCounter++;
		}
	};
	private TimePickerDialog.OnTimeSetListener friStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			friStartHr = hour;
			if(hour >= 12) { friStartAMPM = 1; friStartHr -= 12; } else friStartAMPM = 0;
			if(hour == 0 || hour == 12) friStartHr = 12;  
			friStartMin = min;
			frisCounter++;
			
			text += friStartHr + ":";
			if(friStartMin < 10) text += "0" + friStartMin; else text += friStartMin;
			if(friStartAMPM == 0) text += "a.m."; else text += "p.m.";
			fri_start_display.setText(text);
			
//			if(friCounter != 0 && fri_end_display.getText().toString().compareTo("Not set") != 0)
//				friCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener friEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			friEndHr = hour;
			if(hour >= 12) { friEndAMPM = 1; friEndHr -= 12; } else friEndAMPM = 0;
			if(hour == 0 || hour == 12) friEndHr = 12;  
			friEndMin = min;
			frieCounter++;
			
			text += friEndHr + ":";
			if(friEndMin < 10) text += "0" + friEndMin; else text += friEndMin;
			if(friEndAMPM == 0) text += "a.m."; else text += "p.m.";
			fri_end_display.setText(text);
			
//			if(friCounter != 0 && fri_start_display.getText().toString().compareTo("Not set") != 0)
//				friCounter++;
		}
	};
	private TimePickerDialog.OnTimeSetListener satStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			satStartHr = hour;
			if(hour >= 12) { satStartAMPM = 1; satStartHr -= 12; } else satStartAMPM = 0;
			if(hour == 0 || hour == 12) satStartHr = 12;  
			satStartMin = min;
			satsCounter++;
			
			text += satStartHr + ":";
			if(satStartMin < 10) text += "0" + satStartMin; else text += satStartMin;
			if(satStartAMPM == 0) text += "a.m."; else text += "p.m.";
			sat_start_display.setText(text);
			
//			if(satCounter != 0 && sat_end_display.getText().toString().compareTo("Not set") != 0)
//				satCounter--;
		}
	};
	private TimePickerDialog.OnTimeSetListener satEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			satEndHr = hour;
			if(hour >= 12) { satEndAMPM = 1; satEndHr -= 12; } else satEndAMPM = 0;
			if(hour == 0 || hour == 12) satEndHr = 12;  
			satEndMin = min;
			sateCounter++;
			
			text += satEndHr + ":";
			if(satEndMin < 10) text += "0" + satEndMin; else text += satEndMin;
			if(satEndAMPM == 0) text += "a.m."; else text += "p.m.";
			sat_end_display.setText(text);
			
//			if(satCounter != 0 && sat_start_display.getText().toString().compareTo("Not set") != 0)
//				satCounter++;
		}
	};
	private TimePickerDialog.OnTimeSetListener sunStartTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			sunStartHr = hour;
			if(hour >= 12) { sunStartAMPM = 1; sunStartHr -= 12; } else sunStartAMPM = 0;
			if(hour == 0 || hour == 12) sunStartHr = 12;  
			sunStartMin = min;
			sunsCounter++;
			
			text += sunStartHr + ":";
			if(sunStartMin < 10) text += "0" + sunStartMin; else text += sunStartMin;
			if(sunStartAMPM == 0) text += "a.m."; else text += "p.m.";
			sun_start_display.setText(text);
			
//			if(sunCounter != 0 && sun_end_display.getText().toString().compareTo("Not set") != 0)
//				sunCounter--;
		}
	};	
	private TimePickerDialog.OnTimeSetListener sunEndTimePicker = new TimePickerDialog.OnTimeSetListener() 
	{						
		public void onTimeSet(TimePicker arg0, int hour, int min) 
		{
			String text = "";
			sunEndHr = hour;
			if(hour >= 12) { sunEndAMPM = 1; sunEndHr -= 12; } else sunEndAMPM = 0;
			if(hour == 0 || hour == 12) sunEndHr = 12;  
			sunEndMin = min;
			suneCounter++;
			
			text += sunEndHr + ":";
			if(sunEndMin < 10) text += "0" + sunEndMin; else text += sunEndMin;
			if(sunEndAMPM == 0) text += "a.m."; else text += "p.m.";
			sun_end_display.setText(text);
			
//			if(sunCounter != 0 && sun_start_display.getText().toString().compareTo("Not set") != 0)
//				sunCounter++;
		}
	};	
	
	public void enableDisableDays(String day, boolean set) //enable/disable day blocks
	{
		if(day.compareTo("Monday") == 0)
		{
			mon_text.setEnabled(set);
			beginMon_label.setEnabled(set);
			endMon_label.setEnabled(set);
			mon_start_display.setEnabled(set);
			mon_end_display.setEnabled(set);
			mon_start.setEnabled(set);
			mon_end.setEnabled(set);
			if(monStartHr == 0)
				mon_start_display.setText("Not set");
			if(monEndHr == 0)
				mon_end_display.setText("Not set");
//			monCounter = 0;
		}
		else if(day.compareTo("Tuesday") == 0)
		{
			tue_text.setEnabled(set);
			beginTue_label.setEnabled(set);
			endTue_label.setEnabled(set);
			tue_start_display.setEnabled(set);
			tue_end_display.setEnabled(set);
			tue_start.setEnabled(set);
			tue_end.setEnabled(set);
//			tueCounter = 0;
		}
		else if(day.compareTo("Wednesday") == 0)
		{
			wed_text.setEnabled(set);
			beginWed_label.setEnabled(set);
			endWed_label.setEnabled(set);
			wed_start_display.setEnabled(set);
			wed_end_display.setEnabled(set);
			wed_start.setEnabled(set);
			wed_end.setEnabled(set);
//			wedCounter = 0;
		}
		else if(day.compareTo("Thursday") == 0)
		{
			thu_text.setEnabled(set);
			beginThu_label.setEnabled(set);
			endThu_label.setEnabled(set);
			thu_start_display.setEnabled(set);
			thu_end_display.setEnabled(set);
			thu_start.setEnabled(set);
			thu_end.setEnabled(set);
//			thuCounter = 0;
		}
		else if(day.compareTo("Friday") == 0)
		{
			fri_text.setEnabled(set);
			beginFri_label.setEnabled(set);
			endFri_label.setEnabled(set);
			fri_start_display.setEnabled(set);
			fri_end_display.setEnabled(set);
			fri_start.setEnabled(set);
			fri_end.setEnabled(set);
//			friCounter = 0;
		}
		else if(day.compareTo("Saturday") == 0)
		{
			sat_text.setEnabled(set);
			beginSat_label.setEnabled(set);
			endSat_label.setEnabled(set);
			sat_start_display.setEnabled(set);
			sat_end_display.setEnabled(set);
			sat_start.setEnabled(set);
			sat_end.setEnabled(set);
//			satCounter = 0;
		}
		else if(day.compareTo("Sunday") == 0)
		{
			sun_text.setEnabled(set);
			beginSun_label.setEnabled(set);
			endSun_label.setEnabled(set);
			sun_start_display.setEnabled(set);
			sun_end_display.setEnabled(set);
			sun_start.setEnabled(set);
			sun_end.setEnabled(set);
//			sunCounter = 0;
		}
	}
}
