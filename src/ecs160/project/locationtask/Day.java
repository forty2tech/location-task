package ecs160.project.locationtask;

public class Day 
{
	public Day() {}
	
	public Day(String d, Time s, Time f)
	{
		day = d;
		start = s;
		finish = f;
	}
	
	public Day(String d) //Day constructor used when the user only sets the day
	{
		day = d;
		start = new Time(0, 0, 0);
		finish = new Time(0, 0, 0);
	}
	
	public String getDay()
	{
		return day;
	}
	
	public void setDay(String s)
	{
		day = s;
	}
	
	public Time getStart()
	{
		return start;
	}
	
	public void setStart(Time t)
	{
		start = t;
	}
	
	public Time getFinish()
	{
		return finish;
	}
	
	public void setFinish(Time t)
	{
		finish = t;
	}
	
	private String day;
	private Time start;
	private Time finish;
}
