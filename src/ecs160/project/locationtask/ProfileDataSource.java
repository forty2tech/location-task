package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ProfileDataSource 
{
	// Database fields
	private SQLiteDatabase database2;
	private MySQLiteHelper2 dbHelper;

	public ProfileDataSource(Context context) 
	{
		dbHelper = new MySQLiteHelper2(context);
	}

	public void open() throws SQLException 
	{
		database2 = dbHelper.getWritableDatabase();
	}

	public void close() 
	{
		dbHelper.close();
	}
	
//	public void updateProfile(Profile p) 
//	{		
//		ContentValues cv = new ContentValues();
//		cv.put(MySQLiteHelper2.PROFILE_ENABLED, p.getEnabled());
//		cv.put(MySQLiteHelper2.RINGER_ENABLED, p.getRinger());
//		cv.put(MySQLiteHelper2.VIBRATE_ENABLED, p.getVibrate());
//		
//		String days = "";
//		ArrayList<Day> allDays = p.getDaysOfTheWeek();
//		for(int i = 0; i <allDays.size(); i++)
//		{
//			days += allDays.get(i).getDay() + " " + 
//					allDays.get(i).getStart().getHour() + " " +
//					allDays.get(i).getStart().getMinute() + " " + 
//					allDays.get(i).getStart().getAMPM() + " " +
//					allDays.get(i).getFinish().getHour() + " " +
//					allDays.get(i).getFinish().getMinute() + " " + 
//					allDays.get(i).getFinish().getAMPM() + " " + ";";
//		}
//		cv.put(MySQLiteHelper2.DAYS, days);
//		
//		String locations = "";
//		ArrayList<Location> locationExceptions = p.getLocationExceptions();
//		for(int i = 0; i < locationExceptions.size(); i++)
//		{
//			locations += locationExceptions.get(i).getLatitude() + " " 
//						+ locationExceptions.get(i).getLongitude() + ";";
//		}
//		cv.put(MySQLiteHelper2.LOCATIONS, locations);
//		
//		String contacts = "";
//		ArrayList<String> contactExceptions = p.getContactExceptions();
//		for(int i = 0; i < contactExceptions.size(); i++)
//		{
//			contacts += contactExceptions.get(i) + ";";
//		}
//		cv.put(MySQLiteHelper2.CONTACTS, contacts);
//		
//		String expiration = "" + p.getExpiration().getMonth() + " " + p.getExpiration().getDay() + " " + p.getExpiration().getYear();
//		cv.put(MySQLiteHelper2.EXPIRATION, expiration);
//		
//		String profileName = p.getProfileName();
//		String profileUser = p.getProfileUser();
//		String args[] = { profileName, profileUser };
//		
//        database2.update(MySQLiteHelper2.PROFILES_TABLE, cv, MySQLiteHelper2.PROFILE_NAME + "=? and " +
//        														MySQLiteHelper2.USER_ID + "=?" , args);
//	}
	
	public void updateProfileEnable(Profile p)
	{
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper2.PROFILE_ENABLED, p.getEnabled());
		
		String profileName = p.getProfileName();
		String profileUser = p.getProfileUser();	
		String args[] = { profileName, profileUser };
		
        database2.update(MySQLiteHelper2.PROFILES_TABLE, cv, MySQLiteHelper2.PROFILE_NAME + "=? and " +
        														MySQLiteHelper2.USER_ID + "=?" , args);
	}

	public void storeProfile(Profile profile)//(String user, String msg, double lat, double lon, boolean read) 
	{ 
		String locations = "";
		ArrayList<Location> locationExceptions = profile.getLocationExceptions();
		
		for(int i = 0; i < locationExceptions.size(); i++)
		{
			locations += locationExceptions.get(i).getLatitude() + " " + locationExceptions.get(i).getLongitude() + ";";
		}
		 
		String contacts = "";
		ArrayList<String> contactExceptions = profile.getContactExceptions();
		for(int i = 0; i < contactExceptions.size(); i++)
		{
			contacts += contactExceptions.get(i) + ";";
		}
	
		String days = "";
		ArrayList<Day> allDays = profile.getDaysOfTheWeek();
		for(int i = 0; i <allDays.size(); i++)
		{
			days += allDays.get(i).getDay() + " " + 
					allDays.get(i).getStart().getHour() + " " +
					allDays.get(i).getStart().getMinute() + " " + 
					allDays.get(i).getStart().getAMPM() + " " +
					allDays.get(i).getFinish().getHour() + " " +
					allDays.get(i).getFinish().getMinute() + " " + 
					allDays.get(i).getFinish().getAMPM() + " " + ";";
		}
		
		String expiration = "" + profile.getExpiration().getMonth() + " " + profile.getExpiration().getDay() + " " + profile.getExpiration().getYear();
		
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper2.USER_ID, profile.getProfileUser());
		values.put(MySQLiteHelper2.PROFILE_NAME, profile.getProfileName());
		values.put(MySQLiteHelper2.PROFILE_ENABLED, profile.getEnabled());
		values.put(MySQLiteHelper2.RINGER_ENABLED, profile.getRinger());
		values.put(MySQLiteHelper2.VIBRATE_ENABLED, profile.getVibrate());
		values.put(MySQLiteHelper2.DAYS, days);
		values.put(MySQLiteHelper2.LOCATIONS, locations);
		values.put(MySQLiteHelper2.CONTACTS, contacts);
		values.put(MySQLiteHelper2.EXPIRATION, expiration);
		values.put(MySQLiteHelper2.ORIGINAL_RINGER, profile.getOriginalRinger());
		values.put(MySQLiteHelper2.ORIGINAL_VIB_RING, profile.getOriginalVibrateRing());
		values.put(MySQLiteHelper2.ORIGINAL_VIB_NOTI, profile.getOriginalVibrateNoti());
		database2.insert(MySQLiteHelper2.PROFILES_TABLE, null, values);
	}

	public ArrayList<Profile> getAllProfiles(String user)
	{
		ArrayList<Profile> profiles = new ArrayList<Profile>();
	
		String args[] = { user };
		Cursor cursor = database2.query(MySQLiteHelper2.PROFILES_TABLE, null, MySQLiteHelper2.USER_ID + " = ?", 
																		   args, null, null, null);
		cursor.moveToFirst(); //moves cursor to the first row
		while(!cursor.isAfterLast())
		{
			profiles.add(cursorToProfile(cursor)); //convert each query row to a message and store it in the list
			cursor.moveToNext(); //advance the cursor to the next row
		}
		cursor.close();
		return profiles;
	}					
	
	public Profile cursorToProfile(Cursor cursor)
	{
		Profile profile = new Profile();
		
		//profile name and user
		profile.setProfileName(cursor.getString(0));
		profile.setProfileUser(cursor.getString(1));
		
		//enable
		if(cursor.getInt(2) == 1)
			profile.setEnabled(true);
		else
			profile.setEnabled(false);
		
		//ringer
		if(cursor.getInt(3) == 1)
			profile.setRinger(true);
		else
			profile.setRinger(false);
		
		//vibrate
		if(cursor.getInt(4) == 1)
			profile.setVibrate(true);
		else
			profile.setVibrate(false);
		
		//days
		String days = cursor.getString(5);
		StringTokenizer dayTokens = new StringTokenizer(days, ";");
		while (dayTokens.hasMoreTokens()) 
		{
			Day d = new Day(); 
			StringTokenizer singleDays = new StringTokenizer(dayTokens.nextToken(), " ");
			
			while(singleDays.hasMoreElements())
			{
				d.setDay(singleDays.nextToken());
				d.setStart(new Time(Integer.parseInt(singleDays.nextToken()), Integer.parseInt(singleDays.nextToken()), Integer.parseInt(singleDays.nextToken())));
				d.setFinish(new Time(Integer.parseInt(singleDays.nextToken()), Integer.parseInt(singleDays.nextToken()), Integer.parseInt(singleDays.nextToken())));
			}
			
			profile.addDayException(d);
		}
		
		//locations
		String locations = cursor.getString(6);
		StringTokenizer locationTokens = new StringTokenizer(locations, ";");
		while (locationTokens.hasMoreTokens()) {
			int count = 0;
			double la = 0;
			double lo = 0;			
			
			StringTokenizer locationPairs = new StringTokenizer(locationTokens.nextToken(), " ");
				
			while (locationPairs.hasMoreTokens()) 
			{
				if (0 == count) la = Double.parseDouble(locationPairs.nextToken());
				else if (1 == count) lo = Double.parseDouble(locationPairs.nextToken());
				count++;
			}
			profile.addLocationException(new Location(la, lo));
		}
		
		//contacts
		String contacts = cursor.getString(7);
		StringTokenizer contactTokens = new StringTokenizer(contacts, ";");
		while (contactTokens.hasMoreTokens()) {
			profile.addContactException(contactTokens.nextToken());
		}
		
		//expiration
		String expiration = cursor.getString(8);
		StringTokenizer tokens = new StringTokenizer(expiration, " ");
		Time time = new Time();
		int count = 0;
		while (tokens.hasMoreTokens()) {
			if(count == 0)
				time.setMonth(Integer.parseInt(tokens.nextToken()));
			else if(count == 1)
				time.setDay(Integer.parseInt(tokens.nextToken()));
			else if(count == 2)
				time.setYear(Integer.parseInt(tokens.nextToken()));
			count++;			
		}
		profile.setExpiration(time);
		
		//original ringer
		profile.setOriginalRinger(cursor.getInt(9));
		
		//original vibrate ringer
		profile.setOriginalVibrateRing(cursor.getInt(10));
		
		//original vibrate notification
		profile.setOriginalVibrateNoti(cursor.getInt(11));
		
		return profile;
	}
	
	public boolean isDuplicate(String user, String name)
	{
		String args[] = { user, name };
		Cursor cursor = database2.query(MySQLiteHelper2.PROFILES_TABLE, null, MySQLiteHelper2.USER_ID + " = ?" + " and " +
																				MySQLiteHelper2.PROFILE_NAME + " = ?", 
																				args, null, null, null);
		if(cursor.getCount() > 0)
			return true;
		else
			return false;
	}	
	
	public Profile getProfile(String user, String name)
	{
		Profile profile = new Profile();
		String args[] = { user, name };
		Cursor cursor = database2.query(MySQLiteHelper2.PROFILES_TABLE, null, MySQLiteHelper2.USER_ID + " = ?" + " and " +
																		       MySQLiteHelper2.PROFILE_NAME + " = ?", 
																		       args, null, null, null);
		
		cursor.moveToFirst();
		profile = cursorToProfile(cursor);
		cursor.close();
		return profile;
	}
	
	public String getEnabledProfile(String user)
	{
		String args[] = { user, String.valueOf(1) };
		Cursor cursor = database2.query(MySQLiteHelper2.PROFILES_TABLE, null, MySQLiteHelper2.USER_ID + " = ?" + " and " +
			       																MySQLiteHelper2.PROFILE_ENABLED + " = ?", 
																		       args, null, null, null);

		if(cursor.getCount() == 0)
			return "";
		else
		{
			cursor.moveToFirst();
			String profile = cursor.getString(0);
			cursor.close();
			return profile;
		}					
	}
	
	public void deleteProfile(String user, String name) 
	{
		String args[] = { user, name };
		
		database2.delete(MySQLiteHelper2.PROFILES_TABLE, MySQLiteHelper2.USER_ID + " = ? and " +
													  MySQLiteHelper2.PROFILE_NAME + " = ?", args);
	}
}