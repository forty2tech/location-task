package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Calendar;

import ecs160.project.locationtask.TaskAdapter.ViewHolder;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TaskAdapter extends ArrayAdapter<Task> {
	private ArrayList<Task> items;
    private Activity context;
    private boolean isFrom;
	
    static class ViewHolder {
		public TextView toptext;
		public TextView bottomtext;
		public ImageView icon;
	}
    
    public TaskAdapter(Activity context, ArrayList<Task> items,boolean isfrom) {
            super(context, R.layout.task, items);
            this.context = context;
            this.items = items;
            this.isFrom = isfrom;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	    
    	    View rowView = convertView;
    	    
            if (rowView == null) {
            	LayoutInflater inflater = context.getLayoutInflater();
    			rowView = inflater.inflate(R.layout.task, null);
    			
    			ViewHolder viewHolder = new ViewHolder();
    			viewHolder.toptext = (TextView) rowView.findViewById(R.id.task_toptext);
    			viewHolder.bottomtext = (TextView) rowView.findViewById(R.id.task_bottomtext);
    			viewHolder.icon = (ImageView) rowView.findViewById(R.id.task_icon);
    			rowView.setTag(viewHolder);
            }
           
            
            ViewHolder holder = (ViewHolder) rowView.getTag();
            Task m = items.get(position);
            String from = "From ";
            String to = "To ";
            Time checkTime = m.getTaskBegin();
            if (checkTime.getYear() > 0){
            	
	            String raw_sender = m.getSender();
	            Log.i("TASK ADAPTER","raw_sender = "+raw_sender);
	            if (isFrom){
	            	if (raw_sender.length() > 10){
	            		String mod_sender = raw_sender.substring(0, 1) + "-(" + raw_sender.substring(1, 4) + ")-"
		            		                + raw_sender.substring(4, 7) + "-" + raw_sender.substring(7);
	            		from += mod_sender + " :";
	            	}
	            	else
	            		from += raw_sender + ":";
		           holder.toptext.setText(from);
	            }
	            else{
	            	to += raw_sender + ":";
	            	holder.toptext.setText(to);
	            }
	            holder.bottomtext.setText(m.getTextData());
	            
	            Time endTime = m.getTaskEnd();
	            Boolean cur_active = true;
	            final Calendar c = Calendar.getInstance();
			    int cur_Year = c.get(Calendar.YEAR);
			    int cur_Month = c.get(Calendar.MONTH) + 1;
			    int cur_Day = c.get(Calendar.DAY_OF_MONTH);
			    int cur_Hr = c.get(Calendar.HOUR_OF_DAY);
			    int cur_Min = c.get(Calendar.MINUTE);
				
//			    Log.i("VALID","CURYEAR = "+Integer.toString(cur_Year));
//			    Log.i("VALID","CURMONTH = "+Integer.toString(cur_Month));
//			    Log.i("VALID","CURDAY = "+Integer.toString(cur_Day));
//			    Log.i("VALID","CURHR = "+Integer.toString(cur_Hr));
//			    Log.i("VALID","CURMIN = "+Integer.toString(cur_Min));
			    
			 // Check active status of Task
	 			if (cur_Year > endTime.getYear())
	 				cur_active = false;
	 			else if (cur_Year < endTime.getYear())
	 				cur_active = true;
	 			else{			// same year
	 				if (cur_Month > endTime.getMonth())
	 					cur_active = false;
	 				else if (cur_Month < endTime.getMonth())
	 					cur_active = true;
	 				else{		// same month
	 					if (cur_Day > endTime.getDay())
	 						cur_active = false;
	 					else if (cur_Day < endTime.getDay())
	 						cur_active = true;
	 					else{	// same day
	 						if (cur_Hr > endTime.getHour())
	 							cur_active = false;
	 						else if (cur_Hr < endTime.getHour())
	 							cur_active = true;
	 						else{//same hr
	 							if (cur_Min > endTime.getMinute())
	 								cur_active = false;
	 							else 
	 								cur_active = true;
	 						}
	 					}
	 				}
	 			}
	            
	            if (cur_active)
	            	holder.icon.setImageResource(R.drawable.active);
	            else
	            	holder.icon.setImageResource(R.drawable.inactive);
            }
		    else{
		    	holder.toptext.setText("");
		        holder.bottomtext.setText("No Task");
		        holder.icon.setImageResource(R.drawable.nonreply);
		    }
            
            return rowView;
    }
}
