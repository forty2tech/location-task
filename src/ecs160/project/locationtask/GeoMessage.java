package ecs160.project.locationtask;

import java.net.URL;

public class GeoMessage {
	
	private String url;
	private String sender;
	private String textData;
	private Location geoLocation;
	private boolean noGeoMsgflag;
	
	public GeoMessage(){
		this.url = null;
		this.sender = "";
		this.textData = "";
		this.geoLocation = new Location(0.0,0.0);
		noGeoMsgflag = false;
	}
	
	public GeoMessage(boolean flag)
	{
		noGeoMsgflag = flag;
	}
	
	public GeoMessage(String purl, String psender, String pdata, Location ploc){
		this.url = purl;
		this.sender = psender;
		this.textData = pdata;
		this.geoLocation = new Location(ploc.getLatitude(),ploc.getLongitude());
		noGeoMsgflag = false;
	}
	
	public void setPathURL(String purl){
		this.url = purl;
	}
	
	public void setSender(String psender){ 
		this.sender = psender;
	}
	
	public void setTextdata(String pdata){
		this.textData = pdata;
	}
	
	public void setGeoLocation(Location ploc){
		this.geoLocation = ploc;
	}
	
	public String getPathURL(){
		return this.url;
	}
	
	public String getSender(){
		return this.sender ;
	}
	
	public String getTextdata(){
		return this.textData;
	}
	
	public Location getGeoLocation(){
		return this.geoLocation ;
	}
	
	public boolean getNoGeoFlag(){
		return noGeoMsgflag;
	}
}
