package ecs160.project.locationtask;

public class Message extends Communication
{
	public Message() {}
	
	public Message(String text, Location loc)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = false;
		noMsg_flag = false;
	}
	
	public Message(String text, Location loc,String pSender)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = false;
		super.setSender(pSender);
		noMsg_flag = false;
	}
	
	public Message(String text, Location loc, boolean read)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = read;
		noMsg_flag = false;
	}
	
	public Message(String text, Location loc,String pSender,boolean read)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = read;
		super.setSender(pSender);
		noMsg_flag = false;
	}
	
	public Message(String text, Location loc, boolean read, boolean flag)
	{
		super.setTextData(text);
		super.setLocation(loc);
		messageRead = read;
		noMsg_flag = flag;
	}
	
	public boolean getMessageRead()
	{
		return messageRead;
	}
	
	public void setMessageRead(boolean val)
	{
		messageRead = val;
	}
	
	public boolean getNoMsgFlag()
	{
		return noMsg_flag;
	}
	
	private boolean messageRead;
	private boolean noMsg_flag;
}