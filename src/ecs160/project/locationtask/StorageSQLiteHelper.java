package ecs160.project.locationtask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class StorageSQLiteHelper extends SQLiteOpenHelper {

	public static final String COMMUNICATION_TABLE = "storage";
	public static final String TYPE = "type"; //1 = message, 2 = task, 3 = query, 4 = geo_msg
	public static final String COLUMN_ID = "_id";
	public static final String USER_ID = "user";
	public static final String SENDER_ID = "sender";	
	public static final String MESSAGE = "communication";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String HAS_READ = "hasRead";
	    
	public static final String COMMUNICATION_ACTIVE = "taskActive";
	public static final String BEGIN_YEAR = "beginYear";
	public static final String BEGIN_MON = "beginMonth";
	public static final String BEGIN_DAY = "beginDay";
	public static final String BEGIN_HOUR = "beginHour";
	public static final String BEGIN_MIN = "beginMin";
	public static final String BEGIN_SEC = "beginSec";
	public static final String END_YEAR = "endYear";
	public static final String END_MON = "endMonth";
	public static final String END_DAY = "endDay";
	public static final String END_HOUR = "endHour";
	public static final String END_MIN = "endMin";
	public static final String END_SEC = "endSec";

	public static final String QUERY_REPLY = "queryReply";
	public static final String QUERY_REPLIED = "queryReplied";

	public static final String IMAGE_URL = "imageURL";
	
	private static final String DATABASE_NAME = "storageCommunication.db";
	private static final int DATABASE_VERSION = 2;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + COMMUNICATION_TABLE + "( " + USER_ID + " text, " + //0
																						   SENDER_ID + " text, " + //1
																						   TYPE + " integer, " + //2
																						   MESSAGE + " text, " + //3
																						   LATITUDE + " real, " + //4
																						   LONGITUDE + " real, " + //5
																						   HAS_READ + " integer, " + //6
																						   
																						   COMMUNICATION_ACTIVE + " integer, " + //7
																						   BEGIN_YEAR + " integer, " + //8
																						   BEGIN_MON + " integer, " + //9
																						   BEGIN_DAY + " integer, " + //10
																						   BEGIN_HOUR + " integer, " + //11
																						   BEGIN_MIN + " integer, " + //12
																						   BEGIN_SEC + " integer, " + //13
																						   END_YEAR + " integer, " + //14
																						   END_MON + " integer, " + //15
																						   END_DAY + " integer, " + //16
																						   END_HOUR + " integer, " + //17
																						   END_MIN + " integer, " + //18
																						   END_SEC + " integer, " + //19
																						   
																						   QUERY_REPLY + " text, " + //20
																						   QUERY_REPLIED + " integer, " + //21
																						   IMAGE_URL + " text, " + //22																				   	   
																						   COLUMN_ID + " integer primary key autoIncrement); ";																				
	
	public StorageSQLiteHelper(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) 
	{
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + COMMUNICATION_TABLE);
		onCreate(db);
	}

}
