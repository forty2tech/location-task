package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class FavoritePage extends ListActivity {
	private int numOfPlace =0;
	private ArrayList<Favorite> place_list = new ArrayList<Favorite>();
	private FavoriteAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState); 
		LocationTaskActivity.favDATABASE.open(); 
		place_list.add(new Favorite(true));

		// Receive and extras bundle -- get list of message
	    numOfPlace = LocationTaskActivity.favDATABASE.getTotalNumberFavoritePlace(LocationTaskActivity.username);
		if (numOfPlace > 0)
			place_list = LocationTaskActivity.favDATABASE.getAllFavoriteLocation(LocationTaskActivity.username);
			
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.new_back_function, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		
		this.adapter = new FavoriteAdapter(this, place_list);
        setListAdapter(this.adapter);
		LocationTaskActivity.favDATABASE.close();
        
        Button backButton = (Button)findViewById(R.id.fav_back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(FavoritePage.this,Home.class);
				FavoritePage.this.startActivity(i);
			}
		});
		
		Button addNewButton = (Button)findViewById(R.id.fav_new_button);
		addNewButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(FavoritePage.this,AddNewFavorite.class);
				FavoritePage.this.startActivity(i);
			}
		});
	}// end onCreate
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfPlace > 0){
			
			//Inser code to process data
			Double cur_fav_lat  = place_list.get(position).getLocation().getLatitude();
			Double cur_fav_long = place_list.get(position).getLocation().getLongitude();
			String fav_name = place_list.get(position).getName();
			
			Bundle fav_bundle = new Bundle();
			fav_bundle.putString("fav_name", fav_name);
			fav_bundle.putDouble("fav_lat", cur_fav_lat);
			fav_bundle.putDouble("fav_long", cur_fav_long);
			
			Intent i = new Intent(FavoritePage.this, PlaceTodoFromFavorite.class);
			i.putExtras(fav_bundle);
			Log.i("onClick", "After Shift Favorite Bundle");
			FavoritePage.this.startActivity(i);
		}
	}// end onListItemClick
}

