package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class outMessagePage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private ArrayList<Message> out_msg_list = new ArrayList<Message>();
    private MessageAdapter adapter;
	private int numOfoutMsg = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		// Insert code here...
		LocationTaskActivity.myDATABASE.open();
		out_msg_list.add(new Message("",new Location(0,0),false,true));
		
		// Receive and extras bundle -- get list of message
		numOfoutMsg = LocationTaskActivity.myDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
		if (numOfoutMsg > 0){
			//out_msg_list = LocationTaskActivity.myDATABASE.getAllMessagesAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			out_msg_list = LocationTaskActivity.myDATABASE.getAllMessages(LocationTaskActivity.username);
			Log.i("Self Storage", "Number of self storage msg = "+Integer.toString(numOfoutMsg));
		}
       
		// Setup ListView
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new MessageAdapter(this, out_msg_list,false);
        setListAdapter(this.adapter);
		
		LocationTaskActivity.myDATABASE.close();
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(outMessagePage.this,outbox.class);
				outMessagePage.this.startActivity(i);
			}
		});
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Log.i("OUTOFBOUND", "CLICK ON "+Integer.toString(position));
		//Insert code here...
		if(numOfoutMsg>0){
			String cur_outmsg = out_msg_list.get(position).getTextData();
			Double cur_outlatitude   = out_msg_list.get(position).getLocation().getLatitude();
			Double cur_outlongitude  = out_msg_list.get(position).getLocation().getLongitude();
			String toReceiver  = out_msg_list.get(position).getSender();
			Log.w("Self Storage","cur_sel_outmsg = "+cur_outmsg);

			// go to processing message
			Bundle outmsg_bundle = new Bundle();
			outmsg_bundle.putString("outmsg", cur_outmsg);
			outmsg_bundle.putString("toReceiver", toReceiver);
			outmsg_bundle.putDouble("outlatitude", cur_outlatitude);
			outmsg_bundle.putDouble("outlongitude", cur_outlongitude);
			
			Intent i = new Intent(this, ProcessOutMessage.class);
			i.putExtras(outmsg_bundle);
			Log.i("onClick", "After Shift OUTMsg Bundle");
			startActivity(i);
		}
		
	}	
}
