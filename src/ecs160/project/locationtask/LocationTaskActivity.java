package ecs160.project.locationtask;

import com.google.gdata.client.photos.PicasawebService;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LocationTaskActivity extends Activity {
    /** Called when the activity is first created. */
	public final static String AUTH = "authentication";
	public static String username;

    private String pass;
    public static DataSource cmnDATABASE = null;
    public static StorageSource myDATABASE = null;
    public static FavoriteSource favDATABASE = null;
    public static PicasawebService servicePWA = null;

    public static ProfileDataSource profileDATABASE;
	public static AudioManager audioManager;
	public static Profile currentProfile;

    Button SignIn, SignUp;
   
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //  create picasaweb service
        servicePWA = new PicasawebService("ecs160-PicasaApp-1");
        
        // create communication Database
        cmnDATABASE  = new DataSource(this);
        myDATABASE   = new StorageSource(this);
        favDATABASE  = new FavoriteSource(this); 
        
        // create Profile Database
        profileDATABASE = new ProfileDataSource(this);
        
        // instantiate audio manager
		audioManager = (AudioManager)this.getSystemService(this.AUDIO_SERVICE);
        SignIn = (Button)findViewById(R.id.SI_button);
        SignIn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v)  {
				// TODO Auto-generated method stub
		    	Log.w("CREATE","CREATE");
		    	
				// Get username information
				EditText user_input = (EditText)findViewById(R.id.main_username_editText);
				username = user_input.getText().toString().trim();
				Log.d("C2DM","username ="+username);
			    
				// Get password
				EditText pass_input = (EditText)findViewById(R.id.main_pass_editText);
				pass = pass_input.getText().toString().trim();
				
				// Set Profile
				profileDATABASE.open();
				String enabledProfile = profileDATABASE.getEnabledProfile(username); //fetch currently enabled profile, if it doesnt exist return empty string
				if(enabledProfile != "") //if there is a profile in the db that is set to enabled
				{
					currentProfile = profileDATABASE.getProfile(username, enabledProfile);//fetch enabled profile using the username and the name of the profile as parameters
					currentProfile.apply();
				}
				else if(enabledProfile == "") //if no profile exists, create a default profile with user's current settings
				{
					currentProfile = new Profile("Default", username); 
					profileDATABASE.storeProfile(currentProfile);
					currentProfile.apply();
				}
				profileDATABASE.close();
//				
				// Go to next activity
			    Intent i = new Intent(LocationTaskActivity.this,Home.class);
			    LocationTaskActivity.this.startActivity(i);
			    
			    //if returned from activity set allow = true to revert settings changes
			  }//end onClick 
      });
       
       SignUp =  (Button)findViewById(R.id.SU_button);
       SignUp.setEnabled(false);
       SignUp.setOnClickListener(new View.OnClickListener() {
		
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// Insert code here ...
				// to implement sign up page
			}
		});
        
    }// end onCreate
}
