package ecs160.project.locationtask;

public class Time
{
	public Time() {}
	
	public Time(int h, int min, int ap)
	{
		hour = h;
		minute = min;
		ampm = ap;
	}
	
	public Time(int y, int mon, int d, int h, int min, int s)
	{
		year = y;
		month = mon;
		day = d;
		hour = h;
		minute = min;
		second = s;
	}	
	
	public int getYear()
	{
		return year;
	}
	
	public void setYear(int y)
	{
		year = y;
	}
	
	public int getMonth()
	{
		return month;
	}
	
	public void setMonth(int m)
	{
		month = m;
	}
	
	public int getDay()
	{
		return day;
	}
	
	public void setDay(int d)
	{
		day = d;
	}
	
	public int getHour() 
	{
		return hour;
	}
	
	public void setHour(int h) 
	{
		hour = h;
	}
	
	public int getMinute() 
	{
		return minute;
	}
	
	public void setMinute(int m) 
	{
		minute = m;
	}
	
	public int getSecond() 
	{
		return second;
	}
	
	public void setSecond(int s) 
	{
		second = s;
	}	
	
	public int getAMPM()
	{
		return ampm;
	}
	
	public void setAMPM(int i)
	{
		ampm = i;
	}
	
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private int second;
	private int ampm; //0 = am, 1 = pm
}
