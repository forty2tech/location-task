package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.List;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ProcessMessage extends MapActivity {

	Button del_msg,cancel_msg;
	String msg,sender;
	Double msg_lat=0.0;
	Double msg_long=0.0;
	
	 // map view avaliables
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.process_message);
        
		Bundle msg_bundle = getIntent().getExtras();
		msg = msg_bundle.getString("msg");
		sender = msg_bundle.getString("sender");
		msg_lat = msg_bundle.getDouble("latitude");
		msg_long = msg_bundle.getDouble("longitude");
	
		
		// zoom it and creat the pin
		if(msg_lat.intValue() != 0 && msg_long.intValue() != 0) {
		  initMapView();
		  cleanCustomItem();
		  if (sender.length() > 10){
			  String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
					  + sender.substring(4, 7) + "-" + sender.substring(7);
			  createMarker(msg, mod_sender, msg_lat, msg_long, "");
		  }
		  else
			  createMarker(msg, sender, msg_lat, msg_long, "");
		  zoomInToPoint();
		}
		String s = "";
		if (sender.length() > 10){
			String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
        		                + sender.substring(4, 7) + "-" + sender.substring(7);
			s = "   From " + mod_sender + " :\n" +msg;
		}
		else
			s = "   From " + sender + " :\n" +msg;
		TextView msg_view = (TextView)findViewById(R.id.msgView);
		msg_view.setText(s);
		
		del_msg = (Button)findViewById( R.id.del_msg_button);
		del_msg.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				AlertPopUp("Delete Message","Are you sure you want to delete this message ?");
				Log.w("C2DM","del Msg =" + msg);
			}
		});
		
		cancel_msg = (Button)findViewById( R.id.cancel_msg_button);
		cancel_msg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				Intent inbox_msg_page = new Intent(ProcessMessage.this,MessagePage.class);
				ProcessMessage.this.startActivity(inbox_msg_page);
			}
		});
	}

	public void AlertPopUp(String title, String warningMsg){
		new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(warningMsg)
			.setIcon(R.drawable.attentionicon)
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					LocationTaskActivity.cmnDATABASE.open();
					LocationTaskActivity.cmnDATABASE.deleteMessage(LocationTaskActivity.username, new Message(msg,new Location(msg_lat,msg_long),sender));
					LocationTaskActivity.cmnDATABASE.close();
					
					Toast.makeText(getBaseContext(),"Message is deleted", Toast.LENGTH_LONG).show();
					
					Intent inbox_msg_page = new Intent(ProcessMessage.this,MessagePage.class);
					ProcessMessage.this.startActivity(inbox_msg_page);
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(),"Message is not deleted", Toast.LENGTH_LONG).show();
				}
			})
			.show();
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	 private void initMapView() {
	    mapView = (MapView) findViewById(R.id.process_msg_mapView);
	    mapView.setBuiltInZoomControls(true);
	    mapView.displayZoomControls(true);
	    mapView.setSatellite(false);
	    
	    mapController = mapView.getController();
	    mapOverlays = mapView.getOverlays();
	    drawable = getResources().getDrawable(R.drawable.marker2);
	    customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
	  }
	  public void cleanCustomItem() {
	    customItemizedOverlay.getOverLays().clear();
	  }
	  
	  public void zoomInToPoint() {
	    GeoPoint point = new GeoPoint((int)(msg_lat *1e6), (int)(msg_long * 1e6));
	    //mapController.animateTo(point);
	    mapController.setZoom(16);
	    mapController.setCenter(point);
	  }
	  
	  public void createMarker(String text, String sender, double lat, double log, String img_url) {
	      GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
	      CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
	      customItemizedOverlay.addOverlay(overlayItem);
	      mapOverlays.add(customItemizedOverlay);
	  }
}
