package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class GeoMessagePage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private ArrayList<GeoMessage> geomsg_list = new ArrayList<GeoMessage>();
    private GeoMessageAdapter adapter;
	private int numOfGeoMsg = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		geomsg_list.add(new GeoMessage(true));
		
		// Insert code here...
		LocationTaskActivity.cmnDATABASE.open();
		
		// Receive and extras bundle -- get list of message
		numOfGeoMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
		if (numOfGeoMsg > 0){
			//geomsg_list = LocationTaskActivity.cmnDATABASE.getAllGeoMsgAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			geomsg_list = LocationTaskActivity.cmnDATABASE.getAllGeoMsg(LocationTaskActivity.username);
		}
       
		// Setup ListView
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		
		this.adapter = new GeoMessageAdapter(this, geomsg_list,true);
        setListAdapter(this.adapter);
		
		LocationTaskActivity.cmnDATABASE.close();
		
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(GeoMessagePage.this,inbox.class);
				GeoMessagePage.this.startActivity(i);
			}
		});
		
	}// end onCreate
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfGeoMsg > 0){
			
			//Inser code to process data
			String cur_msg = geomsg_list.get(position).getTextdata();
			cur_latitude   = geomsg_list.get(position).getGeoLocation().getLatitude();
			cur_longitude  = geomsg_list.get(position).getGeoLocation().getLongitude();
			String sender  = geomsg_list.get(position).getSender();
			String imgURL  = geomsg_list.get(position).getPathURL();
			
			// go to processing message
			Bundle geomsg_bundle = new Bundle();
			geomsg_bundle.putString("geomsg", cur_msg);
			geomsg_bundle.putString("sender", sender);
			geomsg_bundle.putString("imgURL", imgURL);
			geomsg_bundle.putDouble("geolatitude", cur_latitude);
			geomsg_bundle.putDouble("geolongitude", cur_longitude);
			
			Intent i = new Intent(this, ProcessGeoMessage.class);
			i.putExtras(geomsg_bundle);
			Log.i("onClick", "After Shift GeoMsg Bundle");
			startActivity(i);
		}
	}// end onListItemClick
}
