package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Collections;

import android.media.AudioManager;
import android.util.Log;

public class Profile 
{	
	public Profile()
	{
		expiration = new Time();
		locationExceptions = new ArrayList<Location>();
		days = new ArrayList<Day>();
		contactExceptions = new ArrayList<String>();
//		originalVibrateNotificationSetting = LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION);
//		originalVibrateRingerSetting = LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);
//		originalRingerSetting = LocationTaskActivity.audioManager.getRingerMode();		
	}
	
	public Profile(String n, String u) //used to create a default profile
	{
		name = n;
		user = u;
		profileEnabled = true;
		expiration = new Time();
		locationExceptions = new ArrayList<Location>();
		days = new ArrayList<Day>();
		contactExceptions = new ArrayList<String>();
		
		days.add(new Day("Monday")); 
		days.add(new Day("Tuesday"));
		days.add(new Day("Wednesday"));
		days.add(new Day("Thursday"));
		days.add(new Day("Friday"));
		days.add(new Day("Saturday"));
		days.add(new Day("Sunday"));
		
		
		if(LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER) == AudioManager.VIBRATE_SETTING_ON ||
		   LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER) == AudioManager.VIBRATE_SETTING_ONLY_SILENT)
			vibrateEnabled = true;
		else
			vibrateEnabled = false;
		
		if(LocationTaskActivity.audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL)
			ringerEnabled = true;
		else if(LocationTaskActivity.audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT)
		{
			ringerEnabled = false;
			vibrateEnabled = false;
		}
		else if(LocationTaskActivity.audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
		{
			ringerEnabled = false;
			vibrateEnabled = true;
		}
		
		//store original phone values in the default profile
		setOriginalRinger(LocationTaskActivity.audioManager.getRingerMode());
		setOriginalVibrateRing(LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER));
		setOriginalVibrateNoti(LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION));
		Log.w("ORIGIN","ringer: " + originalRinger + "vibring: " + originalVibrateRinger + "vibnot: " + originalVibrateNotification);
		
//		originalVibrateNotification = LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION);
//		originalVibrateRinger = LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);
//		originalRinger = LocationTaskActivity.audioManager.getRingerMode();		
	}
	
	//accessor and mutator functions
	//profile
	public String getProfileName() { return name; }	
	public void setProfileName(String s) { name = s; }	
	//user
	public String getProfileUser() { return user; }	
	public void setProfileUser(String s) { user = s; }	
	//enable
	public boolean getEnabled() { return profileEnabled; }	
	public void setEnabled(boolean b) { profileEnabled = b; }
	//expiration
	public Time getExpiration() { return expiration; }
	public void setExpiration(Time e) { expiration = e; }
	//ringer
	public boolean getRinger() { return ringerEnabled; }	
	public void setRinger(boolean b) { ringerEnabled = b; }	
	//vibrate
	public boolean getVibrate() { return vibrateEnabled; }	
	public void setVibrate(boolean b) { vibrateEnabled = b; }	
	//locations
	public ArrayList<Location> getLocationExceptions() { return locationExceptions; }	
	public void addLocationException(Location l) { locationExceptions.add(l); }	
	public void deleteLocationException(Location l) { locationExceptions.remove(l); }
	//contacts
	public ArrayList<String> getContactExceptions() { return contactExceptions; }	
	public void addContactException(String s) { contactExceptions.add(s); Collections.sort(contactExceptions, new StringComparator()); }
	public void deleteContactException(String s) { contactExceptions.remove(s); }	
	//days
	public ArrayList<Day> getDaysOfTheWeek() { return days; }
	public void addDayException(Day d) { days.add(d); }
	//original ringer
	public int getOriginalRinger() { return originalRinger; }
	public void setOriginalRinger(int i) { originalRinger = i; }
	//original vibrate(ringer)
	public int getOriginalVibrateRing() { return originalVibrateRinger; }
	public void setOriginalVibrateRing(int i) { originalVibrateRinger = i; }
	//original vibrate(notification)
	public int getOriginalVibrateNoti() { return originalVibrateNotification; }
	public void setOriginalVibrateNoti(int i) { originalVibrateNotification = i; }

	public void apply() 
	{	
			if(ringerEnabled)
				setRingerOn();
			else
				setRingerOff();
			if(vibrateEnabled)
				setVibrateOn();
			else
				setVibrateOff();		
//			if (ringerEnabled && vibrateEnabled) 
//			{
//				setVibrateON();
//				setRingerON();
//			}
//			
//			if (ringerEnabled && !vibrateEnabled) 
//			{
//				setVibrateOff();
//				setRingerON();
//			}		
//			
//			if (!ringerEnabled && vibrateEnabled) 
//			{
//				LocationTaskActivity.audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
//			}
//			
//			if (!ringerEnabled && !vibrateEnabled) 
//			{
//				setVibrateOff();
//				setRingerOff();				
//			}
	}
	
	public void reset() 
	{
		setRingerToDefault();
		setVibrateToDefault();
	}
	
	public void setRingerOn() 
	{
		LocationTaskActivity.audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
	}
	
	public void setVibrateOn() 
	{
		LocationTaskActivity.audioManager.setVibrateSetting(
				AudioManager.VIBRATE_TYPE_NOTIFICATION, AudioManager.VIBRATE_SETTING_ON);
		LocationTaskActivity.audioManager.setVibrateSetting(
				AudioManager.VIBRATE_TYPE_RINGER, AudioManager.VIBRATE_SETTING_ON);
	}
	
	private void setRingerToDefault() //sets the ringer settings to whatever they normally are on the user's phone
	{
		LocationTaskActivity.audioManager.setRingerMode(originalRinger);
	}
	
	public void setRingerOff()
	{
		LocationTaskActivity.audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
	}
	
	public void allON()
	{
		setRingerOn();
		setVibrateOn();
	}
	
	private void setVibrateToDefault() //sets the vibrate settings to whatever they normally are on the user's phone
	{
//		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION, AudioManager.VIBRATE_SETTING_ON);
//		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, AudioManager.VIBRATE_SETTING_ON);
		
		//if vibrate is on, use the phone's original settings instead of assigning new ones
		//only the RINGER settings seem to have an effect in changing things; NOTIFICATION was included just in cases
		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION, originalVibrateNotification);
		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, originalVibrateRinger); 
	}
	
	public void setVibrateOff()
	{
		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION, 
				AudioManager.VIBRATE_SETTING_OFF);
		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, 
				AudioManager.VIBRATE_SETTING_OFF);
	}
	
	private String name; //profile name
	private String user;
	private boolean ringerEnabled;
	private boolean profileEnabled;
	private boolean vibrateEnabled;
	private ArrayList<Location> locationExceptions;
	private ArrayList<Day> days;
	private ArrayList<String> contactExceptions;
	private Time expiration;
	
	private int originalRinger;
	private int originalVibrateRinger;
	private int originalVibrateNotification;
}
