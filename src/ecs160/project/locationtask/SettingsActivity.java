package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/* Links
http://developer.android.com/reference/android/app/AlarmManager.html
http://developer.android.com/reference/android/media/AudioManager.html
http://www.mkyong.com/android/android-spinner-drop-down-list-example/
http://stackoverflow.com/questions/754684/how-to-insert-a-sqlite-record-with-a-datetime-set-to-now-in-android-applicatio
http://developer.android.com/reference/java/util/Calendar.html#getDisplayName%28int,%20int,%20java.util.Locale%29
http://developer.android.com/reference/java/util/Date.html#getDate%28%29
http://www.xyzws.com/javafaq/how-to-use-dateformat-class-in-java/141
http://developer.android.com/resources/tutorials/views/index.html
*/ 

public class SettingsActivity extends Activity 
{
	ImageButton profileAdd, profileDelete, profileEdit;
	TextView ringer, vibrate, expiration, days, contacts, locations, day, dayTime;
	Spinner spinner;
	ArrayList<String> profileNames;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		spinner = (Spinner) findViewById(R.id.settingsSpinner);
		
		//********************* TEMP **********************
		LocationTaskActivity.profileDATABASE.open();
		Profile p1 = new Profile();
		p1.setProfileName("Test1");
		p1.setProfileUser(LocationTaskActivity.username);
		p1.setEnabled(false);
		p1.setRinger(true);
		p1.setVibrate(true);
		p1.addContactException("Prem"); p1.addContactException("Mukherjee"); p1.addContactException("Chen");
		p1.addLocationException(new Location(1.9, 55.6)); p1.addLocationException(new Location(334.4, 555.6)); p1.addLocationException(new Location(17.9, 559.6));
		p1.addDayException(new Day("Monday", new Time(12, 0, 1), new Time(11, 30, 1))); 
		p1.addDayException(new Day("Wednesday", new Time(11, 22, 0), new Time(12, 4, 1))); 
		p1.addDayException(new Day("Friday", new Time(5, 0, 1), new Time(6, 0, 1))); 	
		LocationTaskActivity.profileDATABASE.storeProfile(p1);
		
		Profile p2 = new Profile();
		p2.setProfileName("Test2");
		p2.setProfileUser(LocationTaskActivity.username);
		p2.setEnabled(false);
		p2.setRinger(true);
		p2.setVibrate(false);
		p2.addContactException("Jane"); p2.addContactException("Mary"); p2.addContactException("Isabelle");
		p2.addLocationException(new Location(445, 2)); p2.addLocationException(new Location(78.0, 12.4)); p2.addLocationException(new Location(5.8, 9.6));
		p2.addDayException(new Day("Tuesday", new Time(6, 47, 1), new Time(8, 30, 1))); 
		p2.addDayException(new Day("Thursday", new Time(3, 56, 1), new Time(8, 30, 0)));
		LocationTaskActivity.profileDATABASE.storeProfile(p2);
		 
		Profile p3 = new Profile();
		p3.setProfileName("Test3");
		p3.setProfileUser(LocationTaskActivity.username);
		p3.setEnabled(false);
		p3.setRinger(false);
		p3.setVibrate(false);
		p3.addContactException("John"); p3.addContactException("Zach"); p3.addContactException("Tom");
		p3.addLocationException(new Location(32, 76)); p3.addLocationException(new Location(9.1, 1.8)); p3.addLocationException(new Location(67.8, 4.5));
		p3.addDayException(new Day("Saturday", new Time(10, 15, 0), new Time(12, 0, 1)));
		p3.addDayException(new Day("Sunday", new Time(11, 0, 0), new Time(1, 0, 1)));
		LocationTaskActivity.profileDATABASE.storeProfile(p3);	
		
		Profile p4 = new Profile();
		p4.setProfileName("Test4");
		p4.setProfileUser(LocationTaskActivity.username);
		p4.setEnabled(false);
		p4.setRinger(false);
		p4.setVibrate(false);
		p4.addLocationException(new Location(2147483647, 2147483647));
		p4.addDayException(new Day("Saturday", new Time(10, 15, 0), new Time(12, 0, 1)));
		p4.addDayException(new Day("Sunday", new Time(11, 0, 0), new Time(1, 0, 1)));
		LocationTaskActivity.profileDATABASE.storeProfile(p4);	
		LocationTaskActivity.profileDATABASE.close(); 
		//********************* TEMP **********************
		
		refreshSpinner(spinner);
		refreshProfileView();
//		LocationTaskActivity.audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
////		LocationTaskActivity.audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
////		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION, AudioManager.VIBRATE_SETTING_OFF);
//		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, AudioManager.VIBRATE_SETTING_OFF);
////		LocationTaskActivity.audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, AudioManager.VIBRATE_SETTING_ON);
//		Log.d("SETTINGS","ringermode="+LocationTaskActivity.audioManager.getRingerMode());
//		Log.d("SETTINGS","vibratesettingnotification="+LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION));
//		Log.d("SETTINGS","vibratesettingringer="+LocationTaskActivity.audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER));
		
		//ADD BUTTON 
		profileAdd = (ImageButton) findViewById(R.id.addProfile);
		profileAdd.setOnClickListener(new View.OnClickListener() {		
			public void onClick(View v) 
			{
				Intent addProfile = new Intent(SettingsActivity.this, SettingsAddProfileActivity.class);
				SettingsActivity.this.startActivityForResult(addProfile, 1);
			}
		});
		
		//DELETE BUTTON
		profileDelete = (ImageButton) findViewById(R.id.deleteProfile);
		profileDelete.setOnClickListener(new View.OnClickListener() {		
			public void onClick(View v) 
			{
				if(LocationTaskActivity.currentProfile.getProfileName().compareTo("Default") == 0)
					Toast.makeText(getApplicationContext(), "Cannot delete Default profile", Toast.LENGTH_SHORT).show();
				else //if Default profile isn't being deleted
				{
					Toast.makeText(getApplicationContext(), "Profile \"" + LocationTaskActivity.currentProfile.getProfileName() + "\" has been deleted", Toast.LENGTH_LONG).show();
					LocationTaskActivity.profileDATABASE.open();
					LocationTaskActivity.profileDATABASE.deleteProfile(LocationTaskActivity.username, LocationTaskActivity.currentProfile.getProfileName()); //delete profile from database
					LocationTaskActivity.currentProfile = LocationTaskActivity.profileDATABASE.getProfile(LocationTaskActivity.username, "Default"); //set the current profile to the default profile
					LocationTaskActivity.currentProfile.setEnabled(true); //enable the default profile
					LocationTaskActivity.profileDATABASE.updateProfileEnable(LocationTaskActivity.currentProfile); //update the enabled profile in the database		
					LocationTaskActivity.profileDATABASE.open();
					refreshSpinner(spinner);
					refreshProfileView();
				}
			}
		});
		
		//EDIT BUTTON
		profileEdit = (ImageButton) findViewById(R.id.editProfile);
		profileEdit.setOnClickListener(new View.OnClickListener() {		
			public void onClick(View v) 
			{
				Intent editProfile = new Intent(SettingsActivity.this, SettingsEditProfileActivity.class);
				SettingsActivity.this.startActivityForResult(editProfile, 1);
			}
		});	
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{     
		super.onActivityResult(requestCode, resultCode, data); 
		switch(requestCode) 
		{ 
			case 1: //used by the profileAdd and profileEdit buttons
			{ 
				refreshSpinner(spinner);
				refreshProfileView();
			} 
		} 
	}

	public void refreshSpinner(Spinner s)
	{
		LocationTaskActivity.profileDATABASE.open();
		profileNames = new ArrayList<String>();
		ArrayList<Profile> profiles = LocationTaskActivity.profileDATABASE.getAllProfiles(LocationTaskActivity.username);
		Collections.sort(profiles, new ProfileComparator()); //sort the profiles
		for(int i = 0; i < profiles.size(); i++)
		{
			profileNames.add(profiles.get(i).getProfileName()); //add the names of the profiles from the profiles array to the string array
		}
		LocationTaskActivity.profileDATABASE.close();	
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, profileNames);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		
		spinner.setSelection(profileNames.indexOf(LocationTaskActivity.currentProfile.getProfileName())); //sets the spinner to the current profile
		
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
			{
				String selectedProfile = profileNames.get(position);
				
				LocationTaskActivity.profileDATABASE.open();
				LocationTaskActivity.currentProfile.setEnabled(false); //disable the current profile
				LocationTaskActivity.profileDATABASE.updateProfileEnable(LocationTaskActivity.currentProfile); //store that change in the db
				LocationTaskActivity.currentProfile = LocationTaskActivity.profileDATABASE.getProfile(LocationTaskActivity.username, selectedProfile); //get profile from db that was selected from spinner
				LocationTaskActivity.currentProfile.setEnabled(true); //enable the profile
				LocationTaskActivity.profileDATABASE.updateProfileEnable(LocationTaskActivity.currentProfile); //update the enabled profile
				LocationTaskActivity.profileDATABASE.close();
				LocationTaskActivity.currentProfile.apply(); //apply the profile
				refreshProfileView(); //refresh the screen
			}

			public void onNothingSelected(AdapterView<?> arg0) //do not delete, this is needed even if it's not used
			{
				// TODO Auto-generated method stub
			}
		});
	}
	
	public void refreshProfileView()
	{
		//RINGER
		ringer = (TextView) findViewById(R.id.ringer);
		if(LocationTaskActivity.currentProfile.getRinger())
			ringer.setText("ON");
		else
			ringer.setText("OFF");
		
		//VIBRATE
		vibrate = (TextView) findViewById(R.id.vibrate);
		if(LocationTaskActivity.currentProfile.getVibrate())
			vibrate.setText("ON");
		else
			vibrate.setText("OFF");
		
		//EXPIRATION
		expiration = (TextView) findViewById(R.id.expiration);
		if(LocationTaskActivity.currentProfile.getExpiration().getMonth() == 0 &&
				LocationTaskActivity.currentProfile.getExpiration().getDay() == 0 &&
				LocationTaskActivity.currentProfile.getExpiration().getYear() == 0)
			expiration.setText("Never");
		else
			expiration.setText("" + LocationTaskActivity.currentProfile.getExpiration().getMonth() + "-" + 
									LocationTaskActivity.currentProfile.getExpiration().getDay() + "-" +
									LocationTaskActivity.currentProfile.getExpiration().getYear());
		
		//DAYS
		ArrayList<Day> daysOfWeek = LocationTaskActivity.currentProfile.getDaysOfTheWeek();
		
		//for all the days, reset disable them and set their time to None by default
		//monday
		day = (TextView) findViewById(R.id.mon);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.monTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
			
		//tuesday
		day = (TextView) findViewById(R.id.tue);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.tueTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
		
		//wednesday
		day = (TextView) findViewById(R.id.wed);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.wedTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
		
		//thursday
		day = (TextView) findViewById(R.id.thu);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.thuTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
		
		//friday
		day = (TextView) findViewById(R.id.fri);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.friTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
		
		//saturday
		day = (TextView) findViewById(R.id.sat);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.satTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);
		
		//sunday
		day = (TextView) findViewById(R.id.sun);
		day.setTextSize(17);
		day.setTypeface(Typeface.DEFAULT);
		day.setEnabled(false);
		dayTime = (TextView) findViewById(R.id.sunTime);
		dayTime.setTextSize(17);
		dayTime.setText("None");
		dayTime.setEnabled(false);

		for(int i = 0; i < daysOfWeek.size(); i++) //go through each day in the days arraylist
		{
			if(daysOfWeek.get(i).getDay().compareTo("Monday") == 0)
			{			
				day = (TextView) findViewById(R.id.mon);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);
				dayTime = (TextView) findViewById(R.id.monTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Tuesday") == 0)
			{
				day = (TextView) findViewById(R.id.tue);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);		
				dayTime = (TextView) findViewById(R.id.tueTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Wednesday") == 0)
			{
				day = (TextView) findViewById(R.id.wed);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);	
				dayTime = (TextView) findViewById(R.id.wedTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Thursday") == 0)
			{
				day = (TextView) findViewById(R.id.thu);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);
				dayTime = (TextView) findViewById(R.id.thuTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Friday") == 0)
			{
				day = (TextView) findViewById(R.id.fri);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);
				dayTime = (TextView) findViewById(R.id.friTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Saturday") == 0)
			{
				day = (TextView) findViewById(R.id.sat);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);
				dayTime = (TextView) findViewById(R.id.satTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
			else if(daysOfWeek.get(i).getDay().compareTo("Sunday") == 0)
			{
				day = (TextView) findViewById(R.id.sun);
				day.setEnabled(true);
				day.setTypeface(Typeface.DEFAULT_BOLD);
				day.setTextSize(19);
				dayTime = (TextView) findViewById(R.id.sunTime);
				dayTime.setEnabled(true);
				dayTime.setTextSize(19);
				
				if(daysOfWeek.get(i).getStart().getHour() == 0)
					dayTime.setText("Always");
				else
					dayTime.setText(getTimeString(daysOfWeek.get(i)));
			}
		}
	 
 		//CONTACTS
		String contactsTmp = "";
		ArrayList<String> contactList = LocationTaskActivity.currentProfile.getContactExceptions();
		contacts = (TextView) findViewById(R.id.contactEx);
		if(contactList.size() == 0)
			contactsTmp = "None";
		for (int i = 0; i < contactList.size(); i++) 
		{
			if(i == contactList.size() - 1) //if it's the last one, don't print newline
				contactsTmp += contactList.get(i);
			else
				contactsTmp += contactList.get(i)  + "\n";
		}
		contacts.setText(contactsTmp);
		
		//LOCATIONS
		String locationsTmp = "";
		ArrayList<Location> locationList = LocationTaskActivity.currentProfile.getLocationExceptions();
		locations = (TextView) findViewById(R.id.locationEx);
		if(locationList.size() == 0)
			locationsTmp = "None";
		for (int i = 0; i < locationList.size(); i++) 
		{
			if(i == contactList.size() - 1) //if it's the last one, don't print newline
				locationsTmp += locationList.get(i).getLatitude() + " " + locationList.get(i).getLongitude();
			else
				locationsTmp += locationList.get(i).getLatitude() + " " + locationList.get(i).getLongitude() + "\n";
		}
		locations.setText(locationsTmp);
	}
	
	public String getTimeString(Day d)
	{
		String time;
		
		time = d.getStart().getHour() + ":";
		
		if(d.getStart().getMinute() < 10) time += "0" + d.getStart().getMinute();	else time += d.getStart().getMinute();
		if(d.getStart().getAMPM() == 0) time += "a.m."; //am
		else time += "p.m."; //pm
		
		time += " - ";
		time += d.getFinish().getHour() + ":";
		
		if(d.getFinish().getMinute() < 10) time += "0" + d.getFinish().getMinute();	else time += d.getFinish().getMinute();
		if(d.getFinish().getAMPM() == 0) time += "a.m."; //am
		else time += "p.m."; //pm
		
		return time;
	}
}
