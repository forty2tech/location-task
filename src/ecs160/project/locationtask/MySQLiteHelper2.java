package ecs160.project.locationtask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper2 extends SQLiteOpenHelper 
{	
	public static final String PROFILES_TABLE = "profiles";
	public static final String PROFILE_NAME = "profile"; //1 = message, 2 = task, 3 = query
	public static final String USER_ID = "user";
	public static final String PROFILE_ENABLED = "profile_enabled";	
	public static final String RINGER_ENABLED = "ringer_enabled";
	public static final String VIBRATE_ENABLED = "vibrate_enabled";
	public static final String DAYS = "days";
	public static final String LOCATIONS = "locations";
	public static final String CONTACTS = "contacts";
	public static final String EXPIRATION = "expiration";
	public static final String ORIGINAL_RINGER = "original_ringer";
	public static final String ORIGINAL_VIB_RING = "original_vib_ring";
	public static final String ORIGINAL_VIB_NOTI = "original_vib_noti";
	public static final String COLUMN_ID = "_id";    

	private static final String DATABASE_NAME = "profilesDatabase.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + PROFILES_TABLE + "( " + PROFILE_NAME + " text, " + //0
																							USER_ID + " text, " + //1
																							PROFILE_ENABLED + " integer, " + //2
																							RINGER_ENABLED + " integer, " + //3
																							VIBRATE_ENABLED + " integer, " + //4
																							DAYS + " text, " + //5
																							LOCATIONS + " text, " + //6
																							CONTACTS + " text, " + //7													   	   
																							EXPIRATION + " text, " + //8													   	   
																							ORIGINAL_RINGER + " integer, " + //9
																							ORIGINAL_VIB_RING + " integer, " + //10
																							ORIGINAL_VIB_NOTI + " integer, " + //11
																							COLUMN_ID + " integer primary key autoIncrement); ";
	 
	public MySQLiteHelper2(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) 
	{
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + PROFILES_TABLE);
		onCreate(db);
	}
}
