package ecs160.project.locationtask;

import java.util.List;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PlaceTodoFromFavorite extends MapActivity {
	
	Double fav_lat = 0.0;
	Double fav_long = 0.0;
	String fav_name = "";
	
  // map view avaliables
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.make_from_fav);
		Bundle fav_bundle = getIntent().getExtras();
		fav_name=fav_bundle.getString("fav_name");
		fav_lat = fav_bundle.getDouble("fav_lat");
		fav_long= fav_bundle.getDouble("fav_long");
		
		initMapView();
		cleanCustomItem();
		createMarker(fav_name, fav_name, fav_lat, fav_long, "");
		zoomInToPoint();
		
		Button make_new_fav_cancel_button = (Button)findViewById(R.id.make_new_fav_cancel_button);
		make_new_fav_cancel_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(PlaceTodoFromFavorite.this,FavoritePage.class);
				PlaceTodoFromFavorite.this.startActivity(i);
			}
		});
		
		Button make_new_fav_make_button = (Button)findViewById(R.id.make_new_fav_make_button);
		make_new_fav_make_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle b = new Bundle();
				b.putDouble("fromfav_lat", fav_lat);
				b.putDouble("fromfav_long", fav_long);
				Intent i = new Intent(PlaceTodoFromFavorite.this,Make.class);
				i.putExtra("favorite_bundle", b);
				PlaceTodoFromFavorite.this.startActivity(i);
			}
		});
		
		Button make_new_fav_del_button = (Button)findViewById(R.id.make_new_fav_del_button);
		make_new_fav_del_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertPopUp("Delete Favorite Location","Are you sure you want to delete this location ?");
				Log.i("Favorite Locatioin", "Favorite Location is deleted");
			}
		});
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void AlertPopUp(String title, String warningMsg){
		new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(warningMsg)
			.setIcon(R.drawable.attentionicon)
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					LocationTaskActivity.favDATABASE.open();
					LocationTaskActivity.favDATABASE.deleteFavorite(LocationTaskActivity.username, new Favorite(new Location(fav_lat,fav_long),fav_name));
					LocationTaskActivity.favDATABASE.close();
					
					Toast.makeText(getBaseContext(), "Favorited Location is deleted", Toast.LENGTH_SHORT).show();
					
					Intent i = new Intent(PlaceTodoFromFavorite.this,FavoritePage.class);
					PlaceTodoFromFavorite.this.startActivity(i);}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(),"Favorited Location is not deleted", Toast.LENGTH_LONG).show();
				}
			})
			.show();
	}
	
	 // map for place to do
  private void initMapView() {
    mapView = (MapView) findViewById(R.id.make_from_fav_mapView);
    mapView.setBuiltInZoomControls(true);
    mapView.displayZoomControls(true);
    mapView.setSatellite(false);
    
    mapController = mapView.getController();
    mapOverlays = mapView.getOverlays();
    drawable = getResources().getDrawable(R.drawable.marker2);
    customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
  }
  public void cleanCustomItem() {
    customItemizedOverlay.getOverLays().clear();
  }
  
  public void zoomInToPoint() {
    GeoPoint point = new GeoPoint((int)(fav_lat *1e6), (int)(fav_long * 1e6));
    //mapController.animateTo(point);
    mapController.setZoom(16);
    mapController.setCenter(point);
  }
  
  public void createMarker(String text, String sender, double lat, double log, String img_url) {
      GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
      CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
      customItemizedOverlay.addOverlay(overlayItem);
      mapOverlays.add(customItemizedOverlay);
  }

}
