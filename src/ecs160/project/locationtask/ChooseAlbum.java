package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.Query;
import com.google.gdata.data.media.mediarss.MediaThumbnail;
import com.google.gdata.data.photos.AlbumEntry;
import com.google.gdata.data.photos.AlbumFeed;
import com.google.gdata.data.photos.GphotoEntry;
import com.google.gdata.data.photos.UserFeed;
import com.google.gdata.util.ServiceException;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

public class ChooseAlbum extends ListActivity {

	private int numOfAlbum =0;
	private List<GphotoEntry> album_list = null;
	private List<MyAlbum> list = new ArrayList<MyAlbum>();
	private AlbumAdapter adapter;
	URL feedUrl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		list.add(new MyAlbum(true));
		
		getAlbumFromPicasa();
		
		Log.i("getAlbum","Start setting AlbumAdapter");
		this.adapter = new AlbumAdapter(this, list);
	    setListAdapter(this.adapter);
	    
	}// end onCreate
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfAlbum > 0){
			
			//Inser code to process album => get picture
			MyAlbum cur_album = list.get(position);
			int index = cur_album.getAlbumId().lastIndexOf("/");
			String albumid = cur_album.getAlbumId().substring(index+1);
			
			Bundle album_bundle = new Bundle();
			album_bundle.putString("albumid", albumid);
			
			Intent i = new Intent(ChooseAlbum.this, ChoosePhoto.class);
			i.putExtras(album_bundle);
			ChooseAlbum.this.startActivity(i);
			
		}
	}// end onListItemClick

	private void getAlbumFromPicasa(){
		
		try {
			String albumUrl = "https://picasaweb.google.com/data/feed/api/user/" + Make.pwa_username +"?kind=album";
			Log.i("getAlbum","albumUrl = "+albumUrl);
			feedUrl = new URL(albumUrl);
			Log.i("getAlbum","feedUrl SUCCEEDED");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			Log.i("getAlbum","CANNOT get feedUrl");
			e.printStackTrace();
		}

		UserFeed myUserFeed = null;
		try {
			myUserFeed = LocationTaskActivity.servicePWA.getFeed(feedUrl, UserFeed.class);
			Log.i("getAlbum","SUCCEED get myUserFeed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i("getAlbum","CANNOT get myUserFeed");
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			Log.i("getAlbum","CANNOT get myUserFeed");
			e.printStackTrace();
		}
	
		album_list = myUserFeed.getEntries();
		numOfAlbum = album_list.size();  
		
		if (numOfAlbum > 0){
			list = new ArrayList<MyAlbum>(); 
			
			for (GphotoEntry entry : album_list){
				String albumTitle = entry.getTitle().getPlainText();
		    	String albumId = entry.getId();
		    	list.add(new MyAlbum(albumTitle,albumId));
		        System.out.println("Album Title ="+albumTitle);
		        System.out.println("Album ID = " + albumId);
			}
		}
	} // end getAlbumFromPicasa
	
}
