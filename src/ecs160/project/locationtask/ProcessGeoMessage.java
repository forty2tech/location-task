package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProcessGeoMessage extends Activity {

	Button del_geomsg,cancel_geomsg;
	String geomsg,sender,imgURL;
	Double geomsg_lat=0.0;
	Double geomsg_long=0.0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.process_geomsg);
		
		//insert code here...
		Bundle geomsg_bundle = getIntent().getExtras();
		geomsg = geomsg_bundle.getString("geomsg");
		sender = geomsg_bundle.getString("sender");
		imgURL = geomsg_bundle.getString("imgURL");
		geomsg_lat = geomsg_bundle.getDouble("geolatitude");
		geomsg_long= geomsg_bundle.getDouble("geolongitude");
		String s = "";
		TextView geomsg_frview = (TextView)findViewById(R.id.geomsg_from_textView);
		if (sender.length() > 10){
			String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
                + sender.substring(4, 7) + "-" + sender.substring(7);
			s = "From " + mod_sender;
		}
		else
			s = "From " + sender;
		geomsg_frview.setText(s);
		
		TextView geomsg_dataview = (TextView)findViewById(R.id.geomsg_msg_textView);
		geomsg_dataview.setText(geomsg);
		
		TextView geomsg_locview = (TextView)findViewById(R.id.geomsg_loc_textView);
		s = "Latitude = "+ Double.toString(geomsg_lat) + " , Longitude = "+ Double.toString(geomsg_long);
		geomsg_locview.setText(s);
		
		Drawable image = ImageOperations(getBaseContext(), imgURL,"image.jpg");
		ImageView imgView = (ImageView)findViewById(R.id.geomsg_imageView);
		imgView.setImageDrawable(image);
		
		del_geomsg = (Button)findViewById( R.id.geomsg_del_button);
		del_geomsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				AlertPopUp("Delete GeoMessage","Are you sure you want to delete this geo_message ?");
				Log.w("C2DM","del GeoMsg =" + geomsg);
			}
		});
		
		cancel_geomsg = (Button)findViewById(R.id.geomsg_cancel_button);
		cancel_geomsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ProcessGeoMessage.this,GeoMessagePage.class);
				ProcessGeoMessage.this.startActivity(i);
			}
		});
		
	}
	
	public void AlertPopUp(String title, String warningMsg){
		new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(warningMsg)
			.setIcon(R.drawable.attentionicon)
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					LocationTaskActivity.cmnDATABASE.open();
					LocationTaskActivity.cmnDATABASE.deleteGeoMessage(LocationTaskActivity.username, new GeoMessage(imgURL, sender, geomsg,new Location(geomsg_lat,geomsg_long)));
					LocationTaskActivity.cmnDATABASE.close();
					
					Toast.makeText(getBaseContext(),"GeoMessage is deleted", Toast.LENGTH_LONG).show();
					
					Intent i = new Intent(ProcessGeoMessage.this,GeoMessagePage.class);
					ProcessGeoMessage.this.startActivity(i);
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(),"GeoMessage is not deleted", Toast.LENGTH_LONG).show();
				}
			})
			.show();
	}
	
	  private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
	
		 try {
		   InputStream is = (InputStream) this.fetch(url);
		   Drawable d = Drawable.createFromStream(is, "src");		
		   return d;	
		 } catch (MalformedURLException e) {		
		   e.printStackTrace();		
		   return null;		
		 } catch (IOException e) {		
		   e.printStackTrace();		
		   return null;		
		 }	
	}
	
	public Object fetch(String address) throws MalformedURLException,IOException {
	
		 URL url = new URL(address);		
		 Object content = url.getContent();		
		 return content;		
	}


}
