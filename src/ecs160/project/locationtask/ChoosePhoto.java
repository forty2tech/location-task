package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.Query;
import com.google.gdata.data.geo.Point;
import com.google.gdata.data.media.mediarss.MediaThumbnail;
import com.google.gdata.data.photos.AlbumFeed;
import com.google.gdata.data.photos.GphotoEntry;
import com.google.gdata.util.ServiceException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.Toast;

public class ChoosePhoto extends Activity {

	private int numOfPhoto =0;
	
	URL feedUrl;
	String albumid;
	AlbumFeed feedPhoto =null;
	TableLayout tab_layout = null;
	ArrayList<String> img_url_list = new ArrayList<String>();
	ArrayList<String> img_name_list= new ArrayList<String>();
	ArrayList<Point>  img_loc_list = new ArrayList<Point>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_photo);
		tab_layout = (TableLayout)findViewById(R.id.photoLayout);
		
		Bundle b = getIntent().getExtras();
		albumid = b.getString("albumid");
		
	    try {
			getPhotoFromPicasa(albumid);
			Log.i("getPhoto", "SUCCEEDED get photos");
			numOfPhoto = img_url_list.size();
			// creating the layout for photos
			modifyThisLayout();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("getPhoto", "FAILED get photos");
		}

		
	}// end onCreate

	private void getPhotoFromPicasa(String albumid) throws ServiceException{
		 
		 //Query albumQuery = null;
		 //String queryFields = "entry(title,gphoto:id,gphoto:location)";
		 try {
				String albumIdUrl = "https://picasaweb.google.com/data/feed/api/user/" + Make.pwa_username +"/albumid/" + albumid;
				Log.i("getPhoto","albumIdUrl = "+albumIdUrl);
				feedUrl = new URL(albumIdUrl);
				Log.i("getPhoto", "SUCCEEDED get feedURL");
				//albumQuery = new Query(feedUrl);
				//albumQuery.setFields(queryFields);
				//Log.i("getPhoto", "SUCCEEDED get albumQuery");
		 } catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				Log.i("getPhoto", "FAILED get feedURL");
				e.printStackTrace();
		 }

		try {
			//feedPhoto = LocationTaskActivity.servicePWA.getFeed(albumQuery, AlbumFeed.class);
			feedPhoto = LocationTaskActivity.servicePWA.getFeed(feedUrl, AlbumFeed.class);
			Log.i("getPhoto", "SUCCEED get feedPhoto");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i("getPhoto", "FAIL get feedPhoto");
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			Log.i("getPhoto", "FAIL get feedPhoto");
			e.printStackTrace();
		}
	
	    
	    for(GphotoEntry photo: feedPhoto.getEntries()) {
		      String url = "";
		      String title = "";
		      url = ((MediaThumbnail)photo.getMediaThumbnails().get(2)).getUrl();
		      title = photo.getTitle().getPlainText();
		      img_url_list.add(url);
		      img_name_list.add(title);
		      if(photo.getGeoLocation() !=null)
		    	  img_loc_list.add(photo.getGeoLocation());
		      else
		    	  img_loc_list.add(null);
		}
	}// end getPhotoFromPicasa
	
	private void modifyThisLayout(){
	
		int counter = 1;
		int numOfLine = numOfPhoto / 3;
		int remainder = numOfPhoto % 3;
		LinearLayout linear_layout = null;
		
		// Start getting all photos from album
		for ( int current = 0; current < numOfPhoto; current++){
		
			String imgName = "image"+ Integer.toString(current) + ".jpg";
			Log.i("getPhoto", "imageName = "+imgName);
			Drawable image = ImageOperations(getBaseContext(), img_url_list.get(current),imgName);
			
			// Set up image view
			ImageView iv = new ImageView(this);
    		iv.setLayoutParams(new LayoutParams(150,150));
    		iv.setPadding(20, 20, 0, 0);
    		iv.setImageDrawable(image);
    		
    		String imgInfo = Integer.toString(current) +";"+ img_name_list.get(current) + ";" + img_url_list.get(current);
    		iv.setTag(imgInfo);
    		
    		iv.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String rawImgData = (String) v.getTag();
					int delim = rawImgData.indexOf(";");
					int index = Integer.parseInt(rawImgData.substring(0,delim));
					String imgData = rawImgData.substring(delim+1); // contain imgUrl and imgNam
					Point p = img_loc_list.get(index);
					
					Bundle photoBundle = new Bundle();
					photoBundle.putString("imgData", imgData);
					if (p!=null){
						photoBundle.putDouble("imgLat", p.getLatitude());
						photoBundle.putDouble("imgLong", p.getLongitude());
					}
					else{
						photoBundle.putDouble("imgLat", 0.0);
						photoBundle.putDouble("imgLong", 0.0);
					}
					Intent i = new Intent(ChoosePhoto.this,Make.class);
					i.putExtra("gotPhoto", photoBundle);
					ChoosePhoto.this.startActivity(i);
				}
			});
    		
    		// Set up a LinearLayout contains 3 images
            if (counter == 1){	 		
            	linear_layout = new LinearLayout(this);
        		linear_layout.setOrientation(0);
        		linear_layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
            }	
        		
            linear_layout.addView(iv);
        	counter++;
            
            // If the current LinearLayout has enough 3 images, then push it into the View 
            if (counter > 3 && numOfLine != 0){
            	Log.i("getPhoto","Print line "+Integer.toString(numOfLine));
            	TableRow tabRow = new TableRow(this);
            	tabRow.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));   
            	tabRow.addView(linear_layout);
            	tab_layout.addView(tabRow,new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
                counter = 1;
                numOfLine -= 1;
            }
            // If the numOfPhoto is not divisible by 3, then the LinearLayout for those remainding images
            else if ( numOfLine == 0){
            	// If the current remainder images is not the last images
            	// then dont push the last Linearlayout into the View
            	if (remainder == 1){
	            	Log.i("getPhoto","Print last line ");
	                TableRow lastRow = new TableRow(this);
	            	lastRow.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));   
	            	lastRow.addView(linear_layout);
	            	tab_layout.addView(lastRow,new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
            	}
            	else
            		remainder -=1;
            }
            else ;
        } // end for loop
		
	}// end modifyThisLayout
	
	private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
	
		 try {
		   InputStream is = (InputStream) this.fetch(url);
		   Drawable d = Drawable.createFromStream(is, "src");		
		   return d;	
		 } catch (MalformedURLException e) {		
		   e.printStackTrace();		
		   return null;		
		 } catch (IOException e) {		
		   e.printStackTrace();		
		   return null;		
		 }	
	} // end ImageOperations
	
	public Object fetch(String address) throws MalformedURLException,IOException {
	
		 URL url = new URL(address);		
		 Object content = url.getContent();		
		 return content;		
	}// end fetching Image from URL

	
	
		
}
