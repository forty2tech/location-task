package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.ListView;

public class QueryPage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private int numOfQuery = 0;
	private ArrayList<Query> query_list = new ArrayList<Query>();
	private QueryAdapter adapter;
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		LocationTaskActivity.cmnDATABASE.open();
		query_list.add(new Query("",new Location(0,0),new Time(0,0,0,0,0,0),new Time(0,0,0,0,0,0)));
		
		// Receive and extras bundle -- get list of message
		numOfQuery = LocationTaskActivity.cmnDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
		if (numOfQuery > 0){
			//query_list = LocationTaskActivity.cmnDATABASE.getAllQueriesAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			query_list = LocationTaskActivity.cmnDATABASE.getAllQueries(LocationTaskActivity.username);
			
		}
        
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new QueryAdapter(this, query_list,true);
		setListAdapter(adapter);
		LocationTaskActivity.cmnDATABASE.close();
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(QueryPage.this,inbox.class);
				QueryPage.this.startActivity(i);
			}
		});
	}// end onCreate
	
	
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfQuery>0){
			String cur_query = query_list.get(position).getTextData();
			Boolean cur_active = true;
			cur_latitude     = query_list.get(position).getLocation().getLatitude();
			cur_longitude    = query_list.get(position).getLocation().getLongitude();
			String sender    = query_list.get(position).getSender();
			Time endTime     = query_list.get(position).getQueryEnd();
			Time startTime   = query_list.get(position).getQueryBegin();
			
			Log.w("C2DM","cur_sel = "+cur_query);
			
			final Calendar c = Calendar.getInstance();
		    int cur_Year = c.get(Calendar.YEAR);
		    int cur_Month = c.get(Calendar.MONTH) + 1;
		    int cur_Day = c.get(Calendar.DAY_OF_MONTH);
		    int cur_Hr = c.get(Calendar.HOUR_OF_DAY);
		    int cur_Min = c.get(Calendar.MINUTE);
			
		    // Check active status of Task
		    if (cur_Year > endTime.getYear())
				cur_active = false;
			else if (cur_Year < endTime.getYear())
				cur_active = true;
			else{			// same year
				if (cur_Month > endTime.getMonth())
					cur_active = false;
				else if (cur_Month < endTime.getMonth())
					cur_active = true;
				else{		// same month
					if (cur_Day > endTime.getDay())
						cur_active = false;
					else if (cur_Day < endTime.getDay())
						cur_active = true;
					else{	// same day
						if (cur_Hr > endTime.getHour())
							cur_active = false;
						else if (cur_Hr < endTime.getHour())
							cur_active = true;
						else{//same hr
							if (cur_Min > endTime.getMinute())
								cur_active = false;
							else 
								cur_active = true;
						}
					}
				}
			}
			
		    
		    if (!cur_active){
		 		LocationTaskActivity.cmnDATABASE.open();
				LocationTaskActivity.cmnDATABASE.updateQuery(LocationTaskActivity.username, new Query(cur_query,new Location(cur_latitude,cur_longitude)
															,new Time(startTime.getYear(),startTime.getMonth(),startTime.getDay(),startTime.getHour(),startTime.getMinute(),0)
															,new Time(endTime.getYear(),endTime.getMonth(),endTime.getDay(),endTime.getHour(),endTime.getMinute(),0),sender,false));
				LocationTaskActivity.cmnDATABASE.close();
		 	}
		    
			
			// go to processing task
			Bundle query_bundle = new Bundle();
			query_bundle.putString("query", cur_query);
			query_bundle.putString("sender", sender);
			query_bundle.putBoolean("active", cur_active);
			query_bundle.putDouble("latitude", cur_latitude);
			query_bundle.putDouble("longitude", cur_longitude);
			query_bundle.putInt("endYear", endTime.getYear());
			query_bundle.putInt("endMonth", endTime.getMonth());
			query_bundle.putInt("endDate", endTime.getDay());
			query_bundle.putInt("endHr", endTime.getHour());
			query_bundle.putInt("endMin", endTime.getMinute());
			query_bundle.putInt("startYear", startTime.getYear());
			query_bundle.putInt("startMonth", startTime.getMonth());
			query_bundle.putInt("startDate", startTime.getDay());
			query_bundle.putInt("startHr", startTime.getHour());
			query_bundle.putInt("startMin", startTime.getMinute());
			
			Intent i = new Intent(this, ProcessQuery.class);
			i.putExtras(query_bundle);
			Log.i("onClick", "After Shift query Bundle");
			startActivity(i);
		}
	}// end onListItemClick
}
