package ecs160.project.locationtask;

import java.util.Comparator;

public class ProfileComparator implements Comparator<Profile> 
{
	public int compare(Profile p1, Profile p2)
	{
		if(p1.getProfileName().toLowerCase().compareTo(p2.getProfileName().toLowerCase()) == 0)
		{
			if(p1.getProfileName().compareTo(p2.getProfileName()) == 0)
				return 0;
			else if(p1.getProfileName().compareTo(p2.getProfileName()) < 0) 
				return 1;
			else
				return -1;
		}
		else if(p1.getProfileName().toLowerCase().compareTo(p2.getProfileName().toLowerCase()) < 0)
		{
				return -1;
		}
		else //if(p1.getProfileName().toLowerCase().compareTo(p2.getProfileName().toLowerCase()) > 0)
		{
				return 1;
		}
	} 
}
//Returns a negative integer, zero, or a positive integer
//as this object is less than, equal to, or greater than the specified object.