package ecs160.project.locationtask;

public class Location
{
	public Location() {
		this.latitude = 0.0;
		this.longitude = 0.0;		
	}

	public Location(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double parmLat)
	{
		this.latitude = parmLat;
	}
	
	public void setLongitude(double parmLong)
	{
		this.longitude = parmLong;
	}
	
	private double latitude;
	private double longitude;
	
}
