package ecs160.project.locationtask;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FavoriteSource {
	// Database fields
			private SQLiteDatabase database;
			private FavoritePlaceSQLiteHelper dbHelper;

			public FavoriteSource(Context context) 
			{
				dbHelper = new FavoritePlaceSQLiteHelper(context);
			}

			public void open() throws SQLException 
			{
				database = dbHelper.getWritableDatabase();
			}

			public void close() 
			{
				dbHelper.close();
			}
			
			public int getTotalNumberFavoritePlace(String user)
			{
				String args[] = {user};
				Cursor cursor = database.query(FavoritePlaceSQLiteHelper.FAVORITE_TABLE, null, FavoritePlaceSQLiteHelper.USER_ID + " = ?", 
																				   args, null, null, null);
				return cursor.getCount();
			}
			
			

			public void storeFavoritePlace(String user, Favorite fav_place)//(String user, String msg, double lat, double lon, boolean read) 
			{ 
				ContentValues values = new ContentValues();
				values.put(FavoritePlaceSQLiteHelper.USER_ID, user);
				values.put(FavoritePlaceSQLiteHelper.LOCATION_NAME,fav_place.getName());
				values.put(FavoritePlaceSQLiteHelper.LATITUDE, fav_place.getLocation().getLatitude());
				values.put(FavoritePlaceSQLiteHelper.LONGITUDE, fav_place.getLocation().getLongitude());
				database.insert(FavoritePlaceSQLiteHelper.FAVORITE_TABLE, null, values);
			}
			
			
			
			
			
			public ArrayList<Favorite> getAllFavoriteLocation(String user)
			{
				ArrayList<Favorite> favorites = new ArrayList<Favorite>();
			
				String args[] = { user};
				Cursor cursor = database.query(FavoritePlaceSQLiteHelper.FAVORITE_TABLE, null, FavoritePlaceSQLiteHelper.USER_ID + " = ?", 
																				   args, null, null, null);
				cursor.moveToFirst(); //moves cursor to the first row
				while(!cursor.isAfterLast())
				{
					favorites.add(cursorToFavorite(cursor)); //convert each query row to a message and store it in the list
					cursor.moveToNext(); //advance the cursor to the next row
				}
				cursor.close();
				return favorites;
			}
			
			
			
			public void deleteFavorite(String user, Favorite fav) 
			{
				String args[] = { user, fav.getName()};
				
				int count = database.delete(FavoritePlaceSQLiteHelper.FAVORITE_TABLE, FavoritePlaceSQLiteHelper.USER_ID + " = ? and " +
															 FavoritePlaceSQLiteHelper.LOCATION_NAME + " = ?"
															  , args);
				
				Log.w("C2DM","num of row deleted "+ count);
			}
			
			

			private Favorite cursorToFavorite(Cursor cursor) 
			{
				Favorite fav = new Favorite();
				fav.setName(cursor.getString(1));
				fav.setLocation(new Location(cursor.getDouble(2),cursor.getDouble(3)));
				return fav;
			}
			
}
