package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class outGeoMsgPage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private ArrayList<GeoMessage> out_geomsg_list = new ArrayList<GeoMessage>();
    private GeoMessageAdapter adapter;
	private int numOfoutGeoMsg = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		out_geomsg_list.add(new GeoMessage(true));
		
		// Insert code here...
		LocationTaskActivity.myDATABASE.open();
		
		// Receive and extras bundle -- get list of message
		numOfoutGeoMsg = LocationTaskActivity.myDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
		if (numOfoutGeoMsg > 0){
			//out_geomsg_list = LocationTaskActivity.myDATABASE.getAllGeoMsgAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			out_geomsg_list = LocationTaskActivity.myDATABASE.getAllGeoMsg(LocationTaskActivity.username);
		}
       
		// Setup ListView
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new GeoMessageAdapter(this, out_geomsg_list,false);
        setListAdapter(this.adapter);
		LocationTaskActivity.myDATABASE.close();
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(outGeoMsgPage.this,outbox.class);
				outGeoMsgPage.this.startActivity(i);
			}
		});
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfoutGeoMsg >0){
			//Inser code to process data
			String cur_msg = out_geomsg_list.get(position).getTextdata();
			cur_latitude   = out_geomsg_list.get(position).getGeoLocation().getLatitude();
			cur_longitude  = out_geomsg_list.get(position).getGeoLocation().getLongitude();
			String sender  = out_geomsg_list.get(position).getSender();
			String imgURL  = out_geomsg_list.get(position).getPathURL();
			Log.w("C2DM","cur_sel = "+cur_msg);
			
			// go to processing message
			Bundle out_geomsg_bundle = new Bundle();
			out_geomsg_bundle.putString("out_geomsg", cur_msg);
			out_geomsg_bundle.putString("sender", sender);
			out_geomsg_bundle.putString("out_imgURL", imgURL);
			out_geomsg_bundle.putDouble("out_geolatitude", cur_latitude);
			out_geomsg_bundle.putDouble("out_geolongitude", cur_longitude);
			
			Intent i = new Intent(this, ProcessOutGeoMessage.class);
			i.putExtras(out_geomsg_bundle);
			Log.i("onClick", "After Shift GeoMsg Bundle");
			startActivity(i);
		}
	}
}
