package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.List;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;


public class inbox extends MapActivity {

	Button imsg_but,itask_but,iquery_but,igeomsg_but;
	ImageButton irefresh_but,home_but;
	int numOfinMsg,numOfinTask,numOfinQuery,numOfinGeoMsg;

	// map view avaliables
	private LocationManager locationManager = null;
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  private double avg_latitude;
  private double avg_longitude;
  private int total_points;
  
  // all messages, tasks, queue, geomessage
  private ArrayList<Message> msg_list = new ArrayList<Message>();
  private int numOfMsg = 0;
  private ArrayList<Task> task_list = new ArrayList<Task>();
  private int numOfTask = 0;
  private ArrayList<Query> query_list = new ArrayList<Query>();
  private int numOfQuery = 0;
  private ArrayList<GeoMessage> geomsg_list = new ArrayList<GeoMessage>();
  private int numOfGeoMsg = 0;
	
	int msgNotRead,taskActive,queryActive;

	@Override
	protected void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);
		setContentView(R.layout.inbox);
		
		// init the map
		initMapView();
		
		LocationTaskActivity.cmnDATABASE.open();
		
		// set the pin
		cleanCustomItem();
		showAllReceivedMessageTaskQueryAndGeoMsg();
		//zoomInToPoint();
		
		home_but = (ImageButton)findViewById(R.id.inbox_home_button);
		home_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent home_page = new Intent(inbox.this,Home.class);
				inbox.this.startActivity(home_page);
			}
		});
		
		imsg_but = (Button) findViewById(R.id.inbox_msg_button);
		msgNotRead = LocationTaskActivity.cmnDATABASE.getTotalNumberMessagesNotRead(LocationTaskActivity.username);
		numOfinMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
		imsg_but.setText("Message \n"+ Integer.toString(msgNotRead) + "/"+Integer.toString(numOfinMsg));
		imsg_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inbox_msg_page = new Intent(inbox.this,MessagePage.class);
				inbox.this.startActivity(inbox_msg_page);
			}
		});
		
		itask_but = (Button) findViewById(R.id.inbox_task_button);
		taskActive = LocationTaskActivity.cmnDATABASE.getTotalNumberTasksActive(LocationTaskActivity.username);
		numOfinTask = LocationTaskActivity.cmnDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
		itask_but.setText("Task \n"+Integer.toString(taskActive)+ "/"+Integer.toString(numOfinTask));
		itask_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inbox_task_page = new Intent(inbox.this,TaskPage.class);
				inbox.this.startActivity(inbox_task_page);
			}
		});
		
		
		iquery_but = (Button) findViewById(R.id.inbox_query_button);
		queryActive = LocationTaskActivity.cmnDATABASE.getTotalNumberQueriesActive(LocationTaskActivity.username);
		numOfinQuery = LocationTaskActivity.cmnDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
		iquery_but.setText("Query \n"+Integer.toString(queryActive)+ "/"+ Integer.toString(numOfinQuery));
		iquery_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inbox_query_page = new Intent(inbox.this,QueryPage.class);
				inbox.this.startActivity(inbox_query_page);
			}
		});
		
		igeomsg_but = (Button) findViewById(R.id.inbox_geomsg_button);
		numOfinGeoMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
		igeomsg_but.setText("Geo-msg\n"+Integer.toString(numOfinGeoMsg));
		igeomsg_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inbox_geomsg_page = new Intent(inbox.this,GeoMessagePage.class);
				inbox.this.startActivity(inbox_geomsg_page);
			}
		});
		
		irefresh_but = (ImageButton) findViewById(R.id.inbox_refresh_button);
		irefresh_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocationTaskActivity.cmnDATABASE.open();
				msgNotRead = LocationTaskActivity.cmnDATABASE.getTotalNumberMessagesNotRead(LocationTaskActivity.username);
				numOfinMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
				imsg_but.setText("Message \n"+ Integer.toString(msgNotRead) + "/"+Integer.toString(numOfinMsg));
				
				taskActive = LocationTaskActivity.cmnDATABASE.getTotalNumberTasksActive(LocationTaskActivity.username);
				numOfinTask = LocationTaskActivity.cmnDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
				itask_but.setText("Task \n"+Integer.toString(taskActive)+ "/"+Integer.toString(numOfinTask));
				
				queryActive = LocationTaskActivity.cmnDATABASE.getTotalNumberQueriesActive(LocationTaskActivity.username);
				numOfinQuery = LocationTaskActivity.cmnDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
				iquery_but.setText("Query \n"+Integer.toString(queryActive)+ "/"+ Integer.toString(numOfinQuery));
				
				numOfinGeoMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
				igeomsg_but.setText("Geo-msg\n"+Integer.toString(numOfinGeoMsg));
			}
		});
		
		LocationTaskActivity.cmnDATABASE.close();	
	} //onCreate

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void initMapView() {
    mapView = (MapView) findViewById(R.id.inbox_mapView);
    mapView.setBuiltInZoomControls(true);
    mapView.displayZoomControls(true);
    mapView.setSatellite(false);
    
    mapController = mapView.getController();
    mapOverlays = mapView.getOverlays();
    drawable = getResources().getDrawable(R.drawable.marker2);
    customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
    
    locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
    
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
        0, new UpdateCurrentLoaction());
    
    avg_latitude = 0;
    avg_longitude = 0;
    total_points = 0;
	}

  private void showAllReceivedMessageTaskQueryAndGeoMsg() {
//    LocationTaskActivity.cmnDATABASE.open();
    
    // message
    numOfMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberMessagesNotRead(LocationTaskActivity.username);
    
    if (numOfMsg > 0){
      msg_list = LocationTaskActivity.cmnDATABASE.getAllMessagesNotRead(LocationTaskActivity.username);
      for(Message m : msg_list) {
        if(m.getLocation().getLatitude() != 0 && m.getLocation().getLongitude() != 0) {
          String text = m.getTextData();
          String sender = m.getSender();
          double lat = m.getLocation().getLatitude();
          double log = m.getLocation().getLongitude();
          avg_latitude = lat + avg_latitude;
          avg_longitude = log + avg_longitude;
          total_points++;
          text = "Message: " + text;
          if (sender.length()>10){
        	  String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
        			  + sender.substring(4, 7) + "-" + sender.substring(7);
        	  createMarker(text, mod_sender, lat, log, "");
          }
          else
        	  createMarker(text, sender, lat, log, "");
        }
      }
    }
    
    // task
    numOfTask = LocationTaskActivity.cmnDATABASE.getTotalNumberTasksActive(LocationTaskActivity.username);
    if (numOfTask > 0){
      task_list = LocationTaskActivity.cmnDATABASE.getAllTasksActive(LocationTaskActivity.username);
      for(Task t : task_list) {
        if(t.getLocation().getLatitude() != 0 && t.getLocation().getLongitude() != 0) {
          String text = t.getTextData();
          String sender = t.getSender();
          double lat = t.getLocation().getLatitude();
          double log = t.getLocation().getLongitude();
          avg_latitude = lat + avg_latitude;
          avg_longitude = log + avg_longitude;
          total_points++;
          text = "Task: " + text;
          if (sender.length() > 10){
        	  String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
        			  + sender.substring(4, 7) + "-" + sender.substring(7);
        	  createMarker(text, mod_sender, lat, log, "");
          }
          else
        	  createMarker(text, sender, lat, log, "");
        }
      }
    }
    
    // query
    numOfQuery = LocationTaskActivity.cmnDATABASE.getTotalNumberQueriesActive(LocationTaskActivity.username);
    if (numOfQuery > 0){
      query_list = LocationTaskActivity.cmnDATABASE.getAllQueriesActive(LocationTaskActivity.username);
      for(Query q : query_list) {
        if(q.getLocation().getLatitude() != 0 && q.getLocation().getLongitude() != 0) {
          String text = q.getTextData();
          String sender = q.getSender();
          double lat = q.getLocation().getLatitude();
          double log = q.getLocation().getLongitude();
          avg_latitude = lat + avg_latitude;
          avg_longitude = log + avg_longitude;
          total_points++;
          text = "Query: " + text;
          if (sender.length() > 10){
        	  String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
        			  + sender.substring(4, 7) + "-" + sender.substring(7);
        	  createMarker(text, mod_sender, lat, log, "");
          }
          else
        	  createMarker(text, sender, lat, log, "");
        }
      }
    }
    
    // geomsg
    numOfGeoMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
    if (numOfGeoMsg > 0){
      geomsg_list = LocationTaskActivity.cmnDATABASE.getAllGeoMsg(LocationTaskActivity.username);
      for(GeoMessage gs : geomsg_list) {
        if(gs.getGeoLocation().getLatitude() != 0 && gs.getGeoLocation().getLongitude() != 0) {
          String text = gs.getTextdata();
          String sender = gs.getSender();
          double lat = gs.getGeoLocation().getLatitude();
          double log = gs.getGeoLocation().getLongitude();
          String img_url = gs.getPathURL();
          avg_latitude = lat + avg_latitude;
          avg_longitude = log + avg_longitude;
          total_points++;
          text = "GeoMessage: " + text;
          if (sender.length() > 10){
        	  String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
        			  + sender.substring(4, 7) + "-" + sender.substring(7);
        	  createMarker(text, mod_sender, lat, log, img_url);
          }
          else
        	  createMarker(text, sender, lat, log, "");
        }
      }
    }
//    LocationTaskActivity.cmnDATABASE.close();
  }
 
	
  public void cleanCustomItem() {
    customItemizedOverlay.getOverLays().clear();
  }
  
  public void zoomInToPoint() {
    GeoPoint point = new GeoPoint((int)(avg_latitude / total_points *1e6), (int)(avg_longitude / total_points * 1e6));
    //mapController.animateTo(point);
    mapController.setZoom(10);
    mapController.setCenter(point);
  }
  
  public void zoomInPointWithLoaction(Location location) {
	  GeoPoint point = new GeoPoint((int)(location.getLatitude() * 1e6), (int)(location.getLongitude() * 1e6));
	  mapController.setZoom(10);
	  mapController.animateTo(point);
  }
  
  public void createMarker(String text, String sender, double lat, double log, String img_url) {
      GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
      
      CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
      customItemizedOverlay.addOverlay(overlayItem);
      mapOverlays.add(customItemizedOverlay);
  }

  public class UpdateCurrentLoaction  implements LocationListener {

	    @Override
	    public void onLocationChanged(Location location) {
	    	zoomInPointWithLoaction(location);
	    }

	    @Override
	    public void onProviderDisabled(String provider) {
	    }

	    @Override
	    public void onProviderEnabled(String provider) {
	    }

	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {
	    }
	}

}
