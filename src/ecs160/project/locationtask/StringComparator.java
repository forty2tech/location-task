package ecs160.project.locationtask;

import java.util.Comparator;

public class StringComparator implements Comparator<String> 
{
	public int compare(String s1, String s2)
	{
		if(s1.toLowerCase().compareTo(s2.toLowerCase()) == 0)
		{
			if(s1.compareTo(s2) == 0)
				return 0;
			else if(s1.compareTo(s2) < 0)
				return 1;
			else
				return -1;
		}
		else if(s1.toLowerCase().compareTo(s2.toLowerCase()) > 0)
			return 1;
		else
			return -1;
	}
}
//Returns a negative integer, zero, or a positive integer
//as this object is less than, equal to, or greater than the specified object.