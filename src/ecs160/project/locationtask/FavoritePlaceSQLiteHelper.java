package ecs160.project.locationtask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class FavoritePlaceSQLiteHelper extends SQLiteOpenHelper {

	public static final String FAVORITE_TABLE = "favoriteplace";
	public static final String COLUMN_ID = "_id";
	public static final String USER_ID = "user";
	public static final String LOCATION_NAME = "locationname";	
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	
	private static final String DATABASE_NAME = "favoriteLocation.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + FAVORITE_TABLE + "( " + USER_ID + " text, " + //0
																						   LOCATION_NAME + " text, " + //1
																						   LATITUDE + " real, " + //2
																						   LONGITUDE + " real, " + //3
																						   
																						   COLUMN_ID + " integer primary key autoIncrement); ";																				
	
	public FavoritePlaceSQLiteHelper(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) 
	{
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + FAVORITE_TABLE);
		onCreate(db);
	}

	
	
}
