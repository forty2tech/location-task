package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageAdapter extends ArrayAdapter<Message> {

	private ArrayList<Message> items;
    private Activity context;
    private boolean isFrom;
	
    static class ViewHolder {
		public TextView toptext;
		public TextView bottomtext;
		public ImageView icon;
	}
    
    public MessageAdapter(Activity context, ArrayList<Message> items, boolean isfrom) {
            super(context, R.layout.message, items);
            this.context = context;
            this.items = items;
            this.isFrom = isfrom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
            	LayoutInflater inflater = context.getLayoutInflater();
    			rowView = inflater.inflate(R.layout.message, null);
    			
    			ViewHolder viewHolder = new ViewHolder();
    			viewHolder.toptext = (TextView) rowView.findViewById(R.id.toptext);
    			viewHolder.bottomtext = (TextView) rowView.findViewById(R.id.bottomtext);
    			viewHolder.icon = (ImageView) rowView.findViewById(R.id.msg_icon);
    			rowView.setTag(viewHolder);
            }
            
            ViewHolder holder = (ViewHolder) rowView.getTag();
            Message m = items.get(position);
            String from = "From ";
            String to = "To ";
            boolean flag = m.getNoMsgFlag();
            
            if (!flag){
	            
	            String raw_sender = m.getSender();
	            Log.i("MSG ADAPTER","raw_sender = "+raw_sender);
	            if (isFrom){
	            	if (raw_sender.length()>10){
	            		String mod_sender = raw_sender.substring(0, 1) + "-(" + raw_sender.substring(1, 4) + ")-"
		            		                + raw_sender.substring(4, 7) + "-" + raw_sender.substring(7);
		            	from += mod_sender + " :";
	            	}
	            	else
	            		from += raw_sender + ":";
		           holder.toptext.setText(from);
	            }
	            else{
	            	to += raw_sender + ":";
	            	holder.toptext.setText(to);
	            }
	            holder.bottomtext.setText(m.getTextData());
	            
	            if (m.getMessageRead())
	            	holder.icon.setImageResource(R.drawable.read);
	            else
	            	holder.icon.setImageResource(R.drawable.unread);
            }
            else{
            	holder.toptext.setText("");
	            holder.bottomtext.setText("No Message");
	            holder.icon.setImageResource(R.drawable.nonreply);

            }
            return rowView;
    }
}
