package ecs160.project.locationtask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ProcessOutMessage extends Activity {
	Button del_outmsg,cancel_outmsg;
	String outmsg,toReceiver;
	Double outmsg_lat=0.0;
	Double outmsg_long=0.0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.process_outmessage);
        
		Bundle msg_bundle = getIntent().getExtras();
		outmsg = msg_bundle.getString("outmsg");
		toReceiver = msg_bundle.getString("toReceiver");
		outmsg_lat = msg_bundle.getDouble("outlatitude");
		outmsg_long = msg_bundle.getDouble("outlongitude");
		
		String to = "To "+toReceiver+" :";
		
		if(toReceiver.length()>10){
	        String mod_recevier = toReceiver.substring(0, 1) + "-(" + toReceiver.substring(1, 4) + ")-"
	        		                + toReceiver.substring(4, 7) + "-" + toReceiver.substring(7);
	        to = "To " + mod_recevier + " :";
		}
		
		TextView outmsg_view = (TextView)findViewById(R.id.outmsg_msgView);
		outmsg_view.setText(outmsg);
		TextView outsender_view = (TextView)findViewById(R.id.outmsg_senderView);
		outsender_view.setText(to);
		
		del_outmsg = (Button)findViewById( R.id.outmsg_delButton);
		del_outmsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				AlertPopUp("Delete Message","Are you sure you want to delete this message ?");
				Log.w("C2DM","del Out Msg =" + outmsg);
			}
		});
		
		cancel_outmsg = (Button)findViewById( R.id.outmsg_cancel_button);
		cancel_outmsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// delete message from database
				
				Intent i = new Intent(ProcessOutMessage.this,outMessagePage.class);
				ProcessOutMessage.this.startActivity(i);
			}
		});
	} // end onCreate
	
	public void AlertPopUp(String title, String warningMsg){
		new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(warningMsg)
			.setIcon(R.drawable.attentionicon)
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					LocationTaskActivity.myDATABASE.open();
					LocationTaskActivity.myDATABASE.deleteMessage(LocationTaskActivity.username, new Message(outmsg,new Location(outmsg_lat,outmsg_long),toReceiver));
					LocationTaskActivity.myDATABASE.close();
					
					Toast.makeText(getBaseContext(),"Message is deleted", Toast.LENGTH_LONG).show();
					
					Intent i = new Intent(ProcessOutMessage.this,outMessagePage.class);
					ProcessOutMessage.this.startActivity(i);
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getBaseContext(),"Message is not deleted", Toast.LENGTH_LONG).show();
				}
			})
			.show();
	}
}
