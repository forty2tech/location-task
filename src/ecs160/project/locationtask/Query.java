package ecs160.project.locationtask;

public class Query extends Communication 
{
	public Query() {}
	
	public Query(String text, Location loc) 
	{
		super.setTextData(text);
		super.setLocation(loc);
		queryReplied = false;
		queryActive = false;
	}

	public Query(String text, Location loc, Time qb, Time qe) 
	{
		super.setTextData(text);
		super.setLocation(loc);
		queryBegin = qb;
		queryEnd = qe;
		queryReplied = false;
		queryActive = false;
	}
	
	public Query(String text, Location loc, Time qb, Time qe,String pS) 
	{
		super.setTextData(text);
		super.setLocation(loc);
		queryBegin = qb;
		queryEnd = qe;
		queryReplied = false;
		queryActive = false;
		super.setSender(pS);
	}
	
	public Query(String text, Location loc, Time qb, Time qe,String pS,boolean active) 
	{
		super.setTextData(text);
		super.setLocation(loc);
		queryBegin = qb;
		queryEnd = qe;
		queryReplied = false;
		queryActive = active;
		super.setSender(pS);
	}
	
	public void QuerySetTimeWindow() 
	{
		// A little complex to code right now, but the constructor satisfies
		// what Milestone 1 requires.
	}

	public String getQueryReply()
	{
		return queryReply;
	}
	
	public void replyToQuery(String s) 
	{
		queryReply = s;
		queryReplied = true; // We might not need this if the Query object is immediately removed
	}
	
	public boolean getQueryReplied()
	{
		return queryReplied;
	}
	
	public void setQueryReplied(boolean b)
	{
		queryReplied = b;
	}
	
	public boolean getQueryActive()
	{
		return queryActive;
	}
	
	public void setQueryActive(boolean b)
	{
		queryActive = b;
	}
	
	public Time getQueryBegin()
	{
		return queryBegin;
	}

	public void setQueryBegin(Time t)
	{
		queryBegin = t;
	}
	
	public Time getQueryEnd()
	{
		return queryEnd;
	}

	public void setQueryEnd(Time t)
	{
		queryEnd = t;
	}
	
	private String queryReply;
	private boolean queryReplied;
	private boolean queryActive;
	private Time queryBegin;
	private Time queryEnd;
}