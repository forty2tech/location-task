package ecs160.project.locationtask;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AlbumAdapter extends ArrayAdapter<MyAlbum> {

	private List<MyAlbum> items;
    private Activity context;
    
    static class ViewHolder {
		public TextView toptext;
	}
	
	public AlbumAdapter(Activity context, List<MyAlbum> items) {
        super(context, R.layout.choose_album, items);
        this.context = context;
        this.items = items;
    }
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
            if (rowView == null) {
            	LayoutInflater inflater = context.getLayoutInflater();
    			rowView = inflater.inflate(R.layout.choose_album, null);
    			
    			ViewHolder viewHolder = new ViewHolder();
    			viewHolder.toptext = (TextView) rowView.findViewById(R.id.album_toptext);
    			rowView.setTag(viewHolder);
            }
            
            ViewHolder holder = (ViewHolder) rowView.getTag();
            MyAlbum album = items.get(position);
            boolean flag = album.isNoAlbum();
            if (!flag){
            	String a = album.getAlbumTitle();
	            holder.toptext.setText(a);
	        }
            else{
            	holder.toptext.setText("No Album");
	        }
            return rowView;
    }    
}
