package ecs160.project.locationtask;

import java.util.ArrayList;

import ecs160.project.locationtask.GeoMessageAdapter.ViewHolder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FavoriteAdapter extends ArrayAdapter<Favorite> {
	private ArrayList<Favorite> items;
    private Activity context;
	
    static class ViewHolder {
		public TextView toptext;
		public TextView bottomtext;
	}
    
    public FavoriteAdapter(Activity context, ArrayList<Favorite> items) {
            super(context, R.layout.favorite, items);
            this.context = context;
            this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
            	LayoutInflater inflater = context.getLayoutInflater();
    			rowView = inflater.inflate(R.layout.favorite, null);
    			
    			ViewHolder viewHolder = new ViewHolder();
    			viewHolder.toptext = (TextView) rowView.findViewById(R.id.fav_toptext);
    			viewHolder.bottomtext = (TextView) rowView.findViewById(R.id.fav_bottomtext);
    			rowView.setTag(viewHolder);
            }
            
            ViewHolder holder = (ViewHolder) rowView.getTag();
            Favorite m = items.get(position);
            boolean flag = m.getNoFavFlag();
            if (!flag){
	            String s = m.getName();
	            holder.toptext.setText(s);
	            Location loc = m.getLocation();
	            holder.bottomtext.setText("Latitude = "+loc.getLatitude()+", Longitude = "+loc.getLongitude());
	        }
            else{
            	holder.toptext.setText("No Favoriate Location");
	            holder.bottomtext.setText("Latitude = 0.0, Longitude = 0.0");
	        }
            return rowView;
    }    
}
