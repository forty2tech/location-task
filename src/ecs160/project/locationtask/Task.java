package ecs160.project.locationtask;

public class Task extends Communication
{
	public Task() {taskActive=true;}
	
	public Task(String text, Location loc, Time begin, Time end)
	{
		super.setTextData(text);
		super.setLocation(loc);
		taskBegin = begin;
		taskEnd = end;
		taskActive=true;
	}
	
	public Task(String text, Location loc, Time begin, Time end,String pT)
	{
		super.setTextData(text);
		super.setLocation(loc);
		taskBegin = begin;
		taskEnd = end;
		taskActive=true;
		super.setSender(pT);
	}
	
	public Task(String text, Location loc, Time begin, Time end,String pSender,boolean active)
	{
		super.setTextData(text);
		super.setLocation(loc);
		taskBegin = begin;
		taskEnd = end;
		taskActive=active;
		super.setSender(pSender);
	}
	
	public boolean getTaskActive()
	{
		return taskActive;
	}
	
	public void setTaskActive(boolean b)
	{
		taskActive = b;
	}
	
	public Time getTaskBegin()
	{
		return taskBegin;
	}
	
	public void setTaskBegin(Time t)
	{
		taskBegin = t;
	}
	
	public Time getTaskEnd()
	{
		return taskEnd;
	}
	
	public void setTaskEnd(Time t)
	{
		taskEnd = t;
	}
	
	private boolean taskActive;
	private Time taskBegin;
	private Time taskEnd;
}