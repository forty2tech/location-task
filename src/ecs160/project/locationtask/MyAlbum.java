package ecs160.project.locationtask;

import java.util.List;

import com.google.gdata.data.photos.GphotoEntry;

public class MyAlbum {
	
	private String title;
	private String id;
	private boolean noAlbumFlag;
	
	public MyAlbum(){
		this.title = null;
		this.id = null;
		this.noAlbumFlag = false;
	}
	
	public MyAlbum(boolean value){
		this.noAlbumFlag = value;
	}
	
	public MyAlbum(String ptitle, String pid){
		this.title = ptitle;
		this.id = pid;
		this.noAlbumFlag = false;
	}
	
	public void setAlbumTitle(String ptitle){
		this.title = ptitle;
	}
	
	public void setAlbumId(String pid){
		this.id = pid;
	}
	
	public String getAlbumTitle(){
		return this.title;
	}
	
	public String getAlbumId(){ 
		return this.id;
	}
	
	public boolean isNoAlbum(){
		return this.noAlbumFlag;
	}
}
