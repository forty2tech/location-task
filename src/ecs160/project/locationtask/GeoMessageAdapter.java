package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import ecs160.project.locationtask.MessageAdapter.ViewHolder;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GeoMessageAdapter extends ArrayAdapter<GeoMessage> {
	private ArrayList<GeoMessage> items;
    private Activity context;
    private boolean isFrom;
	
    static class ViewHolder {
		public TextView toptext;
		public TextView bottomtext;
		public ImageView icon;
	}
    
    public GeoMessageAdapter(Activity context, ArrayList<GeoMessage> items, boolean isfrom) {
            super(context, R.layout.geomessage, items);
            this.context = context;
            this.items = items;
            this.isFrom = isfrom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
            	LayoutInflater inflater = context.getLayoutInflater();
    			rowView = inflater.inflate(R.layout.geomessage, null);
    			
    			ViewHolder viewHolder = new ViewHolder();
    			viewHolder.toptext = (TextView) rowView.findViewById(R.id.geomsg_toptext);
    			viewHolder.bottomtext = (TextView) rowView.findViewById(R.id.geomsg_bottomtext);
    			viewHolder.icon = (ImageView) rowView.findViewById(R.id.geomsg_icon);
    			viewHolder.icon.setMaxHeight(30);
    			viewHolder.icon.setMaxWidth(30);
    			rowView.setTag(viewHolder);
            }
            
            ViewHolder holder = (ViewHolder) rowView.getTag();
            GeoMessage m = items.get(position);
            String from = "From ";
            String to = "To ";
            boolean flag = m.getNoGeoFlag();
            if (!flag){
	            String raw_sender = m.getSender();
	            Log.i("GEO-MSG ADAPTER","raw_sender = "+raw_sender);
	            if (isFrom){
	            	if (raw_sender.length() > 10 ){
	            		String mod_sender = raw_sender.substring(0, 1) + "-(" + raw_sender.substring(1, 4) + ")-"
		            		                + raw_sender.substring(4, 7) + "-" + raw_sender.substring(7);
	            		from += mod_sender + " :";
	            	}
	            	else
	            		from += raw_sender + ":";
		           holder.toptext.setText(from);
	            }
	            else{
	            	to += raw_sender + ":";
	            	holder.toptext.setText(to);
	            }
	            holder.bottomtext.setText(m.getTextdata());
	            Drawable image = ImageOperations(getContext(), m.getPathURL(),"image.jpg");
	            holder.icon.setImageDrawable(image);
            }
            else{
            	holder.toptext.setText("");
	            holder.bottomtext.setText("No Geo-Message");
	            holder.icon.setImageResource(R.drawable.nonreply);

            }
            return rowView;
    } //end getView
	
	private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
	
		 try {
		   InputStream is = (InputStream) this.fetch(url);
		   Drawable d = Drawable.createFromStream(is, "src");		
		   return d;	
		 } catch (MalformedURLException e) {		
		   e.printStackTrace();		
		   return null;		
		 } catch (IOException e) {		
		   e.printStackTrace();		
		   return null;		
		 }	
	} // end ImageOperations
	
	
	
	public Object fetch(String address) throws MalformedURLException,IOException {
	
		 URL url = new URL(address);		
		 Object content = url.getContent();		
		 return content;		
	} // end fetch

    
}
