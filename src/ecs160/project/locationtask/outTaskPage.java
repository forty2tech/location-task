package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class outTaskPage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private int numOfoutTask =0;
	private ArrayList<Task> out_task_list = new ArrayList<Task>();
	private TaskAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		// Insert code here...
		LocationTaskActivity.myDATABASE.open();
		out_task_list.add(new Task("No Task", new Location(0.0,0.0),new Time(0,0,0,0,0,0), new Time(0,0,0,0,0,0)));
		
		// Receive and extras bundle -- get list of message
	    numOfoutTask = LocationTaskActivity.myDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
		if (numOfoutTask > 0){
			//out_task_list = LocationTaskActivity.myDATABASE.getAllTasksAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			out_task_list = LocationTaskActivity.myDATABASE.getAllTasks(LocationTaskActivity.username);
			Log.i("Self Storage", "Number of self storage task = "+Integer.toString(numOfoutTask));
		}
		
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new TaskAdapter(this, out_task_list,false);
        setListAdapter(this.adapter);
		LocationTaskActivity.myDATABASE.close();
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(outTaskPage.this,outbox.class);
				outTaskPage.this.startActivity(i);
			}
		});
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		//Insert code here...
				if(numOfoutTask>0){
					String cur_outtask       = out_task_list.get(position).getTextData();
					Double cur_outlatitude   = out_task_list.get(position).getLocation().getLatitude();
					Double cur_outlongitude  = out_task_list.get(position).getLocation().getLongitude();
					String toReceiver        = out_task_list.get(position).getSender();
					
					int out_sYr    = out_task_list.get(position).getTaskBegin().getYear();
					int out_sMonth = out_task_list.get(position).getTaskBegin().getMonth();
					int out_sDay   = out_task_list.get(position).getTaskBegin().getDay();
					int out_sHr    = out_task_list.get(position).getTaskBegin().getHour();
					int out_sMin   = out_task_list.get(position).getTaskBegin().getMinute();	
					
					int out_eYr    = out_task_list.get(position).getTaskEnd().getYear();
					int out_eMonth = out_task_list.get(position).getTaskEnd().getMonth();
					int out_eDay   = out_task_list.get(position).getTaskEnd().getDay();
					int out_eHr    = out_task_list.get(position).getTaskEnd().getHour();
					int out_eMin   = out_task_list.get(position).getTaskEnd().getMinute();
					
					Log.w("Self Storage","cur_sel_outtask = "+cur_outtask);
					
					// Update msg has been read
//					LocationTaskActivity.myDATABASE.open();
//					LocationTaskActivity.myDATABASE.updateMessage(LocationTaskActivity.username, new Message(cur_msg,new Location(cur_latitude,cur_longitude),true));
//					LocationTaskActivity.myDATABASE.close();
					
					// go to processing message
					Bundle outtask_bundle = new Bundle();
					outtask_bundle.putString("outtask", cur_outtask);
					outtask_bundle.putString("toReceiver", toReceiver);
					outtask_bundle.putDouble("outlatitude", cur_outlatitude);
					outtask_bundle.putDouble("outlongitude", cur_outlongitude);
					outtask_bundle.putInt("outsYear", out_sYr);
					outtask_bundle.putInt("outsMonth", out_sMonth);
					outtask_bundle.putInt("outsDay", out_sDay);
					outtask_bundle.putInt("outsHr", out_sHr);
					outtask_bundle.putInt("outsMin", out_sMin);
					outtask_bundle.putInt("outeYear", out_eYr);
					outtask_bundle.putInt("outeMonth", out_eMonth);
					outtask_bundle.putInt("outeDay", out_eDay);
					outtask_bundle.putInt("outeHr", out_eHr);
					outtask_bundle.putInt("outeMin", out_eMin);
					
					Intent i = new Intent(this, ProcessOutTask.class);
					i.putExtras(outtask_bundle);
					Log.i("onClick", "After Shift OUTtask Bundle");
					startActivity(i);
				}
	}
}
