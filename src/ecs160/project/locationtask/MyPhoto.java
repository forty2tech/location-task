package ecs160.project.locationtask;

public class MyPhoto {
	
	private String title;
	private String url;
	private String description;
	private ecs160.project.locationtask.Location loc;
	private boolean noPhotoFlag;
	
	public MyPhoto(){
		this.title = null;
		this.url = null;
		this.description = null;
		this.loc = new Location(0.0,0.0);
		this.noPhotoFlag = false;
	}
	
	public MyPhoto(boolean value){
		this.noPhotoFlag = value;
	}
	
	public MyPhoto(String ptitle, String purl, String pdescript, ecs160.project.locationtask.Location ploc){
		this.title = ptitle;
		this.url = purl;
		this.description = pdescript;
		this.loc = ploc;
		this.noPhotoFlag = false;
	}
	
	public void setPhotoTitle(String ptitle){
		this.title = ptitle;
	}
	
	public void setPhotoURL(String purl){
		this.url = purl;
	}
	
	public void setPhotoDescription(String pd){
		this.description = pd;
	}

	public void setPhotoLocation(Location ploc){
		this.loc = ploc;
	}
	
	public String getPhotoTitle(){
		return this.title;
	}
	
	public String getPhotoUrl(){ 
		return this.url;
	}
	
	public String getPhotoDescription(){
		return this.description;
	}
	
	public Location getPhotoLocation(){ 
		return this.loc;
	}
	
	public boolean isNoPhoto(){
		return this.noPhotoFlag;
	}
}
