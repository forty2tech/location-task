package ecs160.project.locationtask;

import java.util.ArrayList;


import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MessagePage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private ArrayList<Message> msg_list = new ArrayList<Message>();
    private MessageAdapter adapter;
	private int numOfMsg = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		LocationTaskActivity.cmnDATABASE.open();
		msg_list.add(new Message("",new Location(0,0),false,true));
		
		// Receive and extras bundle -- get list of message
		numOfMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
		if (numOfMsg > 0){
			//msg_list = LocationTaskActivity.cmnDATABASE.getAllMessagesAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			msg_list = LocationTaskActivity.cmnDATABASE.getAllMessages(LocationTaskActivity.username);
		}
       
		// Setup ListView
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		
		this.adapter = new MessageAdapter(this, msg_list,true);
        setListAdapter(this.adapter);
		
		LocationTaskActivity.cmnDATABASE.close();
		
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MessagePage.this,inbox.class);
				MessagePage.this.startActivity(i);
			}
		});
	}// end onCreate

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfMsg > 0){
			String cur_msg = msg_list.get(position).getTextData();
			cur_latitude   = msg_list.get(position).getLocation().getLatitude();
			cur_longitude  = msg_list.get(position).getLocation().getLongitude();
			String sender  = msg_list.get(position).getSender();
			Log.w("C2DM","cur_sel = "+cur_msg);
			
			// Update msg has been read
			LocationTaskActivity.cmnDATABASE.open();
			LocationTaskActivity.cmnDATABASE.updateMessage(LocationTaskActivity.username, new Message(cur_msg,new Location(cur_latitude,cur_longitude),sender,true));
			LocationTaskActivity.cmnDATABASE.close();
			
			// go to processing message
			Bundle msg_bundle = new Bundle();
			msg_bundle.putString("msg", cur_msg);
			msg_bundle.putString("sender", sender);
			msg_bundle.putDouble("latitude", cur_latitude);
			msg_bundle.putDouble("longitude", cur_longitude);
			
			Intent i = new Intent(this, ProcessMessage.class);
			i.putExtras(msg_bundle);
			Log.i("onClick", "After Shift MSg Bundle");
			startActivity(i);
		}
	}// end onListItemClick
	

}
