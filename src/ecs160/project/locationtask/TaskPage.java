package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class TaskPage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private int numOfTask =0;
	private ArrayList<Task> task_list = new ArrayList<Task>();
	private TaskAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		LocationTaskActivity.cmnDATABASE.open();
		task_list.add(new Task("No Task", new Location(0.0,0.0),new Time(0,0,0,0,0,0), new Time(0,0,0,0,0,0)));
		
		// Receive and extras bundle -- get list of message
	    numOfTask = LocationTaskActivity.cmnDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
		if (numOfTask > 0){
			//task_list = LocationTaskActivity.cmnDATABASE.getAllTasksAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			task_list = LocationTaskActivity.cmnDATABASE.getAllTasks(LocationTaskActivity.username);
		}
		
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new TaskAdapter(this, task_list,true);
        setListAdapter(this.adapter);
		LocationTaskActivity.cmnDATABASE.close();
		
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(TaskPage.this,inbox.class);
				TaskPage.this.startActivity(i);
			}
		});
	}// end onCreate
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if (numOfTask>0){
			String cur_task = task_list.get(position).getTextData();
			String sender   = task_list.get(position).getSender();
			cur_latitude    = task_list.get(position).getLocation().getLatitude();
			cur_longitude   = task_list.get(position).getLocation().getLongitude();
			Boolean cur_active = true;
			Time endTime   = task_list.get(position).getTaskEnd();
			Time startTime = task_list.get(position).getTaskBegin();
			
			final Calendar c = Calendar.getInstance();
		    int cur_Year = c.get(Calendar.YEAR);
		    int cur_Month = c.get(Calendar.MONTH) +1;
		    int cur_Day = c.get(Calendar.DAY_OF_MONTH);
		    int cur_Hr = c.get(Calendar.HOUR_OF_DAY);
		    int cur_Min = c.get(Calendar.MINUTE);
			 
		    // Check active status of Task
		 			if (cur_Year > endTime.getYear())
		 				cur_active = false;
		 			else if (cur_Year < endTime.getYear())
		 				cur_active = true;
		 			else{			// same year
		 				if (cur_Month > endTime.getMonth())
		 					cur_active = false;
		 				else if (cur_Month < endTime.getMonth())
		 					cur_active = true;
		 				else{		// same month
		 					if (cur_Day > endTime.getDay())
		 						cur_active = false;
		 					else if (cur_Day < endTime.getDay())
		 						cur_active = true;
		 					else{	// same day
		 						if (cur_Hr > endTime.getHour())
		 							cur_active = false;
		 						else if (cur_Hr < endTime.getHour())
		 							cur_active = true;
		 						else{//same hr
		 							if (cur_Min > endTime.getMinute())
		 								cur_active = false;
		 							else 
		 								cur_active = true;
		 						}
		 					}
		 				}
		 			}
		 
		 	if (!cur_active){
		 		LocationTaskActivity.cmnDATABASE.open();
				LocationTaskActivity.cmnDATABASE.updateTask(LocationTaskActivity.username, new Task(cur_task,new Location(cur_latitude,cur_longitude)
															,new Time(startTime.getYear(),startTime.getMonth(),startTime.getDay(),startTime.getHour(),startTime.getMinute(),0)
															,new Time(endTime.getYear(),endTime.getMonth(),endTime.getDay(),endTime.getHour(),endTime.getMinute(),0),sender,false));
				LocationTaskActivity.cmnDATABASE.close();
		 	}
		 			
		 			
			// go to processing task
			Bundle task_bundle = new Bundle();
			task_bundle.putString("task", cur_task);
			task_bundle.putString("sender", sender);
			task_bundle.putBoolean("active", cur_active);
			task_bundle.putDouble("latitude", cur_latitude);
			task_bundle.putDouble("longitude", cur_longitude);
			task_bundle.putInt("startYear", startTime.getYear());
			task_bundle.putInt("startMonth", startTime.getMonth());
			task_bundle.putInt("startDate", startTime.getDay());
			task_bundle.putInt("startHr", startTime.getHour());
			task_bundle.putInt("startMin", startTime.getMinute());
			task_bundle.putInt("endYear", endTime.getYear());
			task_bundle.putInt("endMonth", endTime.getMonth());
			task_bundle.putInt("endDate", endTime.getDay());
			task_bundle.putInt("endHr", endTime.getHour());
			task_bundle.putInt("endMin", endTime.getMinute());
			
			Intent i = new Intent(this, ProcessTask.class);
			i.putExtras(task_bundle);
			Log.i("onClick", "After Shift task Bundle");
			startActivity(i);
		}
	}// end onListItemClick
	
}
