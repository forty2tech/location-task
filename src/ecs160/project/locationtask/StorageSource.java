package ecs160.project.locationtask;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class StorageSource {
	// Database fields
		private SQLiteDatabase database;
		private StorageSQLiteHelper dbHelper;

		public StorageSource(Context context) 
		{
			dbHelper = new StorageSQLiteHelper(context);
		}

		public void open() throws SQLException 
		{
			database = dbHelper.getWritableDatabase();
		}

		public void close() 
		{
			dbHelper.close();
		}
		
		public int getTotalNumberMessages(String user)
		{
			String args[] = { user, String.valueOf(1) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getTotalNumberTasks(String user)
		{
			String args[] = { user, String.valueOf(2) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getTotalNumberQueries(String user)
		{
			String args[] = { user, String.valueOf(3) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getTotalNumberGeoMsg(String user)
		{
			String args[] = { user, String.valueOf(4) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																				StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getNumberMessagesAtLocation(String user, Location loc)
		{
			String args[] = { user, String.valueOf(1), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getNumberTasksAtLocation(String user, Location loc)
		{
			String args[] = { user, String.valueOf(2), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getNumberQueriesAtLocation(String user, Location loc)
		{
			String args[] = { user, String.valueOf(3), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public int getNumberGeoMsgAtLocation(String user, Location loc)
		{
			String args[] = { user, String.valueOf(4), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																				StorageSQLiteHelper.TYPE + " = ?" + " and " +
																				StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																				StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			return cursor.getCount();
		}
		
		public void updateMessage(String id, Message m) 
		{		
			String text = m.getTextData();
			double lo = m.getLocation().getLongitude();
			double la = m.getLocation().getLatitude();
			boolean b = m.getMessageRead();
			String args[] = { text, String.valueOf(lo), String.valueOf(la), id, m.getSender(), String.valueOf(1) };
			ContentValues cv = new ContentValues();
	        cv.put(StorageSQLiteHelper.HAS_READ, b);        
	        database.update(StorageSQLiteHelper.COMMUNICATION_TABLE, cv, StorageSQLiteHelper.MESSAGE + "=? and " +
	        														StorageSQLiteHelper.LONGITUDE + "=? and " + 
	        														StorageSQLiteHelper.LATITUDE + "=? and " + 
	        														StorageSQLiteHelper.USER_ID + "=? and " +
	        														StorageSQLiteHelper.SENDER_ID + "=? and " +
	        														StorageSQLiteHelper.TYPE + "=?", args);
		}

		public void storeMessage(String user, Message message)//(String user, String msg, double lat, double lon, boolean read) 
		{ 
			ContentValues values = new ContentValues();
			values.put(StorageSQLiteHelper.USER_ID, user);
			values.put(StorageSQLiteHelper.SENDER_ID, message.getSender());
			values.put(StorageSQLiteHelper.TYPE, 1);
			values.put(StorageSQLiteHelper.MESSAGE, message.getTextData());
			values.put(StorageSQLiteHelper.LATITUDE, message.getLocation().getLatitude());
			values.put(StorageSQLiteHelper.LONGITUDE, message.getLocation().getLongitude());
			values.put(StorageSQLiteHelper.HAS_READ, message.getMessageRead());
			database.insert(StorageSQLiteHelper.COMMUNICATION_TABLE, null, values);
		}
		
		public void storeTask(String user, Task task)//(String user, String msg, double lat, double lon, boolean read) 
		{ 
			ContentValues values = new ContentValues();
			values.put(StorageSQLiteHelper.USER_ID, user);
			values.put(StorageSQLiteHelper.SENDER_ID, task.getSender());
			values.put(StorageSQLiteHelper.TYPE, 2);
			values.put(StorageSQLiteHelper.MESSAGE, task.getTextData());
			values.put(StorageSQLiteHelper.LATITUDE, task.getLocation().getLatitude());
			values.put(StorageSQLiteHelper.LONGITUDE, task.getLocation().getLongitude());
			
			values.put(StorageSQLiteHelper.COMMUNICATION_ACTIVE, task.getTaskActive());
			values.put(StorageSQLiteHelper.BEGIN_YEAR, task.getTaskBegin().getYear());
			values.put(StorageSQLiteHelper.BEGIN_MON, task.getTaskBegin().getMonth());
			values.put(StorageSQLiteHelper.BEGIN_DAY, task.getTaskBegin().getDay());
			values.put(StorageSQLiteHelper.BEGIN_HOUR, task.getTaskBegin().getHour());
			values.put(StorageSQLiteHelper.BEGIN_MIN, task.getTaskBegin().getMinute());
			values.put(StorageSQLiteHelper.BEGIN_SEC, task.getTaskBegin().getSecond());
			values.put(StorageSQLiteHelper.END_YEAR, task.getTaskEnd().getYear());
			values.put(StorageSQLiteHelper.END_MON, task.getTaskEnd().getMonth());
			values.put(StorageSQLiteHelper.END_DAY, task.getTaskEnd().getDay());
			values.put(StorageSQLiteHelper.END_HOUR, task.getTaskEnd().getHour());
			values.put(StorageSQLiteHelper.END_MIN, task.getTaskEnd().getMinute());
			values.put(StorageSQLiteHelper.END_SEC, task.getTaskEnd().getSecond());
			
			database.insert(StorageSQLiteHelper.COMMUNICATION_TABLE, null, values);
		}
		
		public void storeQuery(String user, Query query) 
		{ 
			ContentValues values = new ContentValues();
			values.put(StorageSQLiteHelper.USER_ID, user);
			values.put(StorageSQLiteHelper.SENDER_ID, query.getSender());
			values.put(StorageSQLiteHelper.TYPE, 3);
			values.put(StorageSQLiteHelper.MESSAGE, query.getTextData());
			values.put(StorageSQLiteHelper.LATITUDE, query.getLocation().getLatitude());
			values.put(StorageSQLiteHelper.LONGITUDE, query.getLocation().getLongitude());
			
			values.put(StorageSQLiteHelper.COMMUNICATION_ACTIVE, query.getQueryActive());
			values.put(StorageSQLiteHelper.QUERY_REPLY, query.getQueryReply());
			values.put(StorageSQLiteHelper.QUERY_REPLIED, query.getQueryReplied());
			values.put(StorageSQLiteHelper.BEGIN_YEAR, query.getQueryBegin().getYear());
			values.put(StorageSQLiteHelper.BEGIN_MON, query.getQueryBegin().getMonth());
			values.put(StorageSQLiteHelper.BEGIN_DAY, query.getQueryBegin().getDay());
			values.put(StorageSQLiteHelper.BEGIN_HOUR, query.getQueryBegin().getHour());
			values.put(StorageSQLiteHelper.BEGIN_MIN, query.getQueryBegin().getMinute());
			values.put(StorageSQLiteHelper.BEGIN_SEC, query.getQueryBegin().getSecond());
			values.put(StorageSQLiteHelper.END_YEAR, query.getQueryEnd().getYear());
			values.put(StorageSQLiteHelper.END_MON, query.getQueryEnd().getMonth());
			values.put(StorageSQLiteHelper.END_DAY, query.getQueryEnd().getDay());
			values.put(StorageSQLiteHelper.END_HOUR, query.getQueryEnd().getHour());
			values.put(StorageSQLiteHelper.END_MIN, query.getQueryEnd().getMinute());
			values.put(StorageSQLiteHelper.END_SEC, query.getQueryEnd().getSecond());
			
			database.insert(StorageSQLiteHelper.COMMUNICATION_TABLE, null, values);
		}

		public void storeGeoMsg(String user, GeoMessage geomessage)//(String user, String msg, double lat, double lon, boolean read) 
		{ 
			Log.i("saveGEO", "In storeGeoMsg");
			ContentValues values = new ContentValues();
			values.put(StorageSQLiteHelper.USER_ID, user);
			values.put(StorageSQLiteHelper.SENDER_ID, geomessage.getSender());
			values.put(StorageSQLiteHelper.TYPE, 4);
			values.put(StorageSQLiteHelper.MESSAGE, geomessage.getTextdata());
			values.put(StorageSQLiteHelper.LATITUDE, geomessage.getGeoLocation().getLatitude());
			values.put(StorageSQLiteHelper.LONGITUDE, geomessage.getGeoLocation().getLongitude());
			values.put(StorageSQLiteHelper.IMAGE_URL, geomessage.getPathURL());
			database.insert(StorageSQLiteHelper.COMMUNICATION_TABLE, null, values);
			Log.i("saveGEO", "After storeGeoMsg");
		}
		
		public ArrayList<Message> getAllMessagesAtLocation(String user, Location loc)
		{
			ArrayList<Message> messages = new ArrayList<Message>();
		
			String args[] = { user, String.valueOf(1), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				messages.add(cursorToMessage(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return messages;
		}
		
		public ArrayList<Message> getAllMessages(String user)
		{
			ArrayList<Message> messages = new ArrayList<Message>();
		
			String args[] = { user, String.valueOf(1)};
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				messages.add(cursorToMessage(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return messages;
		}
		
		public ArrayList<Task> getAllTasksAtLocation(String user, Location loc)
		{
			ArrayList<Task> tasks = new ArrayList<Task>();
		
			String args[] = { user, String.valueOf(2), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				tasks.add(cursorToTask(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return tasks;
		}
		
		public ArrayList<Task> getAllTasks(String user)
		{
			ArrayList<Task> tasks = new ArrayList<Task>();
		
			String args[] = { user, String.valueOf(2)};
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				tasks.add(cursorToTask(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return tasks;
		}

		public ArrayList<Query> getAllQueriesAtLocation(String user, Location loc)
		{
			ArrayList<Query> queries = new ArrayList<Query>();
		
			String args[] = { user, String.valueOf(3), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?" + " and " +
																			   StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																			   StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				queries.add(cursorToQuery(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return queries;
		}
		
		public ArrayList<Query> getAllQueries(String user)
		{
			ArrayList<Query> queries = new ArrayList<Query>();
		
			String args[] = { user, String.valueOf(3)};
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																		       StorageSQLiteHelper.TYPE + " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				queries.add(cursorToQuery(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return queries;
		}
		
		public ArrayList<GeoMessage> getAllGeoMsgAtLocation(String user, Location loc)
		{
			ArrayList<GeoMessage> geomsg = new ArrayList<GeoMessage>();
		
			String args[] = { user, String.valueOf(4), String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()) };
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																				StorageSQLiteHelper.TYPE + " = ?" + " and " +
																				StorageSQLiteHelper.LATITUDE + " = ?" + " and " +
																				StorageSQLiteHelper.LONGITUDE	+ " = ?", 
																			   args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				geomsg.add(cursorToGeoMessage(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return geomsg;
		}
		
		public ArrayList<GeoMessage> getAllGeoMsg(String user)
		{
			ArrayList<GeoMessage> geomsg = new ArrayList<GeoMessage>();
		
			String args[] = { user, String.valueOf(4)};
			Cursor cursor = database.query(StorageSQLiteHelper.COMMUNICATION_TABLE, null, StorageSQLiteHelper.USER_ID + " = ?" + " and " +
																				StorageSQLiteHelper.TYPE + " = ?", 
																			    args, null, null, null);
			cursor.moveToFirst(); //moves cursor to the first row
			while(!cursor.isAfterLast())
			{
				geomsg.add(cursorToGeoMessage(cursor)); //convert each query row to a message and store it in the list
				cursor.moveToNext(); //advance the cursor to the next row
			}
			cursor.close();
			return geomsg;
		}
		
		public void deleteMessage(String user, Message msg) 
		{
			String args[] = { user, msg.getSender(), String.valueOf(1), msg.getTextData(), String.valueOf(msg.getLocation().getLatitude()), String.valueOf(msg.getLocation().getLongitude()) };
			
			int count = database.delete(StorageSQLiteHelper.COMMUNICATION_TABLE, StorageSQLiteHelper.USER_ID + " = ? and " +
														  StorageSQLiteHelper.SENDER_ID + " = ? and " +
														  StorageSQLiteHelper.TYPE + " = ? and " +
														  StorageSQLiteHelper.MESSAGE + " = ?  and " +
														  StorageSQLiteHelper.LATITUDE + " = ? and " + 
														  StorageSQLiteHelper.LONGITUDE + " = ?"
														  , args);
			
			Log.w("C2DM","num of row deleted "+ count);
		}
		
		public void deleteTask(String user, Task tsk) 
		{
			String args[] = { user, tsk.getSender(), String.valueOf(2), tsk.getTextData(), 
							  String.valueOf(tsk.getLocation().getLatitude()), String.valueOf(tsk.getLocation().getLongitude()), 
							  String.valueOf(tsk.getTaskBegin().getYear()), String.valueOf(tsk.getTaskBegin().getMonth()), 
							  String.valueOf(tsk.getTaskBegin().getDay()), String.valueOf(tsk.getTaskBegin().getHour()), 
							  String.valueOf(tsk.getTaskBegin().getMinute()), String.valueOf(tsk.getTaskBegin().getSecond()),
							  String.valueOf(tsk.getTaskEnd().getYear()), String.valueOf(tsk.getTaskEnd().getMonth()), 
							  String.valueOf(tsk.getTaskEnd().getDay()), String.valueOf(tsk.getTaskEnd().getHour()), 
							  String.valueOf(tsk.getTaskEnd().getMinute()), String.valueOf(tsk.getTaskEnd().getSecond()) };
			
			database.delete(StorageSQLiteHelper.COMMUNICATION_TABLE, StorageSQLiteHelper.USER_ID + " = ? and " +
														  StorageSQLiteHelper.SENDER_ID + " = ? and " +
														  StorageSQLiteHelper.TYPE + " = ? and " +
														  StorageSQLiteHelper.MESSAGE + " = ? and " +
														  StorageSQLiteHelper.LATITUDE + " = ? and " + 
														  StorageSQLiteHelper.LONGITUDE + " = ? and " + 
														  StorageSQLiteHelper.BEGIN_YEAR + " = ? and " +
														  StorageSQLiteHelper.BEGIN_MON + " = ? and " +
														  StorageSQLiteHelper.BEGIN_DAY + " = ? and " +
														  StorageSQLiteHelper.BEGIN_HOUR + " = ? and " +
														  StorageSQLiteHelper.BEGIN_MIN + " = ? and " +
														  StorageSQLiteHelper.BEGIN_SEC + " = ? and " +
														  StorageSQLiteHelper.END_YEAR + " = ? and " +
														  StorageSQLiteHelper.END_MON + " = ? and " +
														  StorageSQLiteHelper.END_DAY + " = ? and " +
														  StorageSQLiteHelper.END_HOUR + " = ? and " +
														  StorageSQLiteHelper.END_MIN + " = ? and " +
														  StorageSQLiteHelper.END_SEC + " = ?", args);
		}
		
		public void deleteQuery(String user, Query q) 
		{
			String args[] = { user, q.getSender(), String.valueOf(3), q.getTextData(), String.valueOf(q.getLocation().getLatitude()),
					String.valueOf(q.getLocation().getLongitude()),
					
					String.valueOf(q.getQueryBegin().getYear()), String.valueOf(q.getQueryBegin().getMonth()), 
					String.valueOf(q.getQueryBegin().getDay()), String.valueOf(q.getQueryBegin().getHour()), 
					String.valueOf(q.getQueryBegin().getMinute()), String.valueOf(q.getQueryBegin().getSecond()),
					String.valueOf(q.getQueryEnd().getYear()), String.valueOf(q.getQueryEnd().getMonth()), 
					String.valueOf(q.getQueryEnd().getDay()), String.valueOf(q.getQueryEnd().getHour()), 
					String.valueOf(q.getQueryEnd().getMinute()), String.valueOf(q.getQueryEnd().getSecond())};
			
			database.delete(StorageSQLiteHelper.COMMUNICATION_TABLE, StorageSQLiteHelper.USER_ID + " = ? and " +
														  StorageSQLiteHelper.SENDER_ID + " = ? and " +
														  StorageSQLiteHelper.TYPE + " = ? and " +
														  StorageSQLiteHelper.MESSAGE + " = ? and " +
														  StorageSQLiteHelper.LATITUDE + " = ? and " + 
														  StorageSQLiteHelper.LONGITUDE + " = ? and " +
														  StorageSQLiteHelper.BEGIN_YEAR + " = ? and " +
														  StorageSQLiteHelper.BEGIN_MON + " = ? and " +
														  StorageSQLiteHelper.BEGIN_DAY + " = ? and " +
														  StorageSQLiteHelper.BEGIN_HOUR + " = ? and " +
														  StorageSQLiteHelper.BEGIN_MIN + " = ? and " +
														  StorageSQLiteHelper.BEGIN_SEC + " = ? and " +
														  StorageSQLiteHelper.END_YEAR + " = ? and " +
														  StorageSQLiteHelper.END_MON + " = ? and " +
														  StorageSQLiteHelper.END_DAY + " = ? and " +
														  StorageSQLiteHelper.END_HOUR + " = ? and " +
														  StorageSQLiteHelper.END_MIN + " = ? and " +
														  StorageSQLiteHelper.END_SEC + " = ?", args);
		}
		
		public void deleteGeoMessage(String user, GeoMessage msg) 
		{
			String args[] = { user, msg.getSender(), String.valueOf(4), msg.getTextdata(), String.valueOf(msg.getGeoLocation().getLatitude()), String.valueOf(msg.getGeoLocation().getLongitude()), msg.getPathURL() };
			
			int count = database.delete(StorageSQLiteHelper.COMMUNICATION_TABLE, StorageSQLiteHelper.USER_ID + " = ? and " +
														StorageSQLiteHelper.SENDER_ID + " = ? and " +
														StorageSQLiteHelper.TYPE + " = ? and " +
														StorageSQLiteHelper.MESSAGE + " = ?  and " +
														StorageSQLiteHelper.LATITUDE + " = ? and " + 
														StorageSQLiteHelper.LONGITUDE + " = ? and " +
														StorageSQLiteHelper.IMAGE_URL + " = ?"
														  , args);
			
			Log.w("C2DM","GeoMSG num of row deleted "+ count);
		}

		private Message cursorToMessage(Cursor cursor) 
		{
			Message message = new Message();
			message.setSender(cursor.getString(1));
			message.setTextData(cursor.getString(3));
			message.setLocation(new Location(cursor.getDouble(4), cursor.getDouble(5)));
			if(cursor.getInt(6) == 1)
				message.setMessageRead(true);
			else
				message.setMessageRead(false);
			return message;
		}
		
		private Task cursorToTask(Cursor cursor) 
		{
			Task task = new Task();
			task.setSender(cursor.getString(1));
			task.setTextData(cursor.getString(3));
			task.setLocation(new Location(cursor.getDouble(4), cursor.getDouble(5)));
			if(cursor.getInt(7) == 1)
				task.setTaskActive(true);
			else
				task.setTaskActive(false);
			task.setTaskBegin(new Time(cursor.getInt(8), 
									   cursor.getInt(9), 
									   cursor.getInt(10),
									   cursor.getInt(11),
									   cursor.getInt(12),
									   cursor.getInt(13)));
			task.setTaskEnd(new Time(cursor.getInt(14), 
								     cursor.getInt(15), 
								     cursor.getInt(16),
								     cursor.getInt(17),
								     cursor.getInt(18),
								     cursor.getInt(19)));
			return task;
		}
		
		private Query cursorToQuery(Cursor cursor) 
		{
			Query query = new Query();
			query.setSender(cursor.getString(1));
			query.setTextData(cursor.getString(3));
			query.setLocation(new Location(cursor.getDouble(4), cursor.getDouble(5)));
			if(cursor.getInt(7) == 1)
				query.setQueryActive(true);
			else
				query.setQueryActive(false);
			if(cursor.getInt(21) == 1)
				query.setQueryReplied(true);
			else
				query.setQueryReplied(false);
			query.setQueryBegin(new Time(cursor.getInt(8), 
					   					 cursor.getInt(9), 
					   					 cursor.getInt(10),
					   					 cursor.getInt(11),
					   					 cursor.getInt(12),
					   					 cursor.getInt(13)));
			query.setQueryEnd(new Time(cursor.getInt(14), 
				     				   cursor.getInt(15), 
				     				   cursor.getInt(16),
				     				   cursor.getInt(17),
				     				   cursor.getInt(18),
				     				   cursor.getInt(19)));
			query.replyToQuery(cursor.getString(20));
			return query;
		}
		
		private GeoMessage cursorToGeoMessage(Cursor cursor) 
		{
			GeoMessage geomsg = new GeoMessage();
			geomsg.setSender(cursor.getString(1));
			geomsg.setTextdata(cursor.getString(3));
			geomsg.setGeoLocation(new Location(cursor.getDouble(4), cursor.getDouble(5)));
			geomsg.setPathURL(cursor.getString(22));
			return geomsg;
		}
}
