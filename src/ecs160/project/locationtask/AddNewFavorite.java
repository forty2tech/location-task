package ecs160.project.locationtask;

import java.util.List;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddNewFavorite extends MapActivity {

	Button fav_cancel,fav_confirm,fav_add;
	String favoriteName="No Location";
	Double fav_latitude=0.0;
	Double fav_longitude=0.0;
	
  // map view avaliables
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  private ecs160.project.locationtask.Location locationData = null;
  
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_new_fav);
		
		// init the map
		initMapView();
		
		// CANCEL BUTTON -- CANCEL ADD NEW FAVORITE LOCATION 
		fav_cancel = (Button)findViewById(R.id.fav_cancel_button);
		fav_cancel.setOnClickListener(new View.OnClickListener() {
			
		 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(AddNewFavorite.this,FavoritePage.class);
				AddNewFavorite.this.startActivity(i);
			}
		});
		// CONFIRM BUTTON -- SHOW THE LOCATION ON THE MAP
		fav_confirm = (Button)findViewById(R.id.fav_confirm_button);
		fav_confirm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Handle showing on the map
			  locationData = null;
			  cleanCustomItem();
		       EditText locLabel_input = (EditText) findViewById(R.id.fav_addr_editText);
		       String locLabel = locLabel_input.getText().toString().trim();
		       
		       EditText locationName_input = (EditText)findViewById(R.id.fav_name_editText);
		        favoriteName = locationName_input.getText().toString().trim();
		        
		       locationData = Make.translateToAddressFromName(locLabel);
		       fav_latitude = locationData.getLatitude();
		       fav_longitude = locationData.getLongitude();
		       createMarker(locLabel, favoriteName, fav_latitude, fav_longitude, "");
		       zoomInToPoint();
			}
		});
		
		// ADD BUTTON -- ADD NEW FAVORITE LOCATION TO THE LIST 
		fav_add = (Button)findViewById(R.id.fav_add_button);
		fav_add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Handle adding to the database
				EditText locationName_input = (EditText)findViewById(R.id.fav_name_editText);
				favoriteName = locationName_input.getText().toString().trim();
				
				EditText locationAddr_input = (EditText)findViewById(R.id.fav_addr_editText);
				
				LocationTaskActivity.favDATABASE.open();
				LocationTaskActivity.favDATABASE.storeFavoritePlace(LocationTaskActivity.username, new Favorite(new Location(fav_latitude,fav_longitude),favoriteName));
				LocationTaskActivity.favDATABASE.close();
				
				
				Intent i = new Intent(AddNewFavorite.this,FavoritePage.class);
				AddNewFavorite.this.startActivity(i);
			}
		});
		
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	
	 // map set up
  private void initMapView() {
    mapView = (MapView) findViewById(R.id.add_new_fav_mapView);
    mapView.setBuiltInZoomControls(true);
    mapView.displayZoomControls(true);
    mapView.setSatellite(false);
    
    mapController = mapView.getController();
    mapOverlays = mapView.getOverlays();
    drawable = getResources().getDrawable(R.drawable.marker2);
    customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
  }
  public void cleanCustomItem() {
    customItemizedOverlay.getOverLays().clear();
  }
  
  public void zoomInToPoint() {
    GeoPoint point = new GeoPoint((int)(fav_latitude *1e6), (int)(fav_longitude * 1e6));
    //mapController.animateTo(point);
    mapController.setZoom(16);
    mapController.setCenter(point);
  }
  
  public void createMarker(String text, String sender, double lat, double log, String img_url) {
      GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
      CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
      customItemizedOverlay.addOverlay(overlayItem);
      mapOverlays.add(customItemizedOverlay);
  }
}
