package ecs160.project.locationtask;

public class Favorite {
	private Location location;
	private String name;
	private boolean noFavFlag;
	
	public Favorite(){
		this.location = new Location(0,0);
		this.name = null;
		this.noFavFlag = false;
	}
	
	public Favorite(boolean parm){
		this.location = new Location(0,0);
		this.name = null;
		this.noFavFlag = parm;
	}
	
	public Favorite(Location loc, String pname){
		this.location = loc;
		this.name = pname;
		this.noFavFlag = false;
	}
	
	public void setLocation(Location parm){
		this.location = parm;
	}
	
	public void setName (String parm){
		this.name = parm;
	}
	
	public Location getLocation(){
		return this.location;
	}
	
	public String getName(){
		return this.name;
	}
	
	public boolean getNoFavFlag(){
		return this.noFavFlag;
	}
}
