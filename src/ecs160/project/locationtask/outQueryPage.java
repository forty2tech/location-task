package ecs160.project.locationtask;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class outQueryPage extends ListActivity {
	Double cur_latitude=0.0;
	Double cur_longitude=0.0;
	
	private int numOfoutQuery = 0;
	private ArrayList<Query> out_query_list = new ArrayList<Query>();
	private QueryAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		LocationTaskActivity.myDATABASE.open();
		out_query_list.add(new Query("",new Location(0,0),new Time(0,0,0,0,0,0),new Time(0,0,0,0,0,0)));
		
		// Receive and extras bundle -- get list of message
		numOfoutQuery = LocationTaskActivity.myDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
		if (numOfoutQuery > 0){
			//out_query_list = LocationTaskActivity.myDATABASE.getAllQueriesAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
			out_query_list = LocationTaskActivity.myDATABASE.getAllQueries(LocationTaskActivity.username);
			Log.i("Self Storage", "Number of self storage query = "+Integer.toString(numOfoutQuery));
		} 
		
		//Add ListView into xml design
		View home_xml = getLayoutInflater().inflate(R.layout.backfuntion, null);
		ListView lv = getListView();
		lv.addFooterView(home_xml);
		this.adapter = new QueryAdapter(this, out_query_list,false);
		setListAdapter(adapter);
		LocationTaskActivity.myDATABASE.close();
		Button backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(outQueryPage.this,outbox.class);
				outQueryPage.this.startActivity(i);
			}
		});
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		if(numOfoutQuery>0){
			String cur_outquery      = out_query_list.get(position).getTextData();
			Double cur_outlatitude   = out_query_list.get(position).getLocation().getLatitude();
			Double cur_outlongitude  = out_query_list.get(position).getLocation().getLongitude();
			String toReceiver        = out_query_list.get(position).getSender();
			
			int out_sYr    = out_query_list.get(position).getQueryBegin().getYear();
			int out_sMonth = out_query_list.get(position).getQueryBegin().getMonth();
			int out_sDay   = out_query_list.get(position).getQueryBegin().getDay();
			int out_sHr    = out_query_list.get(position).getQueryBegin().getHour();
			int out_sMin   = out_query_list.get(position).getQueryBegin().getMinute();	
			
			int out_eYr    = out_query_list.get(position).getQueryEnd().getYear();
			int out_eMonth = out_query_list.get(position).getQueryEnd().getMonth();
			int out_eDay   = out_query_list.get(position).getQueryEnd().getDay();
			int out_eHr    = out_query_list.get(position).getQueryEnd().getHour();
			int out_eMin   = out_query_list.get(position).getQueryEnd().getMinute();
			
			Log.w("Self Storage","cur_sel_outquery = "+cur_outquery);
			
			// Update msg has been read
//			LocationTaskActivity.myDATABASE.open();
//			LocationTaskActivity.myDATABASE.updateMessage(LocationTaskActivity.username, new Message(cur_msg,new Location(cur_latitude,cur_longitude),true));
//			LocationTaskActivity.myDATABASE.close();
			
			// go to processing message
			Bundle outquery_bundle = new Bundle();
			outquery_bundle.putString("outquery", cur_outquery);
			outquery_bundle.putString("toReceiver", toReceiver);
			outquery_bundle.putDouble("outlatitude", cur_outlatitude);
			outquery_bundle.putDouble("outlongitude", cur_outlongitude);
			outquery_bundle.putInt("outsYear", out_sYr);
			outquery_bundle.putInt("outsMonth", out_sMonth);
			outquery_bundle.putInt("outsDay", out_sDay);
			outquery_bundle.putInt("outsHr", out_sHr);
			outquery_bundle.putInt("outsMin", out_sMin);
			outquery_bundle.putInt("outeYear", out_eYr);
			outquery_bundle.putInt("outeMonth", out_eMonth);
			outquery_bundle.putInt("outeDay", out_eDay);
			outquery_bundle.putInt("outeHr", out_eHr);
			outquery_bundle.putInt("outeMin", out_eMin);
			
			Intent i = new Intent(this, ProcessOutQuery.class);
			i.putExtras(outquery_bundle);
			Log.i("onClick", "After Shift OUTquery Bundle");
			startActivity(i);
		}
	}
}
