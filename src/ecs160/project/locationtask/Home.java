package ecs160.project.locationtask;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Home extends MapActivity {

	ImageButton inbox_but, outbox_but,send_but,favorite_but,settings_but;
	private LocationSensitiveServiceCustomizationBroadcastReceiver LSSC_BroadcastReceiver;
	// map
	private LocationManager locationManager = null;
  private double latituteLoc = 0;
  private double longitudeLoc = 0;
  private String provider;
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  private Location location;
  private double avg_latitude;
  private double avg_longitude;
  private int total_points;
  
  // all messages, tasks, queue, geomessage
  private ArrayList<Message> msg_list = new ArrayList<Message>();
  private int numOfMsg = 0;
  private ArrayList<Task> task_list = new ArrayList<Task>();
  private int numOfTask = 0;
  private ArrayList<Query> query_list = new ArrayList<Query>();
  private int numOfQuery = 0;
  private ArrayList<GeoMessage> geomsg_list = new ArrayList<GeoMessage>();
  private int numOfGeoMsg = 0;
  
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		initMapView();
		findMyself();
		cleanCustomItem();
		showAllReceivedMessageTaskQueryAndGeoMsg();
		zoomInToPoint();
		
		/*======================================================================================= 
		 * Location Sensitive Service Customization Broadcast Receiver
		 *=======================================================================================*/		
		LSSC_BroadcastReceiver = new LocationSensitiveServiceCustomizationBroadcastReceiver();
		
		inbox_but = (ImageButton) findViewById(R.id.inbox_button);
		inbox_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inbox_page = new Intent(Home.this,inbox.class);
				Home.this.startActivity(inbox_page);
			}
		});
		
		outbox_but = (ImageButton) findViewById(R.id.outbox_button);
		outbox_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outbox_page = new Intent(Home.this,outbox.class);
				Home.this.startActivity(outbox_page);
			}
		});
		
		send_but = (ImageButton) findViewById(R.id.send_button);
		send_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent make_page = new Intent(Home.this,Make.class);
				Bundle bundle = new Bundle();
				bundle.putDouble("latitude", latituteLoc);
				bundle.putDouble("longitude", longitudeLoc);
				bundle.putString("provider", provider);
				bundle.putString("imgData", "");
				make_page.putExtra("location", bundle);
				Home.this.startActivity(make_page);
			}
		});
		
		
		favorite_but = (ImageButton) findViewById(R.id.favorite_button);
		favorite_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent favorite_page = new Intent(Home.this,FavoritePage.class);
			    Home.this.startActivity(favorite_page);
				
			}
		});
		
		settings_but = (ImageButton) findViewById(R.id.settings_button);
		settings_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent settings_page = new Intent(Home.this,SettingsActivity.class);
				Home.this.startActivity(settings_page);
				
			}
		});
		
	} //end onCreate()
    
    
    /*=========================================
     *  Methods for implementing the Google Map
     * Writen by Daihua Ye
     *========================================*/
    
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	  
	public void initMapView() {      
      mapView = (MapView) findViewById(R.id.home_mapView);
      mapView.setBuiltInZoomControls(true);
      mapView.displayZoomControls(true);
      mapView.setSatellite(false);
      
      mapController = mapView.getController();
      
      locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
      
      locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
          0, new GeoUpdateHandler());
      // overlay item generated
      mapOverlays = mapView.getOverlays();
      drawable = getResources().getDrawable(R.drawable.marker2);
      customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
      
      avg_latitude = 0;
      avg_longitude = 0;
      total_points = 0;
    }
    
    public void findMyself() {
      boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
      
      if(!enabled) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
      }
      
      Criteria criteria = new Criteria();
      provider = locationManager.getBestProvider(criteria, false);
      
      Log.i("Provider is ", provider.toString()); 
      location = locationManager.getLastKnownLocation(provider);
      
      
      if (location != null) {
        Log.i("Provider ", provider + " has been selected.");
        latituteLoc = (double) (location.getLatitude());
        longitudeLoc = (double) (location.getLongitude());
      } else {
        // set use your current is desable
        latituteLoc = 0;
        longitudeLoc = 0;
        Log.i("mapViewGenerator", "Could not find your location");
      }
    }
    
    private void showAllReceivedMessageTaskQueryAndGeoMsg() {
      LocationTaskActivity.cmnDATABASE.open();
      
      // message
      numOfMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberMessagesNotRead(LocationTaskActivity.username);
      
      if (numOfMsg > 0){
        //msg_list = LocationTaskActivity.cmnDATABASE.getAllMessagesAtLocation(LocationTaskActivity.username, new Location(cur_latitude,cur_longitude));
        msg_list = LocationTaskActivity.cmnDATABASE.getAllMessagesNotRead(LocationTaskActivity.username);
        for(Message m : msg_list) {
          if(m.getLocation().getLatitude() != 0 && m.getLocation().getLongitude() != 0) {
            String text = m.getTextData();
            String sender = m.getSender();
            double lat = m.getLocation().getLatitude();
            double log = m.getLocation().getLongitude();
            avg_latitude = lat + avg_latitude;
            avg_longitude = log + avg_longitude;
            total_points++;
            text = "Message: " + text;
            if (sender.length()>10){
            	String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
            			+ sender.substring(4, 7) + "-" + sender.substring(7);
            	createMarker(text, mod_sender, lat, log, "");
            }	
            else
            	createMarker(text, sender, lat, log, "");
          }
        }
      }
      
      // task
      numOfTask = LocationTaskActivity.cmnDATABASE.getTotalNumberTasksActive(LocationTaskActivity.username);
      if (numOfTask > 0){
        task_list = LocationTaskActivity.cmnDATABASE.getAllTasksActive(LocationTaskActivity.username);
        for(Task t : task_list) {
          if(t.getLocation().getLatitude() != 0 && t.getLocation().getLongitude() != 0) {
            String text = t.getTextData();
            String sender = t.getSender();
            double lat = t.getLocation().getLatitude();
            double log = t.getLocation().getLongitude();
            avg_latitude = lat + avg_latitude;
            avg_longitude = log + avg_longitude;
            total_points++;
            text = "Task: " + text;
            if (sender.length() >10){
	            String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
	                + sender.substring(4, 7) + "-" + sender.substring(7);
	            createMarker(text, mod_sender, lat, log, "");
            }
            else
            	createMarker(text, sender, lat, log, "");
          }
        }
      }
      
      // query
      numOfQuery = LocationTaskActivity.cmnDATABASE.getTotalNumberQueriesActive(LocationTaskActivity.username);
      if (numOfQuery > 0){
        query_list = LocationTaskActivity.cmnDATABASE.getAllQueriesActive(LocationTaskActivity.username);
        for(Query q : query_list) {
          if(q.getLocation().getLatitude() != 0 && q.getLocation().getLongitude() != 0) {
            String text = q.getTextData();
            String sender = q.getSender();
            double lat = q.getLocation().getLatitude();
            double log = q.getLocation().getLongitude();
            avg_latitude = lat + avg_latitude;
            avg_longitude = log + avg_longitude;
            total_points++;
            text = "Query: " + text;
            if (sender.length() > 10){
            	String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
            			+ sender.substring(4, 7) + "-" + sender.substring(7);
            	createMarker(text, mod_sender, lat, log, "");
            }
            else
            	createMarker(text, sender, lat, log, "");
          }
        }
      }
      
      // geomsg
      numOfGeoMsg = LocationTaskActivity.cmnDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
      if (numOfGeoMsg > 0){
        geomsg_list = LocationTaskActivity.cmnDATABASE.getAllGeoMsg(LocationTaskActivity.username);
        for(GeoMessage gs : geomsg_list) {
          if(gs.getGeoLocation().getLatitude() != 0 && gs.getGeoLocation().getLongitude() != 0) {
            String text = gs.getTextdata();
            String sender = gs.getSender();
            double lat = gs.getGeoLocation().getLatitude();
            double log = gs.getGeoLocation().getLongitude();
            String img_url = gs.getPathURL();
            avg_latitude = lat + avg_latitude;
            avg_longitude = log + avg_longitude;
            total_points++;
            text = "GeoMessage: " + text;
            if (sender.length() > 10){
            	String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
            			+ sender.substring(4, 7) + "-" + sender.substring(7);
            	createMarker(text, mod_sender, lat, log, img_url);
            }
            else
            	createMarker(text, sender, lat, log, "");
          }
        }
      }
      LocationTaskActivity.cmnDATABASE.close();
    }
    
    public void zoomToLocation(double lat, double log) {
      // zoom into the item
      GeoPoint point = new GeoPoint((int)lat, (int)log);
      mapController.animateTo(point);
      mapController.setZoom(12);
      
      CustomOverlayItem overlayItem = new CustomOverlayItem(point, "Tomorrow Never Dies (1997)", 
          "(M gives Bond his mission in Daimler car)", 
          "http://ia.media-imdb.com/images/M/MV5BMTM1MTk2ODQxNV5BMl5BanBnXkFtZTcwOTY5MDg0NA@@._V1._SX40_CR0,0,40,54_.jpg");
      
        Log.i("CustomInemizedOverlay", customItemizedOverlay.size() + "");
        Log.i("CustomItem", "Clean it");
        //cleanCustomItem();
        customItemizedOverlay.addOverlay(overlayItem);
        mapOverlays.add(customItemizedOverlay);
    }
    
//    public class GeoUpdateHandler implements LocationListener {
//
//      @Override
//      public void onLocationChanged(Location location) {
////        int lat = (int) (location.getLatitude() * 1E6);
////        int lng = (int) (location.getLongitude() * 1E6);
////        zoomToLocation(lat, lng);
//      }
//
//      @Override
//      public void onProviderDisabled(String provider) {
//      }
//
//      @Override
//      public void onProviderEnabled(String provider) {
//      }
//
//      @Override
//      public void onStatusChanged(String provider, int status, Bundle extras) {
//      }
//    }
    
    // test the jsonObject call for finding the lantitude and longtitude
    public static JSONObject getLocationInfo(String address) {
      StringBuilder stringBuilder = new StringBuilder();
      try {

        address = address.replaceAll(" ","%20");    

        HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        stringBuilder = new StringBuilder();


        response = client.execute(httppost);
        HttpEntity entity = response.getEntity();
        InputStream stream = entity.getContent();
        int b;
        while ((b = stream.read()) != -1) {
            stringBuilder.append((char) b);
        }
      } catch (ClientProtocolException e) {
        return null;
      } catch (IOException e) {
        return null;
      }

      JSONObject jsonObject = new JSONObject();
      try {
          jsonObject = new JSONObject(stringBuilder.toString());
      } catch (JSONException e) {
          // TODO Auto-generated catch block
          Log.i("JSONException Need stop here", "here");
          e.printStackTrace();
          return null;
      }
      return jsonObject;
    }
    
    public static GeoPoint getLatLong(JSONObject jsonObject) {

      Double lon = new Double(0);
      Double lat = new Double(0);

      try {
          lon = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
              .getJSONObject("geometry").getJSONObject("location")
              .getDouble("lng");

          lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
              .getJSONObject("geometry").getJSONObject("location")
              .getDouble("lat");
      } catch (Exception e) {
          e.printStackTrace();
          return null;
      }

      return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
    }
    
    public void cleanCustomItem() {
      customItemizedOverlay.getOverLays().clear();
    }
    
    public void zoomInToPoint() {
//      GeoPoint point = new GeoPoint((int)(avg_latitude / total_points *1e6), (int)(avg_longitude / total_points * 1e6));
      //mapController.animateTo(point);
      mapController.setZoom(4);
      GeoPoint point = new GeoPoint((int)(38.134557 * 1e6), (int)(-98.803713 * 1e6));
      mapController.setCenter(point);
    }
    
    public void createMarker(String text, String sender, double lat, double log, String img_url) {
        GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
        
        CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
        customItemizedOverlay.addOverlay(overlayItem);
        mapOverlays.add(customItemizedOverlay);
    }
}

