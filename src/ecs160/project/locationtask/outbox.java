package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.List;

import mapviewballoons.example.custom.CustomItemizedOverlay;
import mapviewballoons.example.custom.CustomOverlayItem;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import ecs160.project.locationtask.inbox.UpdateCurrentLoaction;

public class outbox extends MapActivity {

	Button omsg_but,otask_but,oquery_but,ogeomsg_but;
	ImageButton orefresh_but,ohome_but;
	int numOfoutMsg,numOfoutTask,numOfoutQuery,numOfoutGeoMsg;
	
	 // map view avaliables
	private LocationManager locationManager = null;
  private MapView mapView = null;
  private MapController mapController = null;
  private List<Overlay> mapOverlays; 
  private Drawable drawable;
  private CustomItemizedOverlay<CustomOverlayItem> customItemizedOverlay;
  private double avg_latitude;
  private double avg_longitude;
  private int total_points;
  
  // all messages, tasks, queue, geomessage
  private ArrayList<Message> msg_list = new ArrayList<Message>();
  private int numOfMsg = 0;
  private ArrayList<Task> task_list = new ArrayList<Task>();
  private int numOfTask = 0;
  private ArrayList<Query> query_list = new ArrayList<Query>();
  private int numOfQuery = 0;
  private ArrayList<GeoMessage> geomsg_list = new ArrayList<GeoMessage>();
  private int numOfGeoMsg = 0;
  
	@Override
	protected void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);
		setContentView(R.layout.outbox);
		
		// initialize mapview
		initMapView();
		LocationTaskActivity.myDATABASE.open();
		
		// show the pin
		cleanCustomItem();
		showAllSentMessageTaskQueryAndGeoMsg();
		zoomInToPoint();
		
		ohome_but = (ImageButton)findViewById(R.id.outbox_home_button);
		ohome_but.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent home_page = new Intent(outbox.this,Home.class);
				outbox.this.startActivity(home_page);
			}
		});
		
		omsg_but = (Button) findViewById(R.id.outbox_msg_button);
		numOfoutMsg = LocationTaskActivity.myDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
		omsg_but.setText("Message \n"+ Integer.toString(numOfoutMsg));
		omsg_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outbox_msg_page = new Intent(outbox.this,outMessagePage.class);
				outbox.this.startActivity(outbox_msg_page);
			}
		});
		
		otask_but = (Button) findViewById(R.id.outbox_task_button);
		numOfoutTask = LocationTaskActivity.myDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
		otask_but.setText("Task \n"+Integer.toString(numOfoutTask));
		otask_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outbox_task_page = new Intent(outbox.this,outTaskPage.class);
				outbox.this.startActivity(outbox_task_page);
			}
		});
		
		
		oquery_but = (Button) findViewById(R.id.outbox_query_button);
		numOfoutQuery = LocationTaskActivity.myDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
		oquery_but.setText("Query \n"+Integer.toString(numOfoutQuery));
		oquery_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outbox_query_page = new Intent(outbox.this,outQueryPage.class);
				outbox.this.startActivity(outbox_query_page);
			}
		});
		
		ogeomsg_but = (Button) findViewById(R.id.outbox_geomsg_button);
		numOfoutGeoMsg = LocationTaskActivity.myDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
		ogeomsg_but.setText("Geo-msg\n"+Integer.toString(numOfoutGeoMsg));
		ogeomsg_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outbox_geomsg_page = new Intent(outbox.this,outGeoMsgPage.class);
				outbox.this.startActivity(outbox_geomsg_page);
			}
		});
		
		orefresh_but = (ImageButton) findViewById(R.id.outbox_refresh_button);
		orefresh_but.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// insert code here...
				LocationTaskActivity.myDATABASE.open();
				numOfoutMsg = LocationTaskActivity.myDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
				omsg_but.setText("Message \n"+Integer.toString(numOfoutMsg));
				
				numOfoutTask = LocationTaskActivity.myDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
				otask_but.setText("Task \n"+Integer.toString(numOfoutTask));
				
				numOfoutQuery = LocationTaskActivity.myDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
				oquery_but.setText("Query \n"+Integer.toString(numOfoutQuery));
				
				numOfoutGeoMsg = LocationTaskActivity.myDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
				ogeomsg_but.setText("Geo-msg\n"+Integer.toString(numOfoutGeoMsg));
				
			}
		});
		LocationTaskActivity.myDATABASE.close();
		
	} //onCreate
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	 private void initMapView() {
	    mapView = (MapView) findViewById(R.id.outbox_mapView);
	    mapView.setBuiltInZoomControls(true);
	    mapView.displayZoomControls(true);
	    mapView.setSatellite(false);
	    
	    mapController = mapView.getController();
	    mapOverlays = mapView.getOverlays();
	    drawable = getResources().getDrawable(R.drawable.marker5);
	    customItemizedOverlay = new CustomItemizedOverlay<CustomOverlayItem>(drawable, mapView);
	    
	    locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
	    
	    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
	        0, new UpdateCurrentLoaction());
	    
	    avg_latitude = 0;
	    avg_longitude = 0;
	    total_points = 0;
	  }

	  private void showAllSentMessageTaskQueryAndGeoMsg() {
//	    LocationTaskActivity.cmnDATABASE.open();
	    
	    // message
	    numOfMsg = LocationTaskActivity.myDATABASE.getTotalNumberMessages(LocationTaskActivity.username);
	    
	    if (numOfMsg > 0){
	      msg_list = LocationTaskActivity.myDATABASE.getAllMessages(LocationTaskActivity.username);
	      for(Message m : msg_list) {
	        if(m.getLocation().getLatitude() != 0 && m.getLocation().getLongitude() != 0) {
	          String text = m.getTextData();
	          String sender = m.getSender();
	          double lat = m.getLocation().getLatitude();
	          double log = m.getLocation().getLongitude();
	          avg_latitude = lat + avg_latitude;
	          avg_longitude = log + avg_longitude;
	          total_points++;
	          text = "Message: " + text;
//	          String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
//                + sender.substring(4, 7) + "-" + sender.substring(7);
	          createMarker(text, sender, lat, log, "");
	        }
	      }
	    }
	    
	    // task
	    numOfTask = LocationTaskActivity.myDATABASE.getTotalNumberTasks(LocationTaskActivity.username);
	    if (numOfTask > 0){
	      task_list = LocationTaskActivity.myDATABASE.getAllTasks(LocationTaskActivity.username);
	      for(Task t : task_list) {
	        if(t.getLocation().getLatitude() != 0 && t.getLocation().getLongitude() != 0) {
	          String text = t.getTextData();
	          String sender = t.getSender();
	          double lat = t.getLocation().getLatitude();
	          double log = t.getLocation().getLongitude();
	          avg_latitude = lat + avg_latitude;
	          avg_longitude = log + avg_longitude;
	          total_points++;
	          text = "Task: " + text;
//	          String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
//                + sender.substring(4, 7) + "-" + sender.substring(7);
	          createMarker(text, sender, lat, log, "");
	        }
	      }
	    }
	    
	    // query
	    numOfQuery = LocationTaskActivity.myDATABASE.getTotalNumberQueries(LocationTaskActivity.username);
	    if (numOfQuery > 0){
	      query_list = LocationTaskActivity.myDATABASE.getAllQueries(LocationTaskActivity.username);
	      for(Query q : query_list) {
	        if(q.getLocation().getLatitude() != 0 && q.getLocation().getLongitude() != 0) {
	          String text = q.getTextData();
	          String sender = q.getSender();
	          double lat = q.getLocation().getLatitude();
	          double log = q.getLocation().getLongitude();
	          avg_latitude = lat + avg_latitude;
	          avg_longitude = log + avg_longitude;
	          total_points++;
	          text = "Query: " + text;
//	          String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
//                + sender.substring(4, 7) + "-" + sender.substring(7);
	          createMarker(text, sender, lat, log, "");
	        }
	      }
	    }
	    
	    // geomsg
	    numOfGeoMsg = LocationTaskActivity.myDATABASE.getTotalNumberGeoMsg(LocationTaskActivity.username);
	    if (numOfGeoMsg > 0){
	      geomsg_list = LocationTaskActivity.myDATABASE.getAllGeoMsg(LocationTaskActivity.username);
	      for(GeoMessage gs : geomsg_list) {
	        if(gs.getGeoLocation().getLatitude() != 0 && gs.getGeoLocation().getLongitude() != 0) {
	          String text = gs.getTextdata();
	          String sender = gs.getSender();
	          double lat = gs.getGeoLocation().getLatitude();
	          double log = gs.getGeoLocation().getLongitude();
	          String img_url = gs.getPathURL();
	          avg_latitude = lat + avg_latitude;
	          avg_longitude = log + avg_longitude;
	          total_points++;
	          text = "GeoMessage: " + text;
//	          String mod_sender = sender.substring(0, 1) + "-(" + sender.substring(1, 4) + ")-"
//                + sender.substring(4, 7) + "-" + sender.substring(7);
	          createMarker(text, sender, lat, log, img_url);
	        }
	      }
	    }
//	    LocationTaskActivity.cmnDATABASE.close();
	  }
	
  public void cleanCustomItem() {
    customItemizedOverlay.getOverLays().clear();
  }
  
  public void zoomInToPoint() {
    GeoPoint point = new GeoPoint((int)(avg_latitude / total_points *1e6), (int)(avg_longitude / total_points * 1e6));
    //mapController.animateTo(point);
    mapController.setZoom(10);
    mapController.setCenter(point);
  }
  
  public void createMarker(String text, String sender, double lat, double log, String img_url) {
      GeoPoint point = new GeoPoint((int)(lat * 1e6), (int)(log * 1e6));
      
      CustomOverlayItem overlayItem = new CustomOverlayItem(point, sender, text, img_url);
      customItemizedOverlay.addOverlay(overlayItem);
      mapOverlays.add(customItemizedOverlay);
  }
  
  public void zoomInPointWithLoaction(Location location) {
	  GeoPoint point = new GeoPoint((int)(location.getLatitude() * 1e6), (int)(location.getLongitude() * 1e6));
	  mapController.setZoom(10);
	  mapController.animateTo(point);
  }
  
  public class UpdateCurrentLoaction  implements LocationListener {

	    @Override
	    public void onLocationChanged(Location location) {
	    	zoomInPointWithLoaction(location);
	    }

	    @Override
	    public void onProviderDisabled(String provider) {
	    }

	    @Override
	    public void onProviderEnabled(String provider) {
	    }

	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {
	    }
	}


}
