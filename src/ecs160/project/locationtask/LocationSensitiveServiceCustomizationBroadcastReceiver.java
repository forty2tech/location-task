package ecs160.project.locationtask;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.util.Log;

/*==============================================================================
 * Robert's awesome Location Sensitive Service Customization Broadcast Receiver
 *==============================================================================*/

public class LocationSensitiveServiceCustomizationBroadcastReceiver extends BroadcastReceiver {
	
	private Calendar calendar;
	private CdmaCellLocation cdmaCellLocation;
	private TelephonyManager telephonyManager;	
	
	private int cdmaLongitude = 0;
	private int cdmaLatitude = 0;
	
	private boolean contactEx;
	private boolean locationEx;
	private boolean dayEx, hasExpired;
	
	
	public void onReceive(Context context, Intent intent) {
		
		dayEx = false;
		hasExpired = false;
		contactEx = false;
		locationEx = false;
		cdmaCellLocation = new CdmaCellLocation();
		
		telephonyManager = 
				(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		
		telephonyManager.listen(new MyPhoneStateListener(), MyPhoneStateListener.LISTEN_CALL_STATE);

		/*======================================================================
		 * SMS Received 
		 *=====================================================================*/          
	        /*
		        Bundle bundle = intent.getExtras();
		        Object[] pdus = (Object[]) bundle.get("pdus");
		        SmsMessage message = SmsMessage.createFromPdu((byte[])pdus[0]);
		        if(!message.isEmail())
	            	doSomething(message.getOriginatingAddress());
            */		
	}
	
	/*==================================================
	 * Callback invoked when device call state changes.
	 *==================================================*/
	public class MyPhoneStateListener extends PhoneStateListener {
		public void onCallStateChanged(int state, String incomingNumber) {
			
			switch(state) {
				
				case TelephonyManager.CALL_STATE_RINGING:
					
					cdmaCellLocation.requestLocationUpdate();
					cdmaLongitude = cdmaCellLocation.getBaseStationLongitude();
					cdmaLatitude = cdmaCellLocation.getBaseStationLatitude();
					
					Double log = cdmaLongitude/1e6;
					Double lat = cdmaLatitude/1e6;
					
					Log.d("INCOMING PHONE #",incomingNumber);					
					Log.d("CDMA LONGITUDE",Integer.toString(cdmaLongitude));
					Log.d("CDMA LATITUDE",Integer.toString(cdmaLatitude));
					Log.d("CDMA LONGITUDE 1e6",Double.toString(log));
					Log.d("CDMA LATITUDE 1e6",Double.toString(lat));
					
					/* =============================================
					 * checking an ArrayList for contact exception
					 * true = exists
					 * false = does not exist
					 *==============================================*/
					contactEx = LocationTaskActivity.currentProfile.getContactExceptions().
						contains(incomingNumber);
					
					/* =============================================
					 * checking an ArrayList for location exception
					 * true = exists
					 * false = does not exist
					 *==============================================*/
//					locationEx = LocationTaskActivity.currentProfile.getLocationExceptions().
//						contains(new Location(cdmaLatitude, cdmaLongitude));
					
					ArrayList<Location> locations = LocationTaskActivity.currentProfile.getLocationExceptions();
					for(int i = 0; i < locations.size(); i++)
					{
						if(locations.get(i).getLatitude() == cdmaLatitude)
						{
							if(locations.get(i).getLongitude() == cdmaLongitude)
							{
								locationEx = true;
								break;
							}
						}
					}
//					Log.d("locationEx",Boolean.toString(locationEx));
//					Log.w("locationEx", "location: "+LocationTaskActivity.currentProfile.getLocationExceptions().get(0).getLatitude());
//					Log.d("locationEx",Boolean.toString(locationEx));
					
					calendar = Calendar.getInstance(); 
					Time expires = LocationTaskActivity.currentProfile.getExpiration();
					if(calendar.get(Calendar.YEAR) > expires.getYear())
					{
						hasExpired = true;
					}
					else if(calendar.get(Calendar.YEAR) == expires.getYear())
					{
						if(calendar.get(Calendar.MONTH) +1 > expires.getMonth())
							hasExpired = true;
						else if(calendar.get(Calendar.MONTH) +1 == expires.getMonth())
						{
							if(calendar.get(Calendar.DAY_OF_MONTH) > expires.getDay())
								hasExpired = true;
						}
					} 
					
					if(LocationTaskActivity.currentProfile.getExpiration().getMonth() == 0 &&
					   LocationTaskActivity.currentProfile.getExpiration().getYear() == 0 &&
					   LocationTaskActivity.currentProfile.getExpiration().getDay() == 0)
						hasExpired = false;
//					Log.w("DAY_OF_MONTH", "DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
//					ArrayList<Day> daysEnabled
//					if(calendar.get(Calendar.DAY_OF_WEEK) == 1) //sunday
//					{
//						dayEx = LocationTaskActivity.currentProfile.getd
//						
//					}
//					Log.w("DATEEX", "DATEEX: " + Boolean.toString(dateEx));
//					if(dateEx)
//						LocationTaskActivity.currentProfile.apply();
					/* ================================================================================
					 * if we find a contact exception and a location exception in our profile, 
					 * allow the incoming call to ring and vibrate
					 *================================================================================*/				
					if ((contactEx || locationEx) && !hasExpired)
//					else if (contactEx && locationEx)
						LocationTaskActivity.currentProfile.allON();
					//else if(dayEx)

					break;
				
				case TelephonyManager.CALL_STATE_OFFHOOK:
					
					break;
					
				case TelephonyManager.CALL_STATE_IDLE:
					
					/* ===================================================
					 * Restore our profile.
					 *====================================================*/
					LocationTaskActivity.currentProfile.apply();
					
					break;
					
			} // end switch
		} // end onCallStateChanged
	} // end MyPhoneStateListener
} // end Location Sensitive Service Customization Broadcast Receiver
