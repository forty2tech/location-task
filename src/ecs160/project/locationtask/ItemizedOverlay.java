package ecs160.project.locationtask;

import java.util.ArrayList;

import com.google.android.maps.OverlayItem;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class ItemizedOverlay extends com.google.android.maps.ItemizedOverlay<OverlayItem> {
  
  private ArrayList<OverlayItem> mOverlays =  new ArrayList<OverlayItem>();
  Context mContext;
  public ItemizedOverlay(Drawable defaultMarker) {
    super(boundCenterBottom(defaultMarker));
    // TODO Auto-generated constructor stub
  }

  public ItemizedOverlay(Drawable defaultMarker, Context context) {
    super(boundCenterBottom(defaultMarker));
    mContext = context;
  }
  
  @Override
  protected OverlayItem createItem(int i) {
    // TODO Auto-generated method stub
    return mOverlays.get(i);
  }

  @Override
  public int size() {
    // TODO Auto-generated method stub
    return mOverlays.size();
  }

  public void addOverlay(OverlayItem overlay) {
    mOverlays.add(overlay);
    populate();
  }

  @Override
  protected boolean onTap(int index) {
    // TODO Auto-generated method stub
    OverlayItem item = mOverlays.get(index);
    AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
    dialog.setTitle(item.getTitle());
    dialog.setMessage(item.getSnippet());
    dialog.show();
    return true;
  }
  
  

}
